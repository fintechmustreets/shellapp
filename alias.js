module.exports = {
    module: './src/module',
    entrypoint: './src/entrypoint',
    assets: './src/assets',
    config: './src/config',
    constants: './src/constants',
    uiComponents: './src/style/UIComponents',
    gql: './src/gql',
    navigation: './src/navigation',
    screens: './src/screens',
    store: './src/store',
    styled: './src/style',
    utils: './src/utils',
    localization: './src/localization',
    hooks: './src/hooks',
    jest: './src/jest',
    storybook: './storybook'
};