/* eslint-disable brace-style */
import { showMessage } from "react-native-flash-message"
import * as R from 'ramda'

export const requestDataLoader = async ({
    client,
    query,
    variables,
}) => {
    try {
        const answer = await client.query({
            query, variables, fetchPolicy: "network-only"
        });

        return answer
    }
    catch (error) {
        console.warn('error');
    }
};

export const requestMutation = async ({
    client,
    query,
    variables,
}) => {
    try {
        const answer = await client.mutate({
            mutation: query, variables
        });

        return answer
    }
    catch (error) {
        return showMessage({
            message: R.toString(error),
            description: "Error",
            type: "info",
        });
    }
};
