/* eslint-disable react/forbid-prop-types */
/* eslint-disable brace-style */
/* eslint-disable max-statements */
/* eslint-disable no-console */
/* eslint-disable react/jsx-indent-props */
import { useApolloClient } from '@apollo/react-hooks'
import PropTypes from 'prop-types'
import * as R from 'ramda'
import React, { useState } from 'react'
import { showMessage } from "react-native-flash-message"
import UiButtom from 'uiComponents/UiButtom'
import { isOdd } from 'utils/helper'

const MutationButton = (props) => {
    const {
        color,
        onPress,
        QUERY,
        testID,
        Title,
        Update,
        variables,
    } = props;
    const [LoadingStatus, setLoadingStatus] = useState(false);
    const client = useApolloClient();

    const success = async () => {
        setLoadingStatus(true);
        let data;

        try {
            if (QUERY) {
                data = await client.mutate({
                    mutation: QUERY,
                    variables
                })
            }
            setLoadingStatus(false);

            return data
        } catch (error) {
            setLoadingStatus(false);
            showMessage({
                message: R.toString(error),
                description: "Error",
                type: "info",
            });

            return R.toString(error)
        }
    };

    return (
        <UiButtom
            color={color}
            loading={LoadingStatus}
            onPress={async () => {
                const data = await success(variables);

                return onPress(data)
            }}
            testID={isOdd(testID) ? "TEST" : R.join('', ["button", testID])}
            Title={Title}
            Update={Update}
        />
    )
};

MutationButton.propTypes = {
    color: PropTypes.string,
    Title: PropTypes.string,
    Update: PropTypes.string,
    variables: PropTypes.object,
};

MutationButton.getDefaultProps = {
    color: "Mono_Grey",
    Title: null,
    Update: null,
    variables: {},
};

export default MutationButton