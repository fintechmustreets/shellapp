/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable no-unused-vars */
import * as R from 'ramda';
import React from 'react';
import { Alert, TouchableOpacity, View } from 'react-native';
import { Checkbox } from 'react-native-paper';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import i18n from 'localization'
import { usePdfManipulationFile } from 'hooks/usePdfManipulationFile'

export const PolicyAdd = ({ checkbox, setCheckBox, id }) => {
    const [check, setCheck] = React.useState({ policy: false, personal: false });
    const [document, shareFile, alertPdfMessage] = usePdfManipulationFile()

    React.useEffect(() => {
        if (R.all(R.equals(true))(R.values(check))) {
            setCheckBox(true)
        }
    }, [check]);

    const readPolicyCheck = () => {
        Alert.alert(
            i18n.t('HeaderAlert'),
            i18n.t('Read_Data_Confirm'),
            [{
                text: i18n.t('CLOSED'),
                onPress: () => null,
            },
            {
                text: i18n.t('Personal_policy'),
                onPress: () => {
                    document('personal', null)
                    setCheck(R.assocPath(['personal'], true)(check))
                },
            },
            {
                text: i18n.t('Processing_data'),
                onPress: () => {
                    document('policy', null)
                    setCheck(R.assocPath(['policy'], true)(check))
                },
            }],
            { cancelable: false }
        )
    };

    if (id) return (<View />);

    return (
        <RowComponents
            style={{ margin: 10, flex: 1 }}
        >
            <Checkbox
                onPress={() => setCheckBox(!checkbox)}
                status={checkbox ? 'checked' : 'unchecked'}
            />
            <View style={{ flex: 1 }} >
                <TouchableOpacity onPress={() => readPolicyCheck()}>
                    <LabelText
                        color={ColorCard(checkbox ? 'classic' : 'blue').font}
                        fonts="ShellMedium"
                        items="SDescription"
                    >
                        {i18n.t('PolicyAddCheck')}
                    </LabelText>
                </TouchableOpacity>
            </View>
        </RowComponents>
    )
};
