/* eslint-disable no-unneeded-ternary */
/* eslint-disable no-confusing-arrow */
import * as R from 'ramda';
import { isOdd } from 'utils/helper';
import validator from 'validator';

export const ValidationText = (data, value) => {
    const keyboardValid = (elem, data) => R.cond([
        [R.equals("numeric"), R.always(validator.isNumeric(data))],
        [R.equals("default"), R.T],
    ])(elem);

    const isMainValid = (elem, data) => R.cond([
        [R.equals(true), R.always(isOdd(data))],
        [R.equals(false), R.F],
        [isOdd, R.T],
    ])(elem);

    const isLabelValid = (elem, data) => {
        const listRules = ['last', 'first', 'promo'];

        if (R.includes(elem, listRules)) return true;

        return R.cond([
            [R.equals('label'), R.always(validator.isAlphanumeric(data))],
            [R.equals('phone'), R.always(validator.isMobilePhone(data))],
            [R.equals('sms'), R.always(validator.isNumeric(data))],
        ])(elem)
    };

    const lenghtValid = (props, items) => {
        const isEven = (x) => R.equals(props, x);
        const isOdd = (x) => R.anyPass([R.isEmpty, R.isNil])(x);
        const isLenght = (x) => R.length(R.splitEvery(1, x)) < R.path(['Lenght'])(data) ? false : true;
        const isQLenght = (x) => R.equals(R.length(R.splitEvery(1, x)), R.path(['Lenght'])(data)) ? true : false;
        const listRules = ['last', 'first'];
        const listIsRules = ['promo'];

        if (R.includes(R.path(['label'])(data), listRules)) return isLenght(items);
        if (R.includes(R.path(['label'])(data), listIsRules)) return isQLenght(items);

        return R.cond([
            [R.equals(props), R.always(true)],
            [isEven, R.always(true)],
            [isOdd, R.always(false)],
        ])(R.length(R.splitEvery(1, items)))
    };

    const validationfunc = R.uniq([
        keyboardValid(R.path(['keyboardType'])(data), value),
        isMainValid(R.path(['main'])(data)),
        isLabelValid(R.path(['label'])(data), value),
        lenghtValid(R.path([R.path(['mask'])(data) ? 'Lenght' : 'maxLength'])(data), value),
    ]);

    return R.includes(false,
        R.pipe(
            R.map(
                R.ifElse(
                    R.equals(true),
                    R.always(true),
                    R.always(false)
                )
            )
        )(validationfunc)
    ) ? { error: R.path(['error'])(data), value } : { error: null, value }
};