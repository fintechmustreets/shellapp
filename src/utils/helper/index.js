/* eslint-disable max-statements */
/* eslint-disable radix */
/* eslint-disable newline-after-var */
/* eslint-disable newline-before-return */
/* eslint-disable operator-assignment */
import { forEach } from 'lodash';
import * as R from 'ramda';
import RNFS from 'react-native-fs';
import { buttom } from 'styled/colors';
import validator from 'validator';
import moment from 'moment'
import { uiColor } from 'styled/colors/uiColor.json';

export const ColorCard = R.cond([
    [R.equals('error'), R.always({ font: "Red", front: 'Red' })],
    [R.equals('warn'), R.always({ font: "Very_Dark_Grey", front: 'Yellow' })],
    [R.equals('download'), R.always({ font: "Green", front: 'Green' })],
    [R.equals('blue'), R.always({ font: "Light_Blue", front: 'Light_Blue' })],
    [R.equals('classic'), R.always({ font: "Very_Dark_Grey", front: 'Very_Dark_Grey' })],
    [R.equals('trans'), R.always({ font: "White", front: 'White' })],
]);

export const checkTotalPage = (data) => {
    const checkerSplit = (x) => x < 10;
    const datata = R.pipe(R.path(['totalCount']), R.toString, R.splitEvery(1))(data);

    if (R.path(['totalCount'])(data) > 10) {
        if (checkerSplit(R.last(datata))) {
            if (R.pipe(R.dropLast, R.join(''), R.add(1))(1, datata) > R.path(['nextPage'])(data)) return R.path(['nextPage'])(data);
            if (R.pipe(R.dropLast, R.join(''), R.add(1))(1, datata) > R.path(['page'])(data)) {
                return R.pipe(R.dropLast, R.join(''), R.add(1))(1, datata)
            }
        }
    }

    return null
};

export const helperMode = (x, mode) => R.pipe(
    R.path([mode]),
    R.omit(['__typename']),
    R.values,
    R.uniq,
    R.reject(isOdd)
)(x);

export const dataColor = (mode, css) => buttom[css][mode];

export const isOdd = (n) => R.anyPass([R.isNil, R.isEmpty, R.equals("__typename"), R.equals("createdAt"), R.equals("updatedAt")])(n);

export const changeStorageSearch = (searchParam, storage, reducer, mode) => R.cond([
    [R.isNil, R.always(R.path(['items'])(R.equals(mode, 'BLOCK') ? reducer : storage))],
    [R.isEmpty, R.always(R.path(['items'])(R.equals(mode, 'BLOCK') ? reducer : storage))],
    [R.equals(true), R.always(R.path(['items'])(R.includes(mode, ['UNBLOCK', 'BLOCK']) ? reducer : storage))],
    [R.equals(false), R.always(R.path(['items'])(reducer))],
    [R.equals('unblock'), R.always(R.path(['items'])(storage))],
    [R.equals('block'), R.always(R.path(['items'])(reducer))],
])(R.pipe(
    R.path(['search']),
    R.anyPass([R.isEmpty, R.isNil])
)(searchParam));

export const mapper = (z) => R.map(x => R.path([x])(z));
export const zipConCat = (x, z) => R.zipObj(x, mapper(z)(x));
export const nameSpace = (data) => R.join(' ', [R.path(['employee', "name", "last"])(data), R.path(['employee', "name", "first"])(data)]);

export const fastPickOut = (x, y) => R.reject(isOdd)(R.pick(x)(y));
export const rejectWithOut = (x, y) => R.reject(isOdd)(R.pick(R.without(x, R.keys(y)))(y));

export const toString = (search) => {
    try {
        if (R.equals("Number", R.type(search))) return R.toString(search);

        return search
    } catch (error) {
        if (R.equals("Number", R.type(search))) return R.toString(search);

        return search
    }
};

export const toInt = (search) => {
    try {
        if (validator.isNumeric(search)) return R.equals("String", R.type(search)) ? parseInt(search) : search;

        return search
    } catch (error) {
        if (validator.isNumeric(R.toString(search))) return R.equals("String", R.type(search)) ? parseInt(search) : search;

        return search
    }
};

export const phoneMedd = (text) => {
    let count = -1;

    if (isOdd(text)) return "";

    const Zipper = (x) => {
        if (R.equals(x, "_")) {
            count = count + 1;
            const data = R.splitEvery(1, R.equals("+7", R.head(R.splitEvery(2, text))) ? text : R.join("", R.concat(["+7"], [text])))[count];
            if (data) {
                return data
            }

            return "_"
        }

        return x
    };

    return R.pipe(
        R.splitEvery(1),
        R.map(Zipper),
        R.join("")
    )("__ (___) ___-__-__")
};

export const priceModed = (data) => {
    if (!validator.isNumeric(R.toString(data))) return data;
    const ls = (a, key) => {
        const isEven = x => x % 3 === 0;

        if (isEven(key)) return `${a} `;

        return a
    };

    const splitPrice = R.split(".", R.toString(data));

    const concatAll = R.converge(R.reduce(R.concat), [R.head, R.tail]);
    const lenght = () => {
        if (R.length(splitPrice) === 2) {
            const gygy = R.splitEvery(1)(R.last(splitPrice));

            if (R.length(gygy) === 1) {
                return R.join("", R.concat([R.last(splitPrice)], ["0"]))
            }
            return R.take(2, R.last(splitPrice))
        }

        return "00"
    };

    return R.join('', concatAll([[R.pipe(
        R.splitEvery(1),
        R.reverse,
        R.addIndex(R.map)(ls),
        R.reverse,
        R.join(""),
        R.trim
    )(R.head(splitPrice))], ["."], [lenght()]]))
};

export const toFormData = ({
    objectId, objectType, objectProperty, files
}) => {
    const form = new FormData();

    const data = {
        operations: JSON.stringify({
            query: `mutation ($input: CreateFileObjectInput!) {
              createFileObject (input: $input) {
                id
              }
            }`,
            variables: {
                input: {
                    upload: null,
                    objectId,
                    objectType,
                    objectProperty
                }
            }
        }),
        map: JSON.stringify({ 'myfile': ['variables.input.upload'] }),
        myfile: RNFS.readFile(files),
    };

    forEach(data, (field, fieldName) => {
        if (typeof field === 'object' && field.length) {
            forEach(field, (value, key) => {
                form.append(`${fieldName}[${key}]`, value)
            })
        } else form.append(fieldName, field)
    });

    return form
};

export const isDate = (n) => {
    const isMax = (n, z) => n <= z;
    if (R.length(n) == 1) {
        if (!isMax(R.head(n), 31)) {
            return ""
        }
    }
    if (R.length(n) == 2) {
        if (!isMax(n[1], 12)) {
            return R.head(n)
        }
    }

    return R.join('', n)
};

export const totalPartsPrice = (storage) => {
    const total = R.path(['unitCount'])(storage) * R.path(['depotItem', "priceOut", 'amount'])(storage);
    if (R.path(['discount', 'percent'])(storage)) {
        return total * (1 - parseInt(R.path(['discount', 'percent'])(storage)) / 100)
    }
    if (R.path(['discount', 'amount'])(storage)) {
        return total - parseInt(R.path(['discount', 'amount'])(storage))
    }

    return R.defaultTo("0.00")(total)
};

export const totalWorkPrice = (storage) => {
    let total;

    total = R.path(['unitPrice', 'amount'])(storage) * R.path(['unitCount'])(storage);
    if (R.path(['timeHrs'])(storage)) {
        total = total * parseFloat(R.path(['timeHrs'])(storage))
    }
    if (R.path(['discount', 'percent'])(storage)) {
        return total * (1 - parseInt(R.path(['discount', 'percent'])(storage)) / 100)
    }
    if (R.path(['discount', 'amount'])(storage)) {
        return total - parseInt(R.path(['discount', 'amount'])(storage))
    }

    return R.defaultTo("0.00")(total)
};

export const totalPrice = (storage) => {
    const total = R.path(['unitCount'])(storage) * R.path(['depotItem', "priceOut", 'amount'])(storage);
    if (R.path(['discount', 'amount'])(storage)) {
        return total - parseInt(R.path(['discount', 'amount'])(storage))
    }
    if (R.path(['discount', 'percent'])(storage)) {
        return total * (1 - parseInt(R.path(['discount', 'percent'])(storage)) / 100)
    }
    return R.defaultTo('0.00')(total)
};

export const statusTaskChecker = R.cond([
    [R.equals('preorder'), R.always({
        status: ['inWork'],
        edit: ['client', 'owner', 'description', 'description', 'vehicle', 'works', 'performer', 'services', 'parts', 'time', 'recomendation', 'file'],
        screen: ['reconsilation', 'workcompletion'],
        color: uiColor.Polo_Grey
    })],
    [R.equals('inWork'), R.always({
        status: ['completed', 'reconciliation', 'canceled', 'suspended'],
        edit: ['performer', 'services', 'parts', 'works', 'time', 'recomendation', 'file'],
        screen: ['reconsilation', 'workcompletion'],
        color: uiColor.Light_Green
    })],
    [R.equals('completed'), R.always({
        status: ['closed', 'reconciliation'],
        edit: ['recomendation', 'start'],
        color: uiColor.Light_Green
    })],
    [R.equals('closed'), R.always({ status: [''], edit: [] })],
    [R.equals('reconciliation'), R.always({
        status: ['inWork', 'suspended'],
        edit: ['performer', 'services', 'parts', 'time', 'recomendation', 'file'],
        screen: ['reconsilation', 'workcompletion'],
        color: uiColor.Yellow
    })],
    [R.equals('suspended'), R.always({
        status: ['inWork'],
        edit: ['services', 'performer', 'parts', 'description', 'vehicle', 'time', 'recomendation', 'file'],
        color: uiColor.Light_Blue
    })],
    [R.equals('canceled'), R.always({
        status: [''],
        edit: [],
        color: uiColor.Red
    })],
    [isOdd, R.always({
        status: ['preorder'],
        edit: ['services', 'performer', 'owner', 'client', 'parts', 'description', 'vehicle', 'time', 'recomendation', 'file'],
        color: uiColor.Very_Pole_Grey
    })],
]);

export const ChangeData = ({ data, day }) => {
    const dadadad = moment(day).add(23, 'hours').format();

    const isCloser = (item) => R.addIndex(R.map)((x, key) => [x, item[key + 1] || dadadad])(item);
    const diff = (a, b) => a.localeCompare(b);
    const RejectMistake = (x) => R.map(ele => {
        if (!R.equals(R.head(ele), R.last(ele))) return ele;

        return null
    })(x);

    const list = R.path(['garagePostReserves', 'items'])(data);
    const isSeparate = x => {
        if (!list) return R.assocPath(['time'], x)({ data: [] });

        return R.assocPath(['time'], x, {
            data: R.innerJoin(
                (record, id) => R.equals(R.paths([['startAt'], ['endAt']])(record), id),
                list
            )([x])
        })
    };

    const Closet = R.pipe(
        R.sort(diff),
        isCloser,
        RejectMistake,
        R.without([null]),
        R.pipe(R.map(isSeparate))
    )(R.concat(
        R.pipe(
            R.map(R.path(['startAt'])),
            R.prepend(moment(day).format())
        )(list),
        R.pipe(
            R.map(R.path(['endAt'])),
            R.append(dadadad)
        )(list)));

    return Closet
};

export const filterRequest = ({ state }) => {
    let input = {};

    if (R.path(['modification', 'model', 'manufacturerId'])(state)) {
        input = R.assocPath(['vehicle', 'modification', 'model', 'manufacturerId', 'eq'], R.path(['modification', 'model', 'manufacturerId'])(state))(input)
    }
    if (R.path(['modification', 'model', 'id'])(state)) {
        input = { vehicle: { modification: R.assocPath(['modelId', 'eq'], R.path(['modification', 'model', 'id'])(state))(R.path(['vehicle', 'modification'])(input)) } }
    }
    if (R.path(['owner'])(state)) {
        input = R.assocPath(['ownerId', 'eq'], R.path(['owner', 'id'])(state))(input)
    }
    if (R.path(['client'])(state)) {
        input = R.assocPath(['clientId', 'eq'], R.path(['client', 'id'])(state))(input)
    }
    if (R.path(['author'])(state)) {
        input = R.assocPath(['authorId', 'eq'], R.path(['author', 'id'])(state))(input)
    }
    if (R.path(['performer'])(state)) {
        input = R.assocPath(['performerId', 'eq'], R.path(['performer', 'id'])(state))(input)
    }

    return input
};

export const timeHelper = (x, key) => R.cond([
    [R.equals('unix'), R.always(R.product([moment(key).unix(), 1000]))],
    [R.equals('hours'), R.always(moment(key).format("HH-mm"))],
    [R.equals('currentHours'), R.always(moment(key).format("YYYY-MM-DD HH-mm"))],
    [R.equals('add'), R.always(moment(key).add(24, 'hours').format())],
    [R.equals('pre'), R.always(moment(key).add(4, 'hours').format())],
    [R.equals('format'), R.always(moment(key))],
    [R.equals('date'), R.always(moment(key).format("YYYY-MM-DD"))],
    [R.equals('qwe'), R.always(moment(key).format("YYYY-MM-DDTHH:mm:ss"))],
])(x);

const diffStartTime = (a, b) => {
    return a.startAt.localeCompare(b.startAt)
};

const diffLabel = (a, b) => {
    return a.label.localeCompare(b.label)
};

const checkerState = ({ data, startAt }) => {
    let sleep;

    const isRejectTime = (x) => {
        return timeHelper('unix', R.path(['startAt'])(x)) < timeHelper('unix', startAt)
    };

    if (isOdd(data)) {
        sleep = timeHelper('pre', startAt)
    } else {
        sleep = timeHelper('format',
            R.pipe(
                R.sort(diffStartTime),
                R.reject(isRejectTime),
                R.head,
                R.path(['startAt'])
            )(data))
    }
    return sleep
};

export const timeLoading = ({ data, time, startAt }) => {
    let aggr = [];
    const timeCurrent = isOdd(startAt) ? timeHelper('format', timeHelper('date', time)) : timeHelper('format', startAt);
    const endAt = isOdd(startAt) ? timeHelper('unix', timeHelper('add', timeCurrent)) : checkerState({ data, startAt });

    const rejectedSlack = (key) => {
        const timeChecker = R.pipe(
            R.map(x => {
                if (timeHelper('hours', R.path(['startAt'])(x)) <= timeHelper('hours', key) && timeHelper('hours', R.path(['endAt'])(x)) > timeHelper('hours', key)) {
                    return R.assocPath(['time'], timeHelper('hours', key))(x)
                }
                return { time: timeHelper('hours', key) }
            }),
            R.uniq
        )(data);

        if (R.length(timeChecker) >= 2) {
            const state = R.pipe(
                R.without([{ time: timeHelper('hours', key) }]),
                R.head
            )(timeChecker);
            return {
                label: R.join(' - ', [
                    timeHelper('hours', R.path(['startAt'])(state)),
                    timeHelper('hours', R.path(['endAt'])(state)),
                    "(",
                    R.path(['repairId'])(state),
                    ")",
                ]),
                value: timeHelper('unix', key)
            }
        }

        return {
            label: timeHelper('hours', key),
            value: timeHelper('unix', key)
        }
    };

    for (let i = timeHelper('unix', timeCurrent); i < timeHelper('unix', endAt); i = i + 1800000) {
        aggr = [rejectedSlack(i), ...aggr]
    }

    return R.sort(diffLabel)(R.uniq(aggr))
};