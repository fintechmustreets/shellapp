/* eslint-disable no-undef */
/* eslint-disable max-statements */
/* eslint-disable scanjs-rules/call_addEventListener */
/* eslint-disable no-else-return */
/* eslint-disable newline-after-var */
import React from 'react';
import i18n from 'localization'
import ImagePicker from 'react-native-image-picker';
import * as R from 'ramda'
import { ReactNativeFile } from "apollo-upload-client"
import DocumentPicker from 'react-native-document-picker';
import { showMessage } from 'react-native-flash-message';
import { getItem } from 'utils/async'

const REST_API_URL = 'http://devapi.zengine.it';

export const useUploadFile = () => {
    const [state, setState] = React.useState(null);
    const [file, setFile] = React.useState(null)
    const ref = React.useRef()

    React.useEffect(() => {
        if (file) {
            UploadPhoto(file).then(() => {
                ref.current = false
                setState(true)
            })
        }
    }, [file])

    React.useEffect(() => {
        return setState(null)
    }, [state]);

    const UploadPhoto = React.useCallback(async ({ data, response }) => {
        const name = R.cond([
            [R.includes('name'), R.always(R.path(['name'])(response))],
            [R.includes('fileName'), R.always(R.path(['fileName'])(response))],
            [R.includes('path'), R.always(R.pipe(R.path(['path']), R.defaultTo('/'), R.split('/'), R.last)(response))],
        ])(R.keys(response))

        const type = R.cond([
            [R.pipe(R.keys, R.includes('type'), R.not), R.always(R.join('/', ['video', R.pipe(R.path(['path']), R.defaultTo('/'), R.split('/'), R.last, R.defaultTo('.'), R.split('.'), R.last)(response)]))],
            [R.T, R.always(R.path(['type'])(response))],
            [R.F, R.always(R.path(['type'])(response))],
        ])(response)

        const file = new ReactNativeFile({
            name,
            type,
            uri: response.uri,
        });

        const jswToken = await getItem();
        const token = `Bearer ${jswToken}`;

        const xhr = new XMLHttpRequest();
        const formData = new FormData();

        formData.append('operations', R.toString({
            "query": "mutation ($input: CreateFileObjectInput!) {createFileObject (input: $input) {id}}",
            "variables": { "input": data }
        }));
        formData.append('map', '{ "files": ["variables.input.upload"] }');
        formData.append('files', file);

        xhr.upload.addEventListener('progress', (e) => console.log(e));

        xhr.addEventListener("load", () => {
            setFile(null)
            try {
                if (JSON.parse(xhr.response).errors) {
                    setState(xhr.response)

                    return showMessage({
                        message: R.head(JSON.parse(xhr.response).errors).message,
                        description: "Error",
                        type: "danger",
                    })
                }
                setState(xhr.response);

                return showMessage({
                    message: "FILE UPLOAD",
                    description: "Success",
                    type: "success",
                })
            } catch (err) {
                setState(err);
                showMessage({
                    message: R.toString(e),
                    description: "Success",
                    type: "info",
                })
            }
        });
        xhr.open("POST", R.join('/', [REST_API_URL, 'graphql']));
        xhr.setRequestHeader('Authorization', token);
        xhr.send(formData)
    }, [file]);

    const photo = React.useCallback(() => {
        const options = {
            title: "CAMERA ROLL",
            quality: 0.6,
            maxWidth: 600,
            maxHeight: 400,
        }

        const data = {
            "upload": null,
            "objectId": 984,
            "objectType": "Repair",
            "objectProperty": "attachments"
        }

        if (!R.equals(ref.current, true)) {
            ref.current = true
            ImagePicker.showImagePicker(options, (response) => {
                if (response.didCancel) {
                    return showMessage({
                        message: 'User cancelled video picker',
                        description: "Success",
                        type: "success",
                    })
                } else if (response.error) {
                    return showMessage({
                        message: R.join(' ',
                            [
                                'ImagePicker Error: ',
                                R.toString(response.error)
                            ]
                        ),
                        description: "Success",
                        type: "success",
                    })
                } else if (response.customButton) {
                    return showMessage({
                        message: R.join(' ',
                            [
                                'User tapped custom button: ',
                                R.toString(response.customButton)
                            ]
                        ),
                        description: "Success",
                        type: "success",
                    })
                } else {
                    ref.current = true
                    setFile({ response, data })
                }
            })
        }
    }, [])

    const video = React.useCallback((data) => {
        const options = {
            title: "CAMERA ROLL",
            takePhotoButtonTitle: 'Take Video...',
            mediaType: 'video',
            videoQuality: 'high',
            quality: 0.6,
            storageOptions: {
                skipBackup: true,
                path: 'video',
            }
        }

        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                return showMessage({
                    message: 'User cancelled video picker',
                    description: "Success",
                    type: "success",
                })
            } else if (response.error) {
                return showMessage({
                    message: R.join(' ', [
                        'ImagePicker Error: ',
                        R.toString(response.error)
                    ]),
                    description: "Success",
                    type: "success",
                })
            } else if (response.customButton) {
                return showMessage({
                    message: R.join(' ', [
                        'User tapped custom button: ',
                        R.toString(response.customButton)
                    ]),
                    description: "Success",
                    type: "success",
                })
            } else {
                setFile({ response, data })
            }
        })
    }, [state])

    const document = React.useCallback(async (data) => {
        try {
            const response = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            })
            setFile({ response, data })
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                showMessage({
                    message: 'Canceled from single doc picker',
                    description: "Success",
                    type: "success",
                })
            } else {
                showMessage({
                    message: R.join(' ',
                        [
                            'Unknown Error: ',
                            R.toString(err)
                        ]
                    ),
                    description: "Success",
                    type: "success",
                });
            }

            return setState(null)
        }
    }, [state])

    return [state, photo, video, document]
};
