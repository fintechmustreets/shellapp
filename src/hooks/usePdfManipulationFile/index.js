/* eslint-disable scanjs-rules/call_addEventListener */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-sort-props */
/* eslint-disable security/detect-non-literal-fs-filename */
/* eslint-disable prefer-template */
import * as R from 'ramda';
import React from 'react';
import i18n from 'localization'
import { Alert } from 'react-native'
import RNPrint from 'react-native-print';
import Share from 'react-native-share';
import RNFetchBlob from 'rn-fetch-blob';
import { getItem } from 'utils/async';

export const usePdfManipulationFile = () => {
    const documentSelect = React.useCallback(async (url, id) => {
        const state = await getItem()

        return R.cond([
            [R.equals('act'), R.always({ uri: `http://devpdf.zengine.it/ru/pdf/repair?id=${id}&token=${state}&documentType=act`, base: 'base64' })],
            [R.equals('workOrder'), R.always({ uri: `http://devpdf.zengine.it/ru/pdf/repair?id=${id}&token=${state}&documentType=workOrder`, base: "base64" })],
            [R.equals('inspection'), R.always({ uri: `http://devpdf.zengine.it/ru/pdf/inspection?id=${id}&token=${state}&documentType=order`, base: "base64" })],
            [R.equals('repair'), R.always({ uri: `http://devpdf.zengine.it/ru/pdf/repair?id=${id}&token=${state}&documentType=order`, base: "base64" })],
            [R.equals('invoice'), R.always({ uri: `http://devpdf.zengine.it/ru/pdf/invoice?id=${id}&token=${state}`, base: "base64" })],
            [R.equals('policy'), R.always({ uri: "http://devapp.zengine.it/privacy_policy.pdf", base: 'url' })],
            [R.equals('personal'), R.always({ uri: "http://devapp.zengine.it/personal_data_processing_agreement.pdf", base: 'url' })],
            [R.F, R.always({ uri: url, base: 'url' })],
            [R.T, R.always({ uri: url, base: 'url' })]
        ])(url)
    }, [])

    const shareOpen = React.useCallback((url) => {
        const data = R.assocPath(['url'], url, {
            title: 'Share file',
            failOnCancel: false,
            saveToFiles: false,
        })

        return Share.open(data)
    }, [])

    const shareFile = React.useCallback(async (url, id) => {
        const { uri, base } = await documentSelect(url, id)

        if (uri) {
            if (R.equals(base, 'base64')) {
                if (!R.includes('.pdf', uri)) {
                    return RNFetchBlob
                        .config({
                            useDownloadManager: true,
                            fileCache: true,
                            notification: false,
                        })
                        .fetch('GET', uri)
                        .then((res) => res.text())
                        .then((data) => {
                            shareOpen('data:application/pdf;base64,' + data)
                        })
                }
            }

            return shareOpen(uri)
        }
    }, [])

    const document = React.useCallback(async (url, id) => {
        const path = R.join('', [RNFetchBlob.fs.dirs.DocumentDir, '/', `shellGarage.pdf`])
        const { uri } = await documentSelect(url, id)

        if (uri) {
            if (!R.includes('.pdf', uri)) {
                return RNFetchBlob
                    .config({
                        useDownloadManager: true,
                        fileCache: true,
                        notification: false,
                    })
                    .fetch('GET', uri)
                    .then((res) => res.text())
                    .then(data => RNFetchBlob.fs.writeFile(path, data, 'base64').then(() => RNPrint.print({ filePath: path })))
            }

            return RNPrint.print({ filePath: uri })
        }
    }, [])

    const alertPdfMessage = React.useCallback(({ params }) => {
        const url = R.path(['url'])(params)
        const id = R.path(['id'])(params)

        return Alert.alert(
            i18n.t("HeaderAlert"),
            "",
            [
                { text: i18n.t('Cancel'), onPress: () => null, style: "cancel" },
                { text: i18n.t("ShowAndPrint"), onPress: () => document(url, id) },
                { text: i18n.t("Share"), onPress: () => shareFile(url, id) }
            ],
            { cancelable: false }
        )
    }, [])

    return [document, shareFile, alertPdfMessage]
}