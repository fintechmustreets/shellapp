/* eslint-disable radix */
/* eslint-disable react/jsx-curly-newline */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import { CREATE_POST, DESTROY_GARAGE, QUERYPOST, UPGRADE_POST } from 'gql/postGarage';
import { useNavigation } from 'hooks/useNavigation';
import * as R from 'ramda';
import React from 'react';
import { showMessage } from "react-native-flash-message";
import { initialState } from 'screens/Main/PostBuilder/Input/config';
import { alertDestroyConfirm } from 'utils/api/funcComponent/alertConfirm';

export const usePostCurrentHooks = ({ navigation, route }) => {
    const [current, setMoved] = useNavigation({
        navigation,
        move: 'goBack'
    });
    const [state, setState] = React.useState(initialState);

    const message = ({ message, data, error }) => {
        if (error) {
            return showMessage({
                message: data,
                description: "Error",
                type: "danger",
            })
        }
        if (data) {
            return showMessage({
                message,
                description: "Success",
                type: "success",
            })
        }
    };

    const [loadgreeting] = useLazyQuery(QUERYPOST, {
        fetchPolicy: "network-only",
        onCompleted: (data) => setState(
            R.head(
                R.path(
                    R.concat(R.keys(data), ['items'])
                )(data)
            )
        )
    });
    const [createPost] = useMutation(CREATE_POST, {
        onCompleted: (data) => {
            message({
                message: "Пост успешно создан",
                data
            });
            setMoved(current)
        },
        onError: (data) => message({
            message: "Пост успешно создан",
            data,
            error: true
        }),

    });
    const [upgradePost] = useMutation(UPGRADE_POST, {
        onCompleted: (data) => {
            message({
                message: "Пост успешно создан",
                data
            });
            setMoved(current)
        },
        onError: (data) => message({
            message: "Пост успешно создан",
            data,
            error: true
        }),
    });
    const [destroyPost] = useMutation(DESTROY_GARAGE, {
        onCompleted: (data) => {
            if (data) {
                setMoved(current);
                message({
                    message: "Пост успешно создан",
                    data
                })
            }
        },
        onError: (data) => message({
            message: "Пост успешно создан",
            data,
            error: true
        }),
    });

    React.useEffect(() => {
        if (R.path(['params', 'itemId'])(route)) {
            loadgreeting(
                R.assocPath(
                    ["variables", "where", "id", "eq"],
                    R.path(['params', 'itemId'])(route)
                )({}))
        }
    }, [route]);

    const title = 'destroy_post';
    const alertReq = (e) => R.cond([
        [R.equals('Cancel'), () => null],
        [R.equals('Destroy'), () => destroyPost({
            variables: {
                input: R.path(['params', 'id'])(e)
            }
        })],
    ])(R.path(['type'])(e));

    const create = React.useCallback((data) => createPost(
        R.assocPath(
            ['variables', 'input'], {
                garagePostTypeId: R.path(['garagePostType', 'id'])(data),
                position: parseInt(R.path(['position'])(data)),
                name: R.path(['name'])(data)
            })({})
    ), []);
    const update = React.useCallback((data) => upgradePost(
        R.assocPath(['variables', 'input'],
            R.mergeAll([{
                garagePostTypeId: R.path(['garagePostType', 'id'])(data),
                position: parseInt(R.path(['position'])(data)),
                name: R.path(['name'])(data)
            },
                { id: R.path(['id'])(data) }]))({})),
        []);

    const destroy = React.useCallback((data) => alertDestroyConfirm({
            alertReq,
            title,
            params: data
        }),
        []);

    const modify = React.useCallback((data) => setState(data), []);

    return [state, create, update, destroy, modify]
};
