/* eslint-disable no-unused-vars */
/* eslint-disable object-curly-newline */
/* eslint-disable complexity */
/* eslint-disable newline-after-var */
/* eslint-disable no-shadow */
/* eslint-disable max-statements */
/* eslint-disable newline-before-return */
import * as R from 'ramda';
import React from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import { isOdd } from 'utils/helper';

export const useVariables = ({ mode, id, route }) => {
    const [text, setText] = React.useState(null)
    const { filter } = useSelector(state => state.filter, shallowEqual);
    const billAccount = useSelector(state => R.path(['depots', 'bill', 'billAccount', 'id'])(state), shallowEqual);

    const taskVariables = React.useCallback(({ id, route, text, page }) => {
        let data
        data = {}

        const manufacturer = ["modification", "model", "manufacturer", "id"]
        if (!R.includes(text, [{}, null, ''])) {
            data = R.assocPath(['q'], text, data)
        }
        if (id) {
            data = R.mergeAll([data, id])
        }
        if (route) {
            data = R.assocPath(['status', 'eq'], route, data)
        }
        if (R.path(['author', 'id'])(filter)) {
            data = R.assocPath(['authorId', 'eq'], R.path(['author', 'id'])(filter), data)
        }
        if (R.path(['owner', 'id'])(filter)) {
            data = R.assocPath(['ownerId', 'eq'], R.path(['owner', 'id'])(filter), data)
        }
        if (R.path(['client', 'id'])(filter)) {
            data = R.assocPath(['clientId', 'eq'], R.path(['client', 'id'])(filter), data)
        }
        if (R.path(['performer', 'id'])(filter)) {
            data = R.assocPath(['performerId', 'eq'], R.path(['performer', 'id'])(filter), data)
        }
        if (R.path(manufacturer)(filter)) {
            data = R.assocPath(R.pipe(R.prepend(['vehicle']), R.append(['eq']))(manufacturer), R.path(manufacturer)(filter), data)
        }

        return {
            where: data,
            order: { id: 'desc' },
            paginate: { page: R.defaultTo(1)(page), limit: 12 }
        }
    }, [route, filter, id])

    const counterVariables = React.useCallback(({ text, page }) => {
        let data
        data = {}

        if (R.equals('Page', R.path(['type'])(text))) {
            if (R.pipe(R.path(['payload']), isOdd, R.not)(text)) {
                page = R.path(['payload'])(text)
            }
        }
        if (text) {
            data = R.assocPath(['q'], text, data)
        }

        return {
            where: data,
            order: { id: 'desc' },
            paginate: { page, limit: 12 }
        }
    }, [text, route])

    const payVariables = React.useCallback(({ page }) => (
        {
            order: { id: 'desc' },
            paginate: { page: R.defaultTo(1)(page), limit: 12 },
            where: { billAccountId: { eq: billAccount } }
        }
    ), [text, route, billAccount])

    const employeeVariables = React.useCallback((id, route, page) => {
        let data
        data = {}
        if (id) {
            data = R.mergeAll([data, id])
        }
        if (route) {
            data = R.assocPath(['status', 'eq'], route, data)
        }
        if (R.equals('Page', R.path(['type'])(text))) {
            if (R.pipe(R.path(['payload']), isOdd, R.not)(text)) {
                page = R.path(['payload'])(text)
            }
        }

        return {
            order: { id: 'desc' },
            paginate: { page: R.defaultTo(1)(page), limit: 12 },
            where: data
        }
    }, [])

    const vehicleVariables = React.useCallback(({ id, text, page }) => {
        let data = {}
        if (id) {
            data = R.mergeAll([data, id])
        }
        if (!R.includes(text, [{}, null, ''])) {
            data = R.assocPath(['q'], text, data)
        }
        if (R.path(['owner', 'id'])(filter)) {
            data = R.assocPath(['ownerId', 'eq'], R.path(['owner', 'id'])(filter), data)
        }
        if (R.path(['client', 'id'])(filter)) {
            data = R.assocPath(['clientId', 'eq'], R.path(['client', 'id'])(filter), data)
        }
        return {
            order: { id: 'desc' },
            paginate: { page: R.defaultTo(1)(page), limit: 12 },
            where: data
        }
    }, [route, filter])

    const variables = React.useCallback(({ mode, id, route, text, page }) => R.cond([
        [R.equals('repairs'), () => taskVariables({ mode, id, route, text, page })],
        [R.equals('pay'), () => payVariables({ mode, id, route, page })],
        [R.equals('employee'), () => employeeVariables({ mode, id, route, text, page })],
        [R.equals('vehicle'), () => vehicleVariables({ mode, id, route, text, page })],
        [R.equals('contragent'), () => counterVariables({ mode, id, route, text, page })],
        [R.T, R.always({ order: { id: 'desc' } })],
        [R.F, R.always({ order: { id: 'desc' } })],
    ])(mode), [mode, route, billAccount, filter])

    return [variables, text, setText]
}