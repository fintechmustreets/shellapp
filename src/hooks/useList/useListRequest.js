/* eslint-disable complexity */
/* eslint-disable newline-after-var */
/* eslint-disable no-shadow */
/* eslint-disable max-statements */
/* eslint-disable newline-before-return */
import { useLazyQuery } from '@apollo/react-hooks';
import { BILL_INVOICE } from 'gql/billAccount';
import { ALL_CLIENT } from 'gql/clients';
import {
    QUERY_COUNTERPARTIES,
    QUERY_INDIVIDUAL,
    QUERY_LEGAL,
    QUERY_SOLO
} from 'gql/depots';
import { ALL_EMPLOYERS } from 'gql/employee';
import { RESERVED_DEPOTS } from 'gql/garage';
import { QUERYPOST } from 'gql/postGarage';
import * as R from 'ramda';
import React from 'react';
import { VEHICLE_QUERY } from 'screens/Main/CarsScreen/GQL';
import { ALL_REPAIRS } from 'screens/Main/MainScreen/GQL';

export const useListRequest = () => {
    const [data, setData] = React.useState({ query: null })
    const counterRequest = React.useCallback(({ data, route }) => R.cond([
        [R.equals('LEGAL_ENTITY'), () => legalGreeting({ variables: R.defaultTo({ order: { id: 'desc' } })(data) })],
        [R.equals('INDIVIDUAL'), () => indivGreeting({ variables: R.defaultTo({ order: { id: 'desc' } })(data) })],
        [R.equals('SOLE_TRADER'), () => soloGreeting({ variables: R.defaultTo({ order: { id: 'desc' } })(data) })],
        [R.F, () => counterGreeting({ variables: R.defaultTo({ order: { id: 'desc' } })(data) })],
        [R.T, () => counterGreeting({ variables: R.defaultTo({ order: { id: 'desc' } })(data) })],
    ])(route), [])

    const request = React.useCallback(({ variables: data, mode, route }) => {
        setData({ query: null })

        return R.cond([
            [R.equals('repairs'), () => taskGreeting({ variables: R.defaultTo({ order: { id: 'desc' } })(data) })],
            [R.equals('pay'), () => billGreeting({ variables: data })],
            [R.equals('client'), () => clientGreeting({ variables: R.defaultTo({ order: { id: 'desc' } })(data) })],
            [R.equals('employee'), () => employeeGreeting({ variables: data })],
            [R.equals('contragent'), () => counterRequest({ data, route })],
            [R.equals('reserved'), () => reserveGreeting({ variables: R.defaultTo({ order: { id: 'desc' } })(data) })],
            [R.equals('post'), () => postGreeting({ variables: R.defaultTo({ order: { id: 'desc' } })(data) })],
            [R.equals('vehicle'), () => vehicleGreeting({ variables: R.defaultTo({ order: { id: 'desc' } })(data) })],
            [R.T, () => null],
            [R.F, () => null],
        ])(mode)
    }, [])

    const [taskGreeting] = useLazyQuery(ALL_REPAIRS, {
        fetchPolicy: "no-cache",
        onCompleted: (data) => {
            if (data) {
                setData({ query: R.path(R.keys(data))(data) })
            }
        },
        onError: () => setData({ query: null })
    })
    const [billGreeting] = useLazyQuery(BILL_INVOICE, {
        fetchPolicy: "no-cache",
        onCompleted: (data) => {
            if (data) {
                setData({ query: R.path(R.keys(data))(data) })
            }
        },
        onError: () => setData({ query: null })
    })
    const [counterGreeting] = useLazyQuery(QUERY_COUNTERPARTIES, {
        fetchPolicy: "no-cache",
        onCompleted: (data) => {
            if (data) {
                setData({ query: R.path(R.keys(data))(data) })
            }
        },
        onError: () => setData({ query: null })
    })
    const [indivGreeting] = useLazyQuery(QUERY_INDIVIDUAL, {
        fetchPolicy: "no-cache",
        onCompleted: (data) => {
            if (data) {
                setData({ query: R.path(R.keys(data))(data) })
            }
        },
        onError: () => setData({ query: null })
    })
    const [legalGreeting] = useLazyQuery(QUERY_LEGAL, {
        fetchPolicy: "no-cache",
        onCompleted: (data) => {
            if (data) {
                setData({ query: R.path(R.keys(data))(data) })
            }
        },
        onError: () => setData({ query: null })
    })
    const [soloGreeting] = useLazyQuery(QUERY_SOLO, {
        fetchPolicy: "no-cache",
        onCompleted: (data) => {
            if (data) {
                setData({ query: R.path(R.keys(data))(data) })
            }
        },
        onError: () => setData({ query: null })
    })
    const [clientGreeting] = useLazyQuery(ALL_CLIENT, {
        fetchPolicy: "no-cache",
        onCompleted: (data) => {
            if (data) {
                setData({ query: R.path(R.keys(data))(data) })
            }
        },
        onError: () => setData({ query: null })
    })
    const [employeeGreeting] = useLazyQuery(ALL_EMPLOYERS, {
        fetchPolicy: "no-cache",
        onCompleted: (data) => {
            if (data) {
                setData({ query: R.path(R.keys(data))(data) })
            }
        },
        onError: () => setData({ query: null })
    })
    const [reserveGreeting] = useLazyQuery(RESERVED_DEPOTS, {
        fetchPolicy: "no-cache",
        onCompleted: (data) => {
            if (data) {
                setData({ query: R.path(R.keys(data))(data) })
            }
        },
        onError: () => setData({ query: null })
    })
    const [postGreeting] = useLazyQuery(QUERYPOST, {
        fetchPolicy: "no-cache",
        onCompleted: (data) => {
            if (data) {
                setData({ query: R.path(R.keys(data))(data) })
            }
        },
        onError: () => setData({ query: null })
    })
    const [vehicleGreeting] = useLazyQuery(VEHICLE_QUERY, {
        fetchPolicy: "no-cache",
        onCompleted: (data) => {
            if (data) {
                setData({ query: R.path(R.keys(data))(data) })
            }
        },
        onError: () => setData({ query: null })
    })

    return [data, request]
}