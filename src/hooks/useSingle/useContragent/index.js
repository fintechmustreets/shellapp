/* eslint-disable react/jsx-indent-props */
import { useLazyQuery } from '@apollo/react-hooks';
import * as R from 'ramda';
import React from 'react';
import { showMessage } from "react-native-flash-message";
import { useDispatch } from 'react-redux';
import { QUERY_COUNTERPARTY } from 'screens/Contragent/GQL';
import { MainScreen } from 'utils/routing/index.json';

export const useCounterparityNavigation = ({ navigation }) => {
    const ROUTER = MainScreen.EditContragent;
    const ROUTERCONTRAGENT = MainScreen.Contragent;
    const dispatchRedux = useDispatch()

    const [loadGreeting] = useLazyQuery(QUERY_COUNTERPARTY, {
        fetchPolicy: "network-only",
        onCompleted: (data) => {
            if (data) {
                dispatchRedux({
                    type: 'AGENT_UPDATE',
                    payload: R.path([R.keys(data)])(data)
                });
            }
        },
        onError: (data) => {
            dispatchRedux({
                type: 'AGENT_UPDATE',
                payload: {}
            });
            showMessage({
                message: R.toString(data),
                description: "Error",
                type: "danger",
            });
        }
    });

    const Contragent = React.useCallback((data) => {
        if (R.equals('Edit', R.path(['type'])(data))) {
            loadGreeting({
                variables: { where: { id: { eq: R.path(['payload'])(data) } } }
            })
        }

        return R.cond([
            [R.equals('Create'), () => {
                dispatchRedux({
                    type: 'AGENT_UPDATE',
                    payload: {}
                });
                navigation.navigate(ROUTER)
            }],
            [R.equals('Edit'), () => navigation.navigate(ROUTER)],
        ])(R.path(['type'])(data))
    }, [])

    const ListScreen = React.useCallback((data) => navigation.navigate(
        ROUTERCONTRAGENT,
        R.defaultTo(null)(data)
    ), [])

    return [Contragent, ListScreen]
}
