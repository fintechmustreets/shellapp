/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-indent-props */
import { useLazyQuery } from '@apollo/react-hooks';
import * as R from 'ramda';
import React from 'react';
import { CURRENT_CLIENT } from 'screens/Main/ClientScreen/GQL';
import { showMessage } from 'react-native-flash-message';

export const useCurrentClient = ({ id }) => {
    const [data, setData] = React.useState({})
    const ref = React.useRef()

    const [loadgreeting] = useLazyQuery(CURRENT_CLIENT, {
        onCompleted: (data) => setData(R.path(R.keys(data))(data)),
        onError: (data) => showMessage({
            message: R.toString(data),
            description: "Error",
            type: "danger",
        })
    })

    React.useEffect(() => {
        if (id) {
            if (R.pipe(R.equals(ref.current), R.not)(id)) {
                loadgreeting({ variables: { where: { id } } })
            }
        }
    }, [id])

    return [data]
}