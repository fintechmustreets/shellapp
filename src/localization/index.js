import * as RNLocalize from 'react-native-localize';
import i18n from 'i18n-js'
// import memoize from 'lodash.memoize'

import ruRU from './ru-RU'
import enUS from './en-US'

const locales = RNLocalize.getLocales();

if (Array.isArray(locales)) {
    i18n.locale = locales[0].languageCode;
}

i18n.translations = {
    default: ruRU,
    en: enUS,
    ru: ruRU,
};

i18n.fallbacks = true;

export default i18n;
