/* eslint-disable max-len */
/* eslint-disable multiline-ternary */
/* eslint-disable no-unused-vars */
/* eslint-disable no-confusing-arrow */
/* eslint-disable id-length */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-after-var */
/* eslint-disable no-else-return */
/* eslint-disable no-console */
/* eslint-disable scanjs-rules/call_addEventListener */
/* eslint-disable max-statements */
import { useFileAttach } from 'hooks/useFileAttach';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import {
    TouchableOpacity,
    View
} from 'react-native';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const TitleSelecter = ({ hack }) => {
    const { task } = useSelector(state => state.task, shallowEqual);
    const { goods } = useSelector(state => state.goods, shallowEqual);

    const modificator = hack ? goods : task

    const [attacments] = useFileAttach({ hack });
    const dispatchRedux = useDispatch()

    console.log(12312)

    const title = (x) => R.join(' ', [
        i18n.t(x),
        R.isNil(attacments(x)) ? null : R.length(attacments(x))
    ])

    return (
        <CardComponents
            style={[{
                margin: 1,
                marginRight: 10,
                marginLeft: 10
            }, styles.shadow]}
        >
            <RowComponents style={{ margin: 10 }}>
                {R.addIndex(R.map)((x, key) => (
                    <View
                        key={key}
                    >
                        <TouchableOpacity
                            onPress={() => dispatchRedux({ type: hack ? "GOODS_SCREEN" : "TASK_SCREEN", payload: x })}
                            style={R.equals(R.defaultTo('image')(R.path(['screen'])(modificator)), x) ? { borderColor: 'red', paddingBottom: 5, borderBottomWidth: 3 } : { paddingBottom: 5 }}
                        >
                            <LabelText
                                color={ColorCard('classic').font}
                                fonts="ShellMedium"
                                items="SDescription"
                            >
                                {title(x)}
                            </LabelText>
                        </TouchableOpacity>
                    </View>
                ))(['image', 'video', 'document', 'application'])}
            </RowComponents>
        </CardComponents>)
}
