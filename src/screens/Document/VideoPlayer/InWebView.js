/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import { useMutation } from '@apollo/react-hooks';
import { DESTROY_FILE } from 'gql/depots';
import { useDownload } from 'hooks/useDownload';
import { useTaskHook } from 'hooks/useTaskHooks';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import { IconButton, Colors } from 'react-native-paper';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Styles, styles } from 'screens/Main/AddGoods/helper/style';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { alertDestroyConfirm } from 'utils/api/funcComponent/alertConfirm';
import { ColorCard } from 'utils/helper';
import { WebView } from 'react-native-webview';
import { OfficeViewer } from '@sishuguojixuefu/react-native-office-viewer'
import VideoPlayer from 'screens/Document/VideoPlayer';
import ImageViewer from 'react-native-image-zoom-viewer';

export const InWebView = ({ mode }) => {
    const { task } = useSelector(state => state.task, shallowEqual);
    const dispatchRedux = useDispatch();

    const QWeqweqw = R.cond([
        [R.equals('inWebView'), () => (
            <WebView
                source={{ uri: R.path([mode, 'file', 'url'])(task) }}
                style={{ flex: 1 }}
            />
        )],
        [R.equals('inOfficeView'), () => (
            <OfficeViewer
                containerStyle={{ flexGrow: 1, marginTop: 10 }}
                source={{ uri: R.path([mode, 'file', 'url'])(task) }}
            />
        )],
        [R.equals('videoView'), () => (
            <VideoPlayer
                video={R.path([mode, 'file', 'url'])(task)}
            />
        )],
        [R.equals('imageView'), () => (
            <ImageViewer
                enableImageZoom
                getMenu={null}
                imageUrls={[{ url: R.path([mode, 'file', 'url'])(task) }]}
                onSwipeDown={() => dispatchRedux({ type: "TASK_UPDATE", payload: R.omit([mode])(task) })}
            />
        )],
    ])

    if (R.pipe(R.path([mode]), R.isNil, R.not)(task)) {
        return (
            <View style={{ flex: 1 }}>
                {QWeqweqw(mode)}
                <FooterPage mode={mode} />
            </View>
        )
    }

    return (
        <View />
    );
}

export const FooterPage = (props) => {
    const { mode } = props
    const { task } = useSelector(state => state.task, shallowEqual);
    const dispatchRedux = useDispatch();
    const [update] = useTaskHook()
    const [download] = useDownload()
    const file = R.path([mode])(task)

    const [destroyFile] = useMutation(DESTROY_FILE, {
        onCompleted: () => {
            update('update', { id: R.path(['id'])(task) })
            showMessage({
                message: "что то удален успешно",
                description: "Error",
                type: "success",
            })
        },
        onError: (data) => console.log(data)
    });

    const title = 'destroy_post';
    const alertReq = (e) => R.cond([
        [R.equals('Cancel'), () => null],
        [R.equals('Destroy'), () => {
            destroyFile({ variables: { id: R.path(['id'])(file) } })
        }],
    ])(R.path(['type'])(e));

    return (
        <RowComponents>
            <TouchableOpacity
                onPress={() => {
                    alertDestroyConfirm({
                        alertReq,
                        title,
                    })
                }}
                style={[Styles.snap, { left: 0, backgroundColor: 'red' }]}
            >
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SDescription"
                    style={styles.container}
                >
                    {i18n.t("DESTROY")}
                </LabelText>
            </TouchableOpacity>
            <IconButton
                color={Colors.black500}
                icon="download"
                onPress={() => download(file)}
                size={20}
            />
            <TouchableOpacity
                onPress={() => dispatchRedux({ type: 'TASK_UPDATE', payload: R.omit([mode])(task) })}
                style={[Styles.snap, { left: 0, backgroundColor: 'grey' }]}
            >
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SDescription"
                    style={styles.container}
                >
                    {i18n.t("CLOSED")}
                </LabelText>
            </TouchableOpacity>
        </RowComponents>
    )
}
