/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { Header } from 'screens/TaskEditor/components/header';
import { VisualTable } from 'screens/TaskEditor/components/table/component';
import { MainScreen } from 'utils/routing/index.json';
import { useNavigation } from 'hooks/useNavigation'
import { useDispatch } from 'react-redux';
// const [current, setMoved] = useNavigation({ navigation, move: 'toggle' })

export const ContragentScreen = ({ navigation, route }) => {
    const [current, setMoved] = useNavigation({ navigation, move: 'toggle' });
    const dispatchRedux = useDispatch()

    return (
        <Header
            check={{
                name: ["INN", "NAME", "ID"],
                current: "NAME",
            }}
            dispatch={setMoved}
            menu={{ title: "ListContAgent" }}
            svg={current}
        >
            <VisualTable
                button
                dispatch={(e) => {
                    if (R.equals(R.path(['type'])(e), 'Edit')) {
                        return navigation.navigate(MainScreen.EditContragent, { itemId: R.path(['payload'])(e), mode: "Edit", type: route })
                    }
                    if (R.equals(R.path(['type'])(e), "Create")) {
                        dispatchRedux({ type: 'AGENT_UPDATE', payload: {} })

                        return navigation.navigate(MainScreen.EditContragent)
                    }
                }}
                mode="contragent"
                navigation={navigation}
                route={R.path(['params', 'list'])(route)}
                unfilter
            />
        </Header>
    )
};
