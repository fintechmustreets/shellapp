/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
import { useQuery } from '@apollo/react-hooks';
import * as R from 'ramda';
import React from 'react';
import { Linking, View } from 'react-native';
import { SplashScreen } from 'screens/Splash';
import { TableModel } from 'screens/TaskEditor/components/table';
import { MainScreen } from 'utils/routing/index.json';

export const AppScreen = ({
    query, state, variables, navigation
}) => {
    const ROUTER = MainScreen.EditContragent;
    const { error, loading, data } = useQuery(query, {
        variables,
        fetchPolicy: "network-only"
    });

    if (error) return (
        <SplashScreen
            msg={R.toString(error)}
            state="Error"
            testID="Error"
        />
    );
    if (loading) return (
        <SplashScreen
            state="Loading"
            testID="Loading"
        />
    );
    return (
        <View style={{ flex: 1 }}>
            <TableModel
                data={R.path(R.concat(R.keys(data), ['items']))(data)}
                dispatch={(e) => {
                    if (R.equals(R.path(['type'])(e), 'Phone')) {
                        return Linking.openURL(`tel:${R.path(['phone'])(e)}`)
                    }
                    if (R.equals(R.path(['type'])(e), 'Edit')) {
                        return navigation.navigate(ROUTER, { itemId: R.path(['payload'])(e), mode: "Edit", type: R.path(['checker'])(state) })
                    }
                    if (R.equals(R.path(['type'])(e), "Create")) {
                        return navigation.navigate(ROUTER, { itemId: null, mode: null, type: R.path(['checker'])(state) })
                    }
                }}
                header={R.path(['checker'])(state)}
                mode="Date"
                style={{ flex: 1 }}
            />
        </View>
    )
};