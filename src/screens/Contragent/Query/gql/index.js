/* eslint-disable arrow-body-style */
import {
    QUERY_COUNTERPARTIES, QUERY_INDIVIDUAL, QUERY_LEGAL, QUERY_SOLO
} from 'gql/depots';
import * as R from 'ramda';

export const query = R.cond([
    [R.equals("COUNTREPARITY"), () => {
        return QUERY_COUNTERPARTIES
    }],
    [R.equals("INDIVIDUAL"), () => {
        return QUERY_INDIVIDUAL
    }],
    [R.equals("SOLE_TRADER"), () => {
        return QUERY_SOLO
    }],
    [R.equals("LEGAL_ENTITY"), () => {
        return QUERY_LEGAL
    }],
]);
