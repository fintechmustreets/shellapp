/* eslint-disable no-undefined */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-multi-comp */
import * as R from 'ramda';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import i18n from 'localization';

export const ContragentMistake = ({ state }) => {
  const counter = R.equals('LEGAL_ENTITY', R.path(['type'])(state)) ? 10 : 12;
  const mistake = (x) => {
    const isOdd = (x) => R.pipe(R.includes(x))(['', null, undefined, ' ']);

    return [
      isOdd(R.path(['type'])(x)),
      R.path(['inn'])(x) ? R.pipe(R.path(['inn']), R.splitEvery(1), R.length, R.equals(counter), R.not)(x) : true,
      isOdd(R.path(['name'])(x)),
    ]
  };

  const obj = [
    'nottype',
    'notinn',
    'notname',
    'notemail',
    'notphone',
  ];

  return (
    <View>
      {R.addIndex(R.map)((x, key) => {
        if (R.equals(x, false)) {
          return (<View />)
        }

        return (
          <View
            key={key}
            style={styles.items}
          >
            <LabelText
              color={ColorCard('classic').font}
              fonts="ShellMedium"
              items="SSDescription"
            >
              {i18n.t(obj[key])}
            </LabelText>
          </View>
        )
      })(mistake(state))}
    </View>
  )
};

const styles = StyleSheet.create({
  items: {
    height: 35,
    borderLeftWidth: 2,
    paddingLeft: 10,
    borderColor: 'red',
    margin: 2
  }
});