/* eslint-disable max-statements */
/* eslint-disable scanjs-rules/call_connect */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-unused-vars */
import { useAgentType } from 'hooks/useAgentType';
import { useContragentHooks } from 'hooks/useContragentHooks';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { CheckButton, styleContr } from 'screens/Contragent/Input/config';
import { ContragentMistake } from 'screens/Contragent/Input/ContragentMistake';
import { InputContragent } from 'screens/Contragent/Input/EditContragent/InputContragent';
import { ScrolerView } from 'screens/Contragent/style';
import { Header } from 'screens/TaskEditor/components/header';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { uiColor } from 'styled/colors/uiColor.json';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { alertDestroyConfirm } from 'utils/api/funcComponent/alertConfirm';
import { phoneMedd } from 'utils/helper';
import { MainScreen } from 'utils/routing/index.json';

export const ContragentEditScreen = (props) => {
    const { navigation, route } = props;
    const [agentType] = useAgentType()
    const [state, contragentRequest, setState] = useContragentHooks();
    const { agent } = useSelector(state => state.agent, shallowEqual);
    const dispatchRedux = useDispatch();
    const title = "destroy_contragent";
    const ROUTERCONTRAGENT = MainScreen.Contragent;
    const linker = R.path(['id'])(agent) ? ["SAVE", "ADDED_NEW"] : ["ADDEDS"];

    const success = (event) => R.cond([
        [R.equals('GoBack'), () => navigation.navigate(ROUTERCONTRAGENT)],
        [R.equals('ADDED_NEW'), () => dispatchRedux({ type: "AGENT_UPDATE", payload: {} })],
        [R.equals('ADDEDS'), () => contragentRequest('create')],
        [R.equals('SAVE'), () => contragentRequest('update')],
        [R.equals('FIND'), () => contragentRequest('find')],
    ])(R.path(['type'])(event));

    const alertReq = (e) => R.cond([
        [R.equals('Cancel'), () => null],
        [R.equals('Destroy'), () => {
            contragentRequest('destroy');
            navigation.goBack()
        }],
    ])(R.path(['type'])(e));

    const Title = R.path(['id'])(agent) ? <HeaderApp
        button="DESTROY"
        dispatch={() => alertDestroyConfirm({
            alertReq,
            title
        })}
        empty={R.pipe(R.path(['id']), R.not)(agent)}
        header
        title={R.join('\n', [
            R.path(['name'])(agent),
            phoneMedd(R.path(['phone'])(agent))
        ])}
    /> : <View />

    return (
        <Header
            dispatch={() => navigation.goBack()}
            menu={{ title: "ListContAgent" }}
            svg="goBack"
        >
            <ScrolerView
                contentContainerStyle={styleContr.scroll}
            >
                {Title}
                <InputContragent
                    style={{ backgroundColor: uiColor.White }}
                />
                <ContragentMistake state={R.mergeAll([agentType, agent])} />
                <RowComponents>
                    {R.addIndex(R.map)(x => (
                        <TouchesState
                            key={x}
                            color="Yellow"
                            disabled={CheckButton(agent)}
                            flex={1}
                            onPress={() => success({ type: x })}
                            title={R.equals('ADDEDS', x) ? "SAVE" : x}
                        />
                    ))(linker)}
                </RowComponents>
            </ScrolerView>
        </Header>
    )
};
