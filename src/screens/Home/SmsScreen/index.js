/* eslint-disable no-unused-expressions */
/* eslint-disable react/no-multi-comp */
/* eslint-disable scanjs-rules/call_connect */
/* eslint-disable max-statements */
/* eslint-disable scanjs-rules/call_setTimeout */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-wrap-multilines */
import * as R from 'ramda';
import React, { useEffect, useState } from 'react';
import { TouchableOpacity, View, StyleSheet } from 'react-native';
import {
    connect,
    shallowEqual,
    useDispatch,
    useSelector
} from 'react-redux';
import i18n from 'localization'
import { TouchesState } from 'styled/UIComponents/UiButtom';
import HeaderComponentsProps from 'uiComponents/HeaderComponents';
import NameSpaceAppLoading from 'uiComponents/NameSpaceAppLoading';
import { LabelText, SplashContainer } from 'uiComponents/SplashComponents';
import { AuthSmsCode, CreateSmsCode } from 'utils/api';
import { setItem } from 'utils/async';
import { InputComponents } from 'utils/component';
import { ColorCard, phoneMedd } from 'utils/helper';
import { HeaderScreen, RegisterScreen } from 'utils/routing/index.json';
import { navBack } from '../utils';

const SplashScreen = ({ navigation }) => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);
    const [time, setTime] = useState(false);
    const [sms, setSms] = useState({ error: true });
    const [update, setUpdate] = React.useState(null)
    const user = useSelector(state => state.profile, shallowEqual);

    const timerFuncion = () => {
        if (loading) {
            if (time < 1) {
                return setLoading('new')
            }
            setTimeout(() => setTime(time - 1), 1000)
        }
    };

    useEffect(() => {
        timerFuncion()
    }, [loading, time]);

    React.useEffect(() => {
        if (update) {
            navigation.navigate(HeaderScreen.Main)
        }

        return () => setLoading(null)
    }, [update])

    const disabled = R.pipe(R.path(['error']), R.isNil, R.not)(sms)
    const changeText = (change, error) => setSms({
        error,
        sms: change,
        regex: R.pipe(
            R.splitEvery(1),
            R.reject(isOdd),
            R.join('')
        )(change)
    })

    const success = async () => {
        if (R.equals(false, time)) {
            setTime(60);
        }
        setLoading(true);
        const data = await AuthSmsCode(R.path(['phone', 'regex'], user), R.path(['regex'])(sms));
        const { token } = JSON.parse(R.defaultTo({ token: null })(data))

        if (token) {
            dispatch({
                type: 'PROFILE_LOGIN',
                payload: { jswToken: token }
            });
            setItem(token).then(() => navigation.navigate(RegisterScreen.Phone))
        }
        setUpdate(token)
    }

    const onPress = React.useCallback(async () => {
        if (time) return null
        const data = await CreateSmsCode(R.path(['phone', 'regex'], user))

        if (data) {
            setTime(60);
            setLoading(true)
        }
    }, [time])

    const AppComponents = () => {
        const settingsText = time ? [
            i18n.t('ResendSms'),
            R.join(' ', [i18n.t('through'), time, 'сек.'])
        ] : [
                i18n.t('ResendSms'),
                phoneMedd(R.path(['phone', 'regex'], user))
            ]

        return (
            <TouchableOpacity
                onPress={onPress}
            >
                {R.addIndex(R.map)((x, key) => (
                    <LabelText
                        key={key}
                        color={ColorCard('blue').font}
                        fonts="ShellBold"
                        items="SDescription"
                        style={{ textAlign: 'center' }}
                    >
                        {x}
                    </LabelText>
                ))(settingsText)}
            </TouchableOpacity>
        )
    }

    return (
        <SplashContainer>
            <HeaderComponentsProps
                mode="Home"
                onPress={(click) => navBack(click, navigation, dispatch)}
            />
            <View
                style={styles.items}
            >
                <NameSpaceAppLoading absolute />
                <View
                    style={{ padding: 10 }}
                >
                    <View
                        style={styles.padding}
                    >
                        <InputComponents
                            {...smsInput}
                            onChangeText={changeText}
                            storage={sms}
                        />
                    </View>
                    <TouchesState
                        color="Yellow"
                        disabled={disabled}
                        flex={0}
                        font='ShellBold'
                        onPress={success}
                        title="AUTH"
                    />
                    {AppComponents()}
                </View>
            </View>
        </SplashContainer >)
};

const styles = StyleSheet.create({
    items: {
        flex: 1,
        marginTop: 0,
        margin: 10,
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        borderWidth: 0
    },
    padding: {
        paddingLeft: 10,
        paddingRight: 10,
    }
})

const isOdd = (x) => R.anyPass([R.equals("-")])(x);

const smsInput = {
    id: "ИМЯ",
    value: ["sms"],
    error: "Error message",
    label: 'sms',
    example: "",
    path: 'sms',
    main: true,
    mask: "[0000]",
    Lenght: 4,
    keyboardType: "numeric",
};

const mapStateToProp = (state) => ({
    store: state,
    profile: state.profile,
});

export default connect(mapStateToProp)(SplashScreen);
