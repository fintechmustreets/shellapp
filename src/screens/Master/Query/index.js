/* eslint-disable react/no-multi-comp */
/* eslint-disable max-statements */
/* eslint-disable newline-after-var */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable newline-before-return */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native'
import { Header } from 'screens/TaskEditor/components/header';
import { VisualTable } from 'screens/TaskEditor/components/table/component';
import { useNavigation } from 'hooks/useNavigation'
import { useEmployee } from 'hooks/useEmployee'

export const MasterScreen = ({ navigation }) => {
    const [update] = useEmployee({ navigation })
    const [current, setMoved] = useNavigation({ navigation, move: 'toggle' });

    const success = (e) => {
        update(R.path(['payload'])(e))
    };

    return (
        <Header
            dispatch={setMoved}
            menu={{ title: "masters" }}
            svg={current}
        >
            <View style={{ flex: 1 }}>
                <VisualTable
                    button
                    dispatch={success}
                    mode="employee"
                    navigation={navigation}
                    unsearch
                    unfilter
                />
            </View>
        </Header >
    )
};
