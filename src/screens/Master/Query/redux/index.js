import * as R from 'ramda';
import createSensitiveStorage from 'redux-persist-sensitive-storage';

export const masterStorage = createSensitiveStorage({
    keychainService: 'masterChainKeychain',
    sharedPreferencesName: 'masterChainSharedPrefs',
});

export const depotsGarageStorage = createSensitiveStorage({
    keychainService: 'masterChainKeychain',
    sharedPreferencesName: 'masterChainSharedPrefs',
});

export const aliaseStorage = createSensitiveStorage({
    keychainService: 'masterChainKeychain',
    sharedPreferencesName: 'masterChainSharedPrefs',
});

export const employeeStorage = createSensitiveStorage({
    keychainService: 'masterChainKeychain',
    sharedPreferencesName: 'masterChainSharedPrefs',
});

export const filterStorage = createSensitiveStorage({
    keychainService: 'filterChainKeychain',
    sharedPreferencesName: 'filterChainSharedPrefs',
});

export const masterPersistConfig = {
    key: 'master',
    storage: masterStorage,
};

export const taskStorage = createSensitiveStorage({
    keychainService: 'taskChainKeychain',
    sharedPreferencesName: 'taskChainSharedPrefs',
});

export const depotsGoodsStorage = createSensitiveStorage({
    keychainService: 'goodsChainKeychain',
    sharedPreferencesName: 'goodsChainSharedPrefs',
});

export const taskPersistConfig = {
    key: 'task',
    storage: taskStorage,
};

export const initialState = {};

export const taskReducer = (state = initialState, action) => {
    switch (action.type) {
        case "TASK_UPDATE":
            return { task: action.payload };
        case "TASK_SCREEN":
            return { task: R.assocPath(['screen'], action.payload)(R.path(['task'])(state)) };
        default:
            return state
    }
};

export const agentStorage = createSensitiveStorage({
    keychainService: 'agentChainKeychain',
    sharedPreferencesName: 'agentChainSharedPrefs',
});

export const agentPersistConfig = {
    key: 'agent',
    storage: agentStorage,
};

export const agentReducer = (state = initialState, action) => {
    switch (action.type) {
        case "AGENT_UPDATE":
            return { agent: action.payload };
        case "AGENT_phone":
            return {
                agent: R.mergeAll([
                    R.path(['agent'])(state),
                    R.assocPath(['phone'],
                        `+7${R.path(R.keys(action.payload))(action.payload)}`, {})])
            };
        case "AGENT":
            return {
                agent: R.mergeAll([
                    R.defaultTo({})(
                        R.path(['agent'])(state)),
                    action.payload])
            };
        case "AGENT_BANK":
            return {
                agent:
                    R.mergeAll([
                        R.path(['agent'])(state), {
                            bankDetails: action.payload
                        }
                    ])
            };
        case "AGENT_DESTROY_BANK":
            return {
                agent: R.mergeAll([R.path(['agent'])(state), {
                    bankDetails:
                        R.without([action.payload])([
                            R.path(['agent', 'bankDetails'])(state)
                        ])
                }])
            };
        default:
            return state
    }
};

export const depotsGaragePersistConfig = {
    key: 'aliase',
    storage: depotsGarageStorage,
};

export const depotsGarageReducer = (state = initialState, action) => {
    switch (action.type) {
        case "CURRENT_GARAGE":
            return { depots: action.payload };
        case "CURRENT_SCREEN":
            return { depots: R.mergeAll([state.depots, action.payload]) };
        default:
            return state
    }
};

export const depotsGoodsPersistConfig = {
    key: 'aliase',
    storage: depotsGoodsStorage,
};

export const depotsGoodsReducer = (state = initialState, action) => {
    switch (action.type) {
        case "ADD_GOODS":
            return { goods: action.payload };
        case "GOODS_SCREEN":
            return { goods: R.assocPath(['screen'], action.payload)(R.path(['goods'])(state)) };
        default:
            return state
    }
};

export const aliasePersistConfig = {
    key: 'aliase',
    storage: aliaseStorage,
};

export const aliaseReducer = (state = initialState, action) => {
    switch (action.type) {
        case "ALIASE_ADD":
            return { aliase: action.payload };
        default:
            return state
    }
};

export const employeeSPersistConfig = {
    key: 'employee',
    storage: employeeStorage,
};

export const employeeSReducer = (state = initialState, action) => {
    switch (action.type) {
        case "employee_update":
            return { employees: action.payload };
        case "employee_Initial":
            return { employees: action.payload };
        default:
            return state
    }
};

export const filterPersistConfig = {
    key: 'filter',
    storage: filterStorage,
};

export const filterReducer = (state = initialState, action) => {
    switch (action.type) {
        case "INITIAL":
            return { filter: {} };
        case "FILTER_TEXT":
            return { filter: { text: action.payload } };
        case "FILTER_ADD":
            return { filter: R.mergeAll([state.filter, action.payload]) };
        case "FILTER_DESTROY_MODELS":
            return {
                filter: R.mergeAll([R.path(['filter'])(state), {
                    "modification": {
                        "model":
                        {
                            "manufacturer": {
                                "id": R.path(['filter', 'modification', 'model', 'manufacturer', 'id'])(state),
                                "name": R.path(['filter', 'modification', 'model', 'manufacturer', 'name'])(state),
                            },
                            "manufacturerId": R.path(['filter', 'modification', 'model', 'manufacturerId'])(state),
                        }
                    }
                }])
            };
        case "FILTER_DESTROY":
            return { filter: R.omit([action.payload], state.filter) };
        default:
            return state
    }
};

export const masterReducer = (state = initialState, action) => R.cond([
    [R.equals('MASTERS_ADD'), R.always(action.payload)],
    [R.equals('MASTERS_MERGE'), R.mergeRight(state, action.payload)],
    [R.equals('MASTERS_REMOVE'), R.always(initialState)],
    [R.T, R.always(state)],
])(action.type);