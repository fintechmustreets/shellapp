import * as R from 'ramda';
import { initialInputState } from '../reducer';

// <== mapperData ==>
export const mapperData = R.cond([
    [R.equals('edit'), R.always({
        title: 'headerMasterEdit',
        input: initialInputState,
        TitleBTN: 'updateEdit',
        TitleCNC: 'cancelBack',
    })],
    [R.either(R.isNil, R.isEmpty), R.always({
        title: 'masterCreateUser',
        input: initialInputState,
        TitleBTN: 'masterCreateUser',
        TitleCNC: 'cancelBack',
    })],
]);

// <== InitialStage ==>
export const initialStage = (data, user) => ({
    name: {
        first: R.path(["name", "first"])(data),
        last: R.path(["name", "last"])(data),
        middle: R.path(["name", "middle"])(data)
    },
    blocked: R.path(["blocked"])(data),
    phone: R.path(["phone"])(data),
    garageId: R.path(["garageId"])(data),
    role: R.path(["role"])(data),
    id: user,
    error: { phone: null }
});
