/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import { GaragesId, UPGRADE_GARAGE } from 'gql/autoService';
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { Header } from 'screens/TaskEditor/components/header';
import { uiColor } from 'styled/colors/uiColor.json';
import { showMessage } from 'react-native-flash-message';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { variables } from './components/config';
import { Garage } from './components/Garage';
import { isOdd } from 'utils/helper'
import { useNavigation } from 'hooks/useNavigation'
import { ContragentMistake } from './ContragentMistake'

export const AutoService = ({ navigation }) => {
    const [current, setMoved] = useNavigation({ navigation, move: 'toggle' });
    const { agent } = useSelector(state => state.agent, shallowEqual);
    const dispatchRedux = useDispatch();

    const [loadgreeting] = useLazyQuery(GaragesId, {
        onCompleted: (data) => {
            if (data) return dispatchRedux({
                type: "AGENT",
                payload: R.head(R.path(['garages', 'items'])(data))
            });
            showMessage({
                message: "АВТОСЕРВИС УСПЕШНО ОБНОВЛЕН",
                description: "Success",
                type: "success",
            });
        },
        onError: (data) => {
            showMessage({
                message: R.toString(data),
                description: "Error",
                type: "danger",
            });
        },
        fetchPolicy: "network-only"
    });

    React.useEffect(() => {
        loadgreeting()
    }, [navigation]);

    const [upgradeGarage] = useMutation(UPGRADE_GARAGE, {
        onCompleted: (data) => {
            if (data) {
                dispatchRedux({
                    type: "AGENT",
                    payload: R.path([R.head(data)])(data)
                });
                showMessage({
                    message: "АВТОСЕРВИС УСПЕШНО ОБНОВЛЕН",
                    description: "Success",
                    type: "success",
                });
            }
        },
        onError: (data) => {
            showMessage({
                message: R.toString(data),
                description: "Success",
                type: "success",
            });
        },
    });

    const disabled = (agent) => [
        isOdd(R.path(['name'])(agent)),
        isOdd(R.path(['phone'])(agent)),
        isOdd(R.path(['email'])(agent)),
        isOdd(R.path(['settings', 'costPerHour', 'amount'])(agent)),
        isOdd(R.path(['checkingAccount'])(agent)),
        isOdd(R.path(['correspondentAccount'])(agent)),
        isOdd(R.path(['rcbic'])(agent)),
        isOdd(R.path(['inn'])(agent)),
        isOdd(R.path(['kpp'])(agent)),
        isOdd(R.path(['psrn'])(agent)),
        isOdd(R.path(['settings', 'defaultPartSurcharge'])(agent)),
    ];

    return (
        <Header
            dispatch={setMoved}
            menu={{ title: "AutoServices" }}
            search
            svg={current}
        >
            <ScrollView
                style={{
                    padding: 10,
                    backgroundColor: uiColor.Very_Pole_Grey
                }}
            >
                <View
                    style={{ backgroundColor: 'white' }}
                >
                    <Garage />
                </View>
                <ContragentMistake state={agent} />
                <View
                    style={{ marginBottom: 10 }}
                >
                    <TouchesState
                        color="Yellow"
                        disabled={R.includes(true)(disabled(agent))}
                        flex={1}
                        onPress={async () => {
                            upgradeGarage({ variables: variables(agent) });
                            if (R.path(['updateGarage'])(data)) {
                            }
                        }}
                        title="SAVE"
                    />
                </View>
            </ScrollView>
        </Header>
    )
};
