import * as R from 'ramda';
import { isOdd } from 'utils/helper';

export const initialState = {
    right: false,
    left: false,
    text: '',
    swipe: false,
    current: "ID",
    page: 1,
    variables: { where: {}, order: { id: "desc" }, paginate: { page: 1, limit: 20 } }
};
export const reducer = (state = initialState, action) => {
    const initialSearch = null;

    switch (action.type) {
        case "Right":
            return R.mergeAll([state, { right: !R.path(['right'])(state) }]);
        case "Left":
            return R.mergeAll([state, R.assocPath(['search'], initialSearch, { left: !R.path(['left'])(state) })]);
        case "Text":
            return R.mergeAll([state, { text: R.path(['payload'])(action) }]);
        case "Swipe":
            return R.mergeAll([state, { swipe: R.path(['payload'])(action) }]);
        case "Checker":
            if (R.equals(R.path(['payload'])(action))(R.path(['checker'])(state))) return R.mergeAll([state, { checker: "AllWork" }]);

            return R.mergeAll([state, { checker: R.path(['payload'])(action) }]);
        case "Search":
            return R.mergeAll([state, { search: R.path(['payload'])(action) }]);
        case "Updata":
            return R.assocPath(['data'], R.path(['payload'])(action), state);
        case "Variables":
            return R.assocPath(['variables'], R.path(['payload'])(action), state);
        default:
            return state
    }
};

export const UpdateState = (searchText, path) => {
    if (isOdd(searchText)) {
        return { delete: null }
    }

    return R.assocPath(path, searchText, {})
};

export const StateBuild = (state, storage) => {
    const connecter = R.cond([
        [R.equals("Checker"), R.always({ checker: R.equals(state.type, 'Checker') ? state.payload : storage.checker })],
        [R.equals("Search"), R.always({ search: R.equals(state.type, 'Search') ? state.payload : storage.search })],
        [R.equals("Text"), R.always({ text: R.equals(state.type, 'Text') ? state.payload : storage.text })],
        [R.equals("Page"), R.always({ page: R.equals(state.type, 'Page') ? state.payload : storage.page })],
    ]);

    return R.pipe(
        R.map(connecter),
        R.mergeAll,
        R.reject(isOdd)
    )(["Checker", 'Search', 'Text', 'Page'])
};

export const setRequestDataLoader = (state) => {
    const searchText = R.path(['text'])(state);
    const blocked = R.cond([
        [R.equals("COLOR"), R.always(R.assocPath(['vehicles', 'color', 'eq'], searchText, {}))],
        [R.equals("YEAR"), R.always({ blocked: { eq: false } })],
        [isOdd, R.always({ delete: null })],
    ])(R.path(['checker'])(state));

    const text = R.cond([
        [R.equals('LAST'), R.always(UpdateState(searchText, ['name', 'last', 'contain']))],
        [R.equals('NAME'), R.always(UpdateState(searchText, ['name', 'first', 'contain']))],
        [R.equals('PHONE'), R.always(UpdateState(searchText, ['phone', 'contain']))],
        [R.equals("VIN"), R.always(UpdateState(searchText, ['vehicles', 'vin', 'contain']))],
        [R.equals("FRAMENUMBER"), R.always(UpdateState(searchText, ['vehicles', 'frameNumber', 'contain']))],
        [R.isNil, R.always(UpdateState(searchText, ['q']))],
    ])(R.path(['search'])(state));

    return {
        ...R.reject(isOdd, {
            where: R.omit(['delete'], R.mergeAll([blocked, text])),
            paginate: { limit: 20, page: R.path(['page'])(state) },
            order: { id: 'desc' }
        }),
    }
};
