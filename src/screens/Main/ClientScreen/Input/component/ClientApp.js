/* eslint-disable max-statements */
/* eslint-disable no-shadow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-redeclare */
/* eslint-disable react/no-multi-comp */
/* eslint-disable scanjs-rules/call_connect */
/* eslint-disable react/jsx-indent-props */
import { useApolloClient } from '@apollo/react-hooks';
import * as R from 'ramda';
import React from 'react';
import { showMessage } from 'react-native-flash-message';
import { CREATE_CLIENT, UPDATE_CLIENT } from 'screens/Main/ClientScreen/GQL';
import { ScrolerView } from 'screens/Master/CreateEmployeeScreen/style';
import { requestMutation } from 'utils/api/funcComponent/Apollo';
import { MainScreen } from 'utils/routing/index.json';
import { ButtonBlock } from './buttomBlock';
import { InputComponentState } from './input';

export const ClientApp = ({ storage, dispatch, navigation }) => {
    const clientApollo = useApolloClient();
    const ROUTER = MainScreen.Client;

    const setUpdateData = async (event) => {
        const variables = R.cond([
            [R.equals("Action"), R.always({ input: R.omit(['__typename', 'vehicles'])(storage) })],
        ]);
        const document = R.cond([
            [R.equals("CREATE"), R.always({ query: CREATE_CLIENT, variables: variables(R.path(['type'])(event)) })],
            [R.equals("UPDATE"), R.always({ query: UPDATE_CLIENT, variables: variables(R.path(['type'])(event)) })],
        ])(R.path(['payload'])(event));

        try {
            const { data } = await requestMutation(R.mergeAll([document, { client: clientApollo }]));

            if (R.equals(R.path(['payload'])(event), 'DESTROY')) return navigation.navigate(ROUTER);
            showMessage({
                message: "Данные обновлены",
                type: "success",
            });

            return dispatch({ type: "Update", payload: R.path(R.keys(data))(data) })
        } catch (error) {
            return showMessage({
                message: R.toString(error),
                description: "Error",
                type: "info",
            })
        }
    };

    return (
        <ScrolerView
            contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between' }}
        >
            <InputComponentState
                dispatch={(e) => {
                    dispatch(e)
                }}
                storage={storage}
                testID="InputComponentState"
            />
            <ButtonBlock
                dispatch={(e) => {
                    if (R.equals(R.path(['type'])(e))('Action')) return setUpdateData(e);
                    if (R.equals(R.path(['type'])(e))('Cancel')) return navigation.goBack()
                }}
                navigation={navigation}
                storage={storage}
                testID="ButtonBlock"
            />
        </ScrolerView>
    )
};