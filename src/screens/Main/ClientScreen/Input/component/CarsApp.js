/* eslint-disable max-statements */
/* eslint-disable no-shadow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-redeclare */
/* eslint-disable react/no-multi-comp */
/* eslint-disable scanjs-rules/call_connect */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { showMessage } from 'react-native-flash-message';
import { CREATE_VEHICLE, DESTROY_VEHICLE, UPDATE_VEHICLE } from 'screens/Main/CarsScreen/GQL';
import { ScrolerView } from 'screens/Master/CreateEmployeeScreen/style';
import { requestMutation } from 'utils/api/funcComponent/Apollo';
import { MainScreen } from 'utils/routing/index.json';
import { useApolloClient } from '@apollo/react-hooks';
import { ButtonBlock } from 'screens/Main/CarsScreen/Input/component/buttomBlock';
import { InputComponentState } from 'screens/Main/CarsScreen/Input/component/input';
import { isOdd } from 'utils/helper'

export const CarsApp = ({ storage, dispatch, navigation }) => {
    const clientApollo = useApolloClient();
    const ROUTER = MainScreen.Client;

    const setUpdateData = async (event) => {
        const variables = R.cond([
            [R.equals("Action"), R.always({
                id: R.path(['vehicle', 'id'])(storage),
                frameNumber: R.path(['vehicle', 'frameNumber'])(storage),
                vin: R.path(['vehicle', 'vin'])(storage),
                year: parseInt(R.path(['vehicle', 'year'])(storage)),
                color: R.path(['vehicle', 'color'])(storage),
                modificationId: R.path(['vehicle', 'modification', 'id'])(storage),
                plate: R.path(['vehicle', 'plate'])(storage),
                mileage: parseInt(R.path(['vehicle', 'mileage'])(storage)),
                owner: {
                    id: R.path(['id'])(storage),
                    name: {
                        first: R.path(['name', 'first'])(storage),
                        last: R.path(['name', 'last'])(storage),
                        middle: R.path(['name', 'middle'])(storage),
                    },
                    phone: R.path(['phone'])(storage),
                }
            })],
            [R.equals("Delete"), R.always({ input: R.path(['id'])(storage) })],
        ]);

        const document = R.cond([
            [R.equals("CREATE"), R.always({
                query: CREATE_VEHICLE,
                variables: {
                    input: R.reject(isOdd)(R.omit(['id'])(variables(R.path(['type'])(event))))
                }
            })],
            [R.equals("UPDATE"), R.always({
                query: UPDATE_VEHICLE,
                variables: {
                    input: R.reject(isOdd)(R.omit(['owner'])(variables(R.path(['type'])(event))))
                }
            })],
            [R.equals("DESTROY"), R.always({
                query: DESTROY_VEHICLE,
                variables: variables(R.path(['type'])(event))
            })],
        ])(
            R.cond([
                [isOdd, R.always("CREATE")],
                [R.T, R.always("UPDATE")]
            ])(R.path(['vehicle', 'id'])(storage))
        );

        try {
            const data = await requestMutation(R.mergeAll([document, { client: clientApollo }]));

            if (R.equals(R.path(['payload'])(event), 'DESTROY')) return navigation.navigate(ROUTER);
            showMessage({
                message: "Данные обновлены",
                type: "success",
            });

            return dispatch({ type: "Update", payload: R.path(R.keys(data))(data) })
        } catch (error) {
            return showMessage({
                message: R.toString(error),
                description: "Error",
                type: "info",
            })
        }
    };

    return (
        <ScrolerView
            contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between' }}
        >
            <InputComponentState
                dispatch={(e) => {
                    dispatch(e)
                }}
                storage={storage}
                testID="InputComponentState"
            />
            <ButtonBlock
                dispatch={(e) => {
                    if (R.equals(R.path(['type'])(e))('Action')) {
                        if (isOdd(R.path(['id'])(storage))) {
                            return showMessage({
                                message: "Заполните клиента",
                                type: "danger",
                            });
                        }

                        return setUpdateData(e)
                    }
                    if (R.equals(R.path(['type'])(e))('Cancel')) return dispatch(e)
                }}
                navigation={navigation}
                storage={storage}
                style={{ bottom: 0, flex: 1 }}
                testID="ButtonBlock"
            />
        </ScrolerView>
    )
};
