/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable padded-blocks */
/* eslint-disable arrow-body-style */
import * as R from 'ramda';
import React from 'react';
import { InputComponents } from 'utils/component'
import { initialInputState } from '../config';

export const InputComponentState = ({
    storage, dispatch
}) => {
    const changeText = (change, inputPath) => {

        return dispatch({
            type: inputPath.label,
            payload: R.assocPath(inputPath.value, R.equals(inputPath.label, 'phone') ? `${'+7'}${change}` : change, {})
        })
    };

    return (
        <>
            {R.addIndex(R.map)((element, keys) => {
                return (
                    <InputComponents
                        key={keys}
                        {...element}
                        onChangeText={(change) => changeText(change, element)}
                        storage={storage}
                    />
                )
            })(initialInputState)}
        </>
    )
};
