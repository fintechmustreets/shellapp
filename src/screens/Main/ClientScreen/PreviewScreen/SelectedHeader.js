/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { RowComponents, CardComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { styles } from 'screens/Drawler/components/helper/styleSheets';
import { HEADER } from 'screens/Main/CarsScreen/Preview/config';

export const SelectedHeader = ({ state, setState }) => (
    <View>
        <RowComponents
            style={{
                alignItems: 'center',
                margin: 10,
                marginBottom: 0
            }}
        >
            {R.map(x => (
                <TouchableOpacity
                    key={x}
                    onPress={() => setState(x)}
                    style={{
                        paddingLeft: 10,
                        paddingRight: 10
                    }}
                >
                    <LabelText
                        color={ColorCard(R.equals(x, state) ? 'error' : 'classic').font}
                        fonts="ShellMedium"
                        items="SRDescription"
                        style={[styLes.items, R.equals(x, state) ? {
                            borderBottomWidth: 3
                        } : {}]}
                        testID="BlockLaker"
                    >
                        {i18n.t(x)}
                    </LabelText>
                </TouchableOpacity>
            ))(['TaskWorks', 'ListCars'])}
            <View style={{ flex: 1 }} />
        </RowComponents>
    </View>
);


const styLes = StyleSheet.create({
    items: {
        textAlign: 'center',
        textTransform: 'none',
        paddingBottom: 5,
        borderColor: 'red',
    }
});

export const TitleHeader = ({ storage, state, setState }) => (
    <View>
        <CardComponents style={styles.shadow}>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellBold"
                items="SSDescription"
                style={{
                    textAlign: 'left',
                    margin: 5,
                    textTransform: 'uppercase'
                }}
                testID="BlockLaker"
            >
                {HEADER(storage)}
            </LabelText>
        </CardComponents>
        <RowComponents
            style={{
                alignItems: 'center',
                margin: 10,
                marginBottom: 0
            }}
        >
            {R.map(x => (
                <TouchableOpacity
                    key={x}
                    onPress={() => setState(x)}
                    style={{
                        paddingLeft: 10,
                        paddingRight: 10
                    }}
                >
                    <LabelText
                        color={ColorCard(R.equals(x, state) ? 'error' : 'classic').font}
                        fonts="ShellMedium"
                        items="SRDescription"
                        style={[styLes.items, R.equals(x, state) ? {
                            borderBottomWidth: 3
                        } : {}]}
                        testID="BlockLaker"
                    >
                        {i18n.t(x)}
                    </LabelText>
                </TouchableOpacity>
            ))(['ListCars', 'Task'])}
            <View style={{ flex: 1 }} />
        </RowComponents>
    </View>
);