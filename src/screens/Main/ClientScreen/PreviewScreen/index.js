/* eslint-disable no-multiple-empty-lines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-indent-props */
import { useCurrentClient } from 'hooks/useCurrentClient';
import { useNavigation } from 'hooks/useNavigation';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import Modal from 'react-native-modal';
import { useDispatch } from 'react-redux';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { SelectedHeader } from 'screens/Main/ClientScreen/PreviewScreen/SelectedHeader';
import { FilterCar } from 'screens/Main/FilterCar';
import { Header } from 'screens/TaskEditor/components/header';
import { VisualTable } from 'screens/TaskEditor/components/table/component';
import { isOdd } from 'utils/helper';
import { Preview } from 'utils/routing/index.json';
import { Title } from './Title';

export const PrewiewClientScreen = ({ route, navigation }) => {
    const routeId = R.path(['params', 'itemId'])(route)
    const [currenteUser] = useCurrentClient({ id: routeId })
    const ref = React.useRef()
    ref.current = currenteUser

    const [current, setMoved] = useNavigation({ navigation, move: 'goBack' });
    const [state, setState] = React.useState('TaskWorks');
    const ROUTER = Preview.PreviewTaskGarage;
    const dispatchRedux = useDispatch()
    const [modal, setModal] = React.useState({ isVisible: false });
    const [id, setId] = React.useState({});


    const success = (e) => R.cond([
        [R.equals('GOBACK'), () => setMoved(current)],
        [R.equals('Reset'), () => {
            setId({ vehicleId: R.path(['params', 'itemId', 'owner'])(route) });

            return setModal({ isVisible: false })
        }],
        [R.equals('CLOSE'), () => setModal({ isVisible: false })],
        [R.equals('Filter'), () => setModal({ isVisible: true })],
        [R.equals('Apply'), () => {
            setId(R.mergeAll([id, R.path(['payload'])(e)]));

            return setModal({ isVisible: false })
        }],
        [R.equals('Create'), () => {
            dispatchRedux({
                type: 'TASK_UPDATE',
                payload: {}
            })

            navigation.navigate(Preview.PreviewTaskGarage)
        }],
        [R.equals('Edit'), () => navigation.navigate(
            ROUTER,
            R.reject(isOdd, R.path(['payload'])(e))
        )],
    ])(R.path(['type'])(e))

    const Visia = () => {
        const id = R.assocPath([R.equals(state)("TaskWorks") ? 'clientId' : 'ownerId'], R.path(['params', 'itemId'])(route))({})
        const mode = R.equals(state, "TaskWorks") ? "repairs" : "vehicle"

        return (
            <VisualTable
                button
                dispatch={success}
                id={id}
                mode={mode}
                navigation={navigation}
                route={null}
                unfilter
            />
        )
    }


    return (
        <Header
            dispatch={setMoved}
            menu={{ title: "ListClients" }}
        >
            <View>
                <Title id={R.path(['params', 'itemId'])(route)} />
                <SelectedHeader
                    setState={(e) => setState(e)}
                    state={state}
                />
            </View>
            {Visia()}
            <View>
                <Modal isVisible={R.path(['isVisible'])(modal)}>
                    <ModalDescription style={{ flex: 1, margin: 10 }}>
                        <FilterCar
                            dispatch={(e) => success(e)}
                            storage={id}
                        />
                    </ModalDescription>
                </Modal>
            </View>
        </Header>
    )
};
