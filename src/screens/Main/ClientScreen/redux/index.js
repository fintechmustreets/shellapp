import * as R from 'ramda'
import createSensitiveStorage from 'redux-persist-sensitive-storage'

export const clientStorage = createSensitiveStorage({
    keychainService: 'ClientChainKeychain',
    sharedPreferencesName: 'ClientChainSharedPrefs',
});

export const clientPersistConfig = {
    key: 'client',
    storage: clientStorage,
};

export const initialState = {};

export const clientReducer = (state = initialState, action) => R.cond([
    [R.equals('CLIENT_ADD'), R.always(action.payload)],
    [R.equals('CLIENT_MERGE'), R.mergeRight(state, action.payload)],
    [R.equals('CLIENT_REMOVE'), R.always(initialState)],
    [R.T, R.always(state)],
])(action.type);
