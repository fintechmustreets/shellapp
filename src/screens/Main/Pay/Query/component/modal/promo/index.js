/* eslint-disable no-confusing-arrow */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-inline-comments */
/* eslint-disable line-comment-position */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React, { useState } from 'react';
import { View } from 'react-native';
import { styles } from 'screens/Main/Pay/helper';
import { ButtonReview } from 'screens/Main/Pay/Query/component/modal/promo/ButtonReview';
import { promoInput } from 'screens/Main/Pay/Query/component/modal/promo/config';
import { InputComponents } from 'utils/component';

export const PromoModal = ({ dispatch }) => {
    const [state, setState] = useState({ error: true });

    return (
        <>
            <View style={[styles.block, { flex: 1 }]}>
                <InputComponents
                    {...promoInput}
                    onChangeText={(change, error) => setState(R.mergeAll([state, {
                        error,
                        promo: change,
                    }]))}
                    storage={state}
                />
            </View>
            <ButtonReview
                dispatch={(e) => dispatch(e)}
                setState={(e) => setState(e)}
                state={state}
            />
        </>
    )
};
