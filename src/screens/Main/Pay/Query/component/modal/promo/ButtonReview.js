/* eslint-disable no-confusing-arrow */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-inline-comments */
/* eslint-disable line-comment-position */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import { useMutation } from '@apollo/react-hooks';
import { ACTIVATE_PROMO } from 'gql/billAccount';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { showMessage } from "react-native-flash-message";
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { promoInput } from 'screens/Main/Pay/Query/component/modal/promo/config'

export const ButtonReview = ({ state, setState, dispatch }) => {
    const ButtonColor = (x) => R.equals(x, 'CLOSED') ? "VeryRed" : "Yellow";
    const ButtonDisabled = (x) => R.equals(x, 'CLOSED') ? false : !R.isNil(R.path(['error'])(state));
    const [activatePromo] = useMutation(ACTIVATE_PROMO);

    const success = async () => {
        try {
            const { data } = await activatePromo({
                variables: { input: { code: R.path(['promo'])(state) } }
            });

            if (data) {
                showMessage({
                    message: R.toString("ПРОВЕРЬТЕ ПРАВИЛЬНОСТЬ ДАННЫХ"),
                    description: "Error",
                    type: "success",
                });
            }
        } catch (e) {
            showMessage({
                message: R.toString("ПРОВЕРЬТЕ ПРАВИЛЬНОСТЬ ДАННЫХ"),
                description: i18n.t(R.path(['errorMessage'])(promoInput)),
                type: "danger",
            });
        }

        return dispatch(false)
    };

    const update = (x) => {
        if (R.equals(x, 'CLOSED')) {
            dispatch(false);

            return setState({})
        }
        if (R.equals(x, 'Apply')) {
            setState(R.assocPath(['modal'], 'Update', state));

            return success()
        }
    };

    return (
        <RowComponents>
            {R.map(x => (
                <TouchesState
                    key={x}
                    color={ButtonColor(x)}
                    disabled={ButtonDisabled(x)}
                    flex={1}
                    onPress={() => update(x)}
                    title={x}
                />))(["Apply", 'CLOSED'])}
        </RowComponents>
    )
};