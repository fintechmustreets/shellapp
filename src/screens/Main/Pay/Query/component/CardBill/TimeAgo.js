/* eslint-disable react/no-multi-comp */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import moment from 'moment';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { styles } from 'screens/Main/Pay/helper';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { useSelector, shallowEqual } from 'react-redux'

export const TimeAgo = () => {
    const state = useSelector(state => state.depots.bill, shallowEqual);
    const time = moment(moment().unix()) * 1000;
    const endTime = time + (R.path(['billAccount', 'balance'])(state) * 1000);
    const x = moment(endTime).diff(moment(), 'seconds') * 1000;
    const tempTime = moment.duration(x);

    return (
        <View
            style={{ padding: 10, flexDirection: 'column' }}
        >
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellBold"
                items="SRDescription"
                style={styles.items}
                testID="BlockLaker"
            >
                {i18n.t('DiffPay')}
            </LabelText>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SRDescription"
                style={[styles.items, { textTransform: 'lowercase' }]}
                testID="BlockLaker"
            >
                {R.join(' ', [
                    R.head(
                        R.split('.', R.toString(tempTime.asDays()))), 'д. ', tempTime.hours(), 'ч. ', tempTime.minutes(), 'мин. '
                ])}
            </LabelText>
        </View>
    )
};