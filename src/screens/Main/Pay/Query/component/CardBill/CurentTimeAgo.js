/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import moment from 'moment';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { styles } from 'screens/Main/Pay/helper';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { useSelector, shallowEqual } from 'react-redux'

export const CurentTimeAgo = () => {
    const state = useSelector(state => state.depots.bill, shallowEqual);
    const time = moment(moment().unix()) * 1000;
    const endTime = time + (R.path(['billAccount', 'balance'])(state) * 1000);

    return (
        <View
            style={styles.container}
        >
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellBold"
                items="SRDescription"
                style={styles.items}
            >
                {i18n.t('EndedPay')}
            </LabelText>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SRDescription"
                style={styles.items}
            >
                {moment(endTime).format("DD.MM.YYYY HH:mm")}
            </LabelText>
        </View>
    )
};