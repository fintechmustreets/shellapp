/* eslint-disable react/no-multi-comp */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import moment from 'moment';
import * as R from 'ramda';
import React, { useState } from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import { styles } from 'screens/Main/Pay/helper';
import { ButtonReview } from 'screens/Main/Pay/Query/component/CardBill/ButtonReview';
import { CurentTimeAgo } from 'screens/Main/Pay/Query/component/CardBill/CurentTimeAgo';
import { ProgressBarAgo } from 'screens/Main/Pay/Query/component/CardBill/ProgressBarAgo';
import { TimeAgo } from 'screens/Main/Pay/Query/component/CardBill/TimeAgo';
import { ModalSetting } from 'screens/Main/Pay/Query/component/modalPromo';
import { showMessage } from 'react-native-flash-message';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { useEmployeeProfile } from 'hooks/useProfile';

export const CardBillComponents = ({ navigation }) => {
    const depots = useSelector(state => state.depots.bill, shallowEqual);
    const [promo, setPromo] = useState({ isVisible: false });
    const [modal, setModal] = useState({ isVisible: false });
    const [state, update] = useEmployeeProfile()

    React.useEffect(() => {
        if (!state) {
            update()
        }
    }, [state])

    const success = (e) => {
        setPromo(R.assocPath(['isVisible'], e, promo));
        if (modal) setModal(null);
        if (R.equals('Create')(R.path(['type'])(e))) {
            setPromo(R.assocPath(['isVisible'], false, promo));
            showMessage({
                message: 'Счет успешно выставлен. Теперь можете его распечатать или сохранит на устройство',
                description: "Success",
                type: "success",
            })
        }
    };

    const memo = React.useMemo(() => ({
        now: moment().format("DD.MM.YYY Y HH:mm"),
        ...R.path(['billAccount'])(depots)
    }), [depots])

    return (
        <CardComponents
            style={styles.shadow}
        >
            <ProgressBarAgo state={memo} />
            <CurentTimeAgo state={memo} />
            <TimeAgo state={memo} />
            <ButtonReview
                promo={promo}
                setPromo={(e) => setPromo(e)}
            />
            <ModalSetting
                dispatch={(e) => success(e)}
                isVisible={R.path(['isVisible'])(promo)}
                modal={promo}
                navigation={navigation}
            />
        </CardComponents>
    )
};
