/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import card from 'assets/svg/card';
import tag from 'assets/svg/tag';
import { TouchesState } from 'styled/UIComponents/UiButtom';

export const ButtonReview = ({ setPromo, promo }) => R.map(x => (
    <TouchesState
        key={x}
        color={R.equals(x, 'Activate') ? 'Grey' : "Yellow"}
        flex={0}
        onPress={() => setPromo(
            R.assocPath(['isVisible'], true, {
                mode: R.equals('Activate')(x),
                promo
            }))}
        svg={R.equals("Activate", x) ? tag : card}
        title={x}
        unTransform
    />
))(['Activate', 'Pay']);