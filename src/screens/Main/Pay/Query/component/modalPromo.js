/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-wrap-multilines */
import * as R from 'ramda';
import React from 'react';
import Modal from 'react-native-modal';
import { PayModal } from 'screens/Main/Pay/Query/component/modal/payAgent';
import { PromoModal } from 'screens/Main/Pay/Query/component/modal/promo';
import { MainScreen } from 'utils/routing/index.json';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import i18n from 'localization';

export const ModalSetting = ({
    isVisible, dispatch, modal, navigation
}) => {
    const success = (e) => {
        if (R.equals(R.path(['type'])(e), 'Agent')) {
            dispatch(false);
            return navigation.navigate(MainScreen.AutoService)
        }
        return dispatch(e)
    }

    return (
        <Modal isVisible={isVisible}>
            <ModalDescription
                style={{ flex: 1, margin: 10 }}
            >
                <HeaderApp
                    dispatch={() => dispatch(R.assocPath(['isVisible'], false, {}))}
                    empty
                    header
                    title={!R.path(['mode'])(modal) ? i18n.t("BuySystem") : i18n.t("AddedPayement")}
                />
                {R.path(['mode'])(modal) ?
                    <PromoModal
                        dispatch={success}
                        modal={modal}
                    /> :
                    <PayModal
                        dispatch={success}
                        modal={modal}
                    />}
            </ModalDescription>
        </Modal>
    )
};
