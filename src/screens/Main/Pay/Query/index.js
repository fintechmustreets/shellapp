/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import { useNavigation } from 'hooks/useNavigation';
import { useProfile } from 'hooks/useProfile';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { shallowEqual, useSelector } from 'react-redux';
import { CardBillComponents } from 'screens/Main/Pay/Query/component/CardBill';
import { Header } from 'screens/TaskEditor/components/header';
import { VisualTable } from 'screens/TaskEditor/components/table/component';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';

export const PayConnect = ({ navigation }) => {
    const depots = useSelector(state => state.depots.bill, shallowEqual);
    const [current, setMoved] = useNavigation({ navigation, move: 'toggle' });

    return (
        <Header
            dispatch={setMoved}
            menu={{ title: "Tariff" }}
            svg={current}
        >

            <VisualTable
                dispatch={() => null}
                id={R.assocPath(['billAccountId'], { eq: R.path(['billAccount', 'id'])(depots) })({})}
                mode="pay"
                navigation={navigation}
            />
        </Header>
    )
};

export const HeaderInputPay = ({ navigation }) => {
    const [state] = useProfile()

    return (
        <View style={{ marginLeft: 10, marginRight: 10 }}>
            <HeaderApp
                empty
                header
                title={i18n.t('ConnectedSystem')}
            />
            <CardBillComponents
                navigation={navigation}
                state={state}
            />
            <HeaderApp
                empty
                header
                title={i18n.t('MYPAY')}
            />
        </View>
    )
}
