/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import Rub from 'assets/svg/rub';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Checkbox } from 'react-native-paper';
import { SvgXml } from 'react-native-svg';
import { styles } from 'screens/Main/Pay/helper';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const CheckerState = ({ setState, state, item }) => {
    const checkbox = R.equals(R.path(['name'])(item), R.path(['mode', 'name'])(state)) ? 'checked' : 'unchecked';

    return (
        <TouchableOpacity
            onPress={() => {
                setState(R.assocPath(['mode'], item, state))
            }}
        >
            <RowComponents
                style={{
                    paddingRight: 0,
                    justifyContent: 'flex-start',
                    marginTop: 10,
                    marginBottom: 10
                }}
            >
                <Checkbox
                    status={checkbox}
                />
                <CardComponents style={{ justifyContent: 'flex-end' }}>
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SSDescription"
                        testID="TITLE"
                    >
                        {i18n.t(R.path(['name'])(item))}
                    </LabelText>
                    <RowComponents style={{ width: 80, marginTop: 10 }}>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SSDescription"
                            style={styles.items}
                            testID="TITLE"
                        >
                            {R.path(['price', 'amount'])(item) * R.path(['priceCount'])(item)}
                        </LabelText>
                        <SvgXml height="10" weight="10" xml={Rub} />
                    </RowComponents>
                </CardComponents>
            </RowComponents>
        </TouchableOpacity >
    )
};