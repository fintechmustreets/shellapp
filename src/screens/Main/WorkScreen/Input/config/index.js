/* eslint-disable complexity */
/* eslint-disable no-case-declarations */
/* eslint-disable newline-before-return */
/* eslint-disable no-unneeded-ternary */
import * as R from 'ramda'

export const reducer = (state = {}, action) => {
    switch (action.type) {
        case 'vehicleModifications':
            return R.assocPath([action.type], action.payload, R.dissocPath(['vehicleModels', 'vehicleManufacturers'], state));
        case 'vehicleModels':
            return R.assocPath([action.type], action.payload, state);
        case 'vehicleManufacturers':
            return R.assocPath([action.type], action.payload, state);
        case 'vehicle':
            return R.mergeAll([action.payload, state]);
        case 'plate':
            return R.mergeAll([action.payload, state]);
        case 'vin':
            return R.mergeAll([action.payload, state]);
        case 'year':
            return R.mergeAll([action.payload, state]);
        case 'model':
            return R.assocPath([action.type], action.payload, state);
        case 'modification':
            return R.assocPath([action.type], action.payload, state);
        case 'frameNumber':
            return R.mergeAll([action.payload, state]);
        case 'color':
            return R.mergeAll([action.payload, state]);
        case 'Data':
            const keys = R.keys(action.payload);

            if (R.equals(keys, ['vehicleManufacturers'])) {
                return R.assocPath(['lib'], action.payload, state);
            }
            if (R.equals(keys, ['vehicleModels'])) {
                return R.assocPath(['libModels'], action.payload, state);
            }
            if (R.equals(keys, ['vehicleModifications'])) {
                return R.assocPath(['libModifications'], action.payload, state);
            }
            return state;
        case 'Update':
            return R.mergeAll([state, action.payload]);

        default:
            return state
    }
};

export const initialInputState = [
    {
        id: "Name",
        path: 'name',
        keyboardType: "default",
        label: 'name',
        title: "Error message",
        value: ["name"],

        maxLength: 17,
        testID: "NameInput",
    },
    {
        id: "qty",
        path: 'name',
        keyboardType: "numeric",
        label: 'qty',
        title: "Error message",
        value: ["qty"],

        maxLength: 5,
        testID: "QTY",
    },
    {
        id: "Timehrs",
        path: 'plate',
        keyboardType: "numeric",
        label: 'timeHrs',
        title: "Error message",
        value: ["timeHrs"],
        maxLength: 6,

        testID: "TIMEHRS",
    },
    {
        id: "Price",
        path: 'pricePerHour',
        keyboardType: "numeric",
        label: 'pricePerHour',
        title: "Error message",
        value: ["pricePerHour"],
        maxLength: 32,

        testID: "Price",
    },
    {
        id: "Discount",
        path: 'discountPersent',
        keyboardType: "numeric",
        label: 'discountPersent',
        title: "Error message",
        value: ["discount"],
        maxLength: 32,

        testID: "discountPersent",
    },
    {
        id: "Discount",
        path: 'discountCoast',
        keyboardType: "numeric",
        label: 'discountCoast',
        title: "Error message",
        value: ["discount"],
        maxLength: 32,

        testID: "discountCoast",
    },
];

export const qweqwe = [
    {
        id: "ГОД",
        path: 'middle',
        keyboardType: "numeric",
        label: 'year',
        title: "Error message",
        value: ["year"],
        maxLength: 4,

        testID: "Year",
    },
    {
        id: "ПРОБЕГ",
        path: 'mileage',
        keyboardType: "numeric",
        label: 'mileage',
        title: "Error message",
        value: ["mileage"],
        maxLength: 6,

        testID: "Mileage",
    },
];

export const dropdown = [
    {
        id: "МАРКА",
        path: 'vehicleManufacturers',
        keyboardType: "default",
        label: 'vehicleManufacturers',
        title: "Error message",
        value: ["name"],
        valuePath: ["modification", 'model', 'manufacturer', 'name'],
        mode: "dropdown",
        maxLength: 120,
        testID: "Vehicles",
        lib: "lib",
    },
    {
        id: "МОДЕЛЬ",
        path: 'vehicleModels',
        keyboardType: "default",
        label: 'vehicleModels',
        title: "Error message",
        value: ['name'],
        valuePath: ["modification", 'model', 'name'],
        mode: "dropdown",
        maxLength: 120,
        testID: "Model",
        lib: "libModels",
    },
    {
        id: "МОДИФИКАЦИЯ",
        path: 'vehicleModifications',
        keyboardType: "default",
        label: 'vehicleModifications',
        title: "Error message",
        value: ["enginecode"],
        valuePath: ["modification"],
        mode: "dropdown",
        maxLength: 120,
        testID: "Modification",
        lib: "libModifications",
    },
];