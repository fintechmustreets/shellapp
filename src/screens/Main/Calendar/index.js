/* eslint-disable max-len */
/* eslint-disable arrow-body-style */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import React from 'react';
import { Header } from 'screens/TaskEditor/components/header';
import { AgentaCalendar } from 'screens/Main/Calendar/AgentaCalendar';
import { ButtonCoast } from 'screens/Main/Calendar/ButtonCoast';
import { PickerType } from 'screens/Main/Calendar/PickerType';
import { useNavigation } from 'hooks/useNavigation'

export const Calendar = ({ navigation }) => {
    const [current, setMoved] = useNavigation({ navigation, move: 'toggle' });

    const [state, setState] = React.useState(null);

    return (
        <Header
            dispatch={setMoved}
            menu={{ title: "Postupload" }}
            svg={current}
        >
            <PickerType
                setState={(e) => setState(e)}
                state={state}
            />
            <AgentaCalendar
                navigation={navigation}
                post={state}
            />
            <ButtonCoast />
        </Header>
    )
};