/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import * as R from 'ramda';
import React from 'react';
import { ScrollView } from 'react-native';
import { VisualTable } from 'screens/TaskEditor/components/table/component';
import { isOdd } from 'utils/helper';
import { SelectedWork } from './SelectedWork'
import { VehicleId } from './VehicleId'

export const WorkerScreen = ({ day, post, time }) => {
  const [state, setState] = React.useState("preorder");
  const [vehicle, setVehicleId] = React.useState(null);

  if (isOdd(vehicle)) {
    return (
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <SelectedWork
          setState={(e) => setState(e)}
          state={state}
        />
        <VisualTable
          button
          dispatch={(e) => {
            setVehicleId(R.path(['payload', 'id'])(e))
          }}
          mode="repairs"
          navigation={{ addListener: (e) => console.log(e) }}
          route={state}
        />
      </ScrollView >
    )
  }

  return (
    <VehicleId
      day={day}
      post={post}
      setVehicleId={(e) => setVehicleId(e)}
      time={time}
      vehicle={vehicle}
    />
  )
};
