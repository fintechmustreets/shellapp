/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import React from 'react';
import {
    StyleSheet, TouchableOpacity, View
} from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { PlateNumber } from 'screens/TaskEditor/components/table/component/repairs';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { uiColor } from 'styled/colors/uiColor.json';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import * as R from 'ramda'
import moment from 'moment'

export const renderItem = ({ item, time, navigation, setState }) => {
    if (R.path(['id'])(item)) {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: uiColor.Yellow,
                }}
            >
                <CardComponents
                    style={[styles.shadow, TableStyle.card, { margin: 3 }]}
                >
                    <TouchableOpacity
                        onPress={() => navigation({ itemId: R.path(['repair', 'id'])(item) })}
                    >
                        <RowComponents>
                            <LabelText
                                color={ColorCard('error').font}
                                fonts="ShellMedium"
                                items="SDescription"
                                style={{ textAlign: 'left', }}
                                testID="BlockLaker"
                            >
                                {R.join(' - ', R.map(x => moment(x).format("HH-mm"))(R.paths([['startAt'], ['endAt']])(item)))}
                            </LabelText>
                            <PlateNumber
                                color={ColorCard('classic').font}
                                fonts="ShellMedium"
                                items="SDescription"
                                style={{ textAlign: 'right', borderWidth: 1 }}
                                testID="BlockLaker"
                            >
                                {R.path(['repair', 'vehicle', 'plate'])(item)}
                            </PlateNumber>
                        </RowComponents>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SDescription"
                            style={{ textAlign: 'left' }}
                            testID="BlockLaker"
                        >
                            {R.join(' ', R.paths([
                                ['repair', 'vehicle', 'modification', 'manufacturer', 'name'],
                                ['repair', 'vehicle', 'modification', 'model', 'name'],
                                ['repair', 'vehicle', 'modification', 'model', 'subbody']
                            ])(item))}
                        </LabelText>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SDescription"
                            style={{ textAlign: 'left', }}
                            testID="BlockLaker"
                        >
                            {R.path(['repair', 'totalPrice', 'amount'])(item)}
                        </LabelText>
                    </TouchableOpacity>
                </CardComponents>
            </View>
        )
    }

    return (
        <View
            style={{
                flex: 1,
                backgroundColor: uiColor.Yellow,
            }}
        >
            <TouchableOpacity
                onPress={() => setState(time)}
            >
                <CardComponents
                    style={[styles.shadow, TableStyle.empty]}
                >
                    <LabelText
                        color={ColorCard('error').font}
                        fonts="ShellMedium"
                        items="SDescription"
                        style={{ textAlign: 'left' }}
                        testID="BlockLaker"
                    >
                        {R.join(' - ', [timeString(R.head(time)), timeString(R.last(time))])}
                    </LabelText>
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SDescription"
                        style={{ textAlign: 'left' }}
                        testID="BlockLaker"
                    >
                        Place is Free
                    </LabelText>
                </CardComponents>
            </TouchableOpacity>
        </View>
    )
};

export const timeString = (x) => {
    const correctTime = moment(x).format("HH-mm");

    if (R.equals(correctTime, 'Invalid date')) return x;

    return correctTime
};

const TableStyle = StyleSheet.create({
    line: {
        marginTop: 15,
        marginBottom: 15,
        borderColor: uiColor.Mid_Grey,
        borderWidth: 0.2,
        marginLeft: 0,
        marginRight: 0,
    },
    card: {
        margin: 2,
        backgroundColor: uiColor.Very_Pole_Grey,
        padding: 4
    },
    empty: {
        margin: 3,
        flex: 1,
        height: 80,
        backgroundColor: uiColor.Polo_Grey,
        justifyContent: 'space-between',
    }
});