/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-plusplus */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import { useMutation, useQuery, useLazyQuery } from '@apollo/react-hooks';
import { CREATE_GARAGEPOST, UPDATE_GARAGEPOST, GARAGECALENDARPOST } from 'gql/calendar';
import * as R from 'ramda';
import React from 'react';
import { ScrollView, TouchableOpacity, View } from 'react-native';
import { PickerType } from 'screens/Main/Calendar/PickerType';
import { ALL_REPAIRS } from 'screens/Main/MainScreen/GQL';
import { RepairsItem } from 'screens/TaskEditor/components/table/component/repairs';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { UiPicker } from 'styled/UIComponents/UiPicker';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, timeLoading, timeHelper } from 'utils/helper';

const createPost = { "__typename": "GaragePostReserve", "endAt": "2020-06-13T22:30:00.000Z", "garagePost": { "__typename": "GaragePost", "garagePostType": { "__typename": "GaragePostType", "category": "bodyshop", "id": 13 }, "id": 87, "name": "12345" }, "garagePostId": 87, "id": 271, "startAt": "2020-06-13T22:00:00.000Z" };

export const VehicleId = ({
  vehicle, setVehicleId, time, post
}) => {
  const [posted, isPosted] = React.useState(post);
  const [state, setState] = React.useState(createPost);

  const { data, error, loading } = useQuery(ALL_REPAIRS, { variables: { where: { id: { eq: vehicle } } } });
  const [mutationQuery] = useMutation(CREATE_GARAGEPOST, {
    onCompleted: (item) => {
      setState(R.path(R.keys(item))(item))
    }
  });
  const [updateQuery] = useMutation(UPDATE_GARAGEPOST, {
    onCompleted: (item) => {
      setState(R.path(R.keys(item))(item))
    }
  });

  if (error) return (<View />);
  if (loading) return (<View />);

  const input = {
    startAt: R.path(['startAt'])(state),
    endAt: R.path(['endAt'])(state),
    repairId: vehicle,
    garagePostId: R.path(['garagePost', 'id'])(state),
  };

  const variables = {
    variables: {
      input: R.assocPath(['id'],
        R.path(['id'])(state))(
          R.dissoc('repairId', input))
    }
  };
  //  ThisCode

  const success = () => {
    const input = {
      startAt: R.path(['startAt'])(state),
      endAt: R.path(['endAt'])(state),
      repairId: vehicle,
      garagePostId: R.path(['garagePost', 'id'])(state),
    };

    if (R.path(['id'])(state)) {
      const variables = {
        variables: {
          input: R.assocPath(['id'],
            R.path(['id'])(state))(
              R.dissoc('repairId', input))
        }
      };


      return updateQuery(variables)
    }

    return mutationQuery({ variables: { input } })
  };

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between', }}>
      <View style={{ margin: 5 }}>
        <LabelText
          color={ColorCard('classic').font}
          fonts="ShellMedium"
          items="SRDescription"
          style={{
            textAlign: 'left',
            margin: 10,
          }}
        >
          Текущий автомобиль
        </LabelText>
        <TouchableOpacity
          onPress={() => setVehicleId(null)}
          style={{ minHeight: 200, margin: 5 }}
        >
          <RepairsItem
            data={R.defaultTo({ id: null })(R.head(R.path(['repairs', 'items'])(data)))}
          />
        </TouchableOpacity>
        <PickerType
          horizont={false}
          setState={(e) => {
            if (e) {
              setState(R.assocPath(['garagePost', 'id'], R.path(['id'])(e))(state))
            }
          }}
          state={{ id: R.path(['garagePost', 'id'])(state) }}
        />
        <AppScreen
          dispatch={(e) => setState(e)}
          post={posted}
          storage={state}
          time={time}
        />
      </View>
      <TouchesState
        color="Yellow"
        flex={1}
        onPress={() => success()}
        title={R.path(['id'])(state) ? "UPDATE" : "SAVE"}
      />
    </ScrollView >
  )
};

const AppScreen = ({
  post, storage, dispatch, time
}) => {
  const [timer] = React.useState(R.head(time));
  const [garage, setGarage] = React.useState([{ startAt: null, endAt: null }]);
  const [loadgreeting] = useLazyQuery(GARAGECALENDARPOST, {
    fetchPolicy: "network-only",
    onCompleted: (data) => {
      setGarage(R.path(['garagePostReserves', 'items'])(data))
    }
  });

  React.useEffect(() => {
    loadgreeting(R.assocPath(['variables', 'where'], {
      // garagePost: { id: { eq: R.path(['id'])(post) } },
      startAt: { date: timeHelper('date', R.head(time)) }
    })({}))
  }, [post]);


  return (
    <View>
      <UiPicker
        dispatch={(e) => {
          if (e) {
            dispatch(R.assocPath(['startAt'], e)(storage))
          }
        }}
        element={{ path: "GARAGE" }}
        Items={timeLoading({ data: garage, time: timer })}
        main
        placeholder={{ id: R.defaultTo(null)(R.path(['startAt'])(storage)) }}
      />
      <EndTimePicker
        dispatch={(e) => {
          if (e) {
            dispatch(R.assocPath(['endAt'], e)(storage))
          }
        }}
        garage={garage}
        storage={storage}
        timer={timer}
      />
    </View>
  )
};

const EndTimePicker = ({
  storage, garage, timer, dispatch
}) => {
  if (R.path(['startAt'])(storage)) {
    return (
      <UiPicker
        dispatch={(e) => {
          dispatch(e)
        }}
        element={{ path: "GARAGE" }}
        Items={timeLoading({
          data: garage,
          time: timer,
          startAt: R.path(['startAt'])(storage)
        })}
        main
        placeholder={{ id: R.defaultTo(null)(R.path(['endAt'])(storage)) }}
      />
    )
  }

  return (
    <View />
  )
};