/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { Header } from 'screens/TaskEditor/components/header';
import { SearchInputLine } from 'screens/TaskEditor/components/header/component/SearchInputLine';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';
import { useNavigation } from 'hooks/useNavigation'
import { ArticleFind } from './components';
import { ItemSearch } from './components/ItemSearch';

const reducer = (state = { 'query': [] }, action) => {
    switch (action.type) {
        case 'Query':
            return R.assocPath(['query'], action.payload)(state);
        case "Text":
            return R.mergeAll([state, { text: action.payload, article: null }]);
        case "Article":
            return R.assocPath(['article'], action.payload)(state);
        default:
            return state;
    }
};

export const FindParts = ({ navigation }) => {
    const [storage, dispatch] = React.useReducer(reducer, { query: [] });
    const [current, setMoved] = useNavigation({ navigation, move: 'toggle' });

    const Connecter = R.cond([
        [R.path(['article']), () => (
            <ArticleFind
                {...R.path(['article'])(storage)}
                dispatch={(e) => dispatch(e)}
            />)],
        [R.path(['text']), () => (<ItemSearch
            dispatch={(e) => {
                dispatch(e)
            }}
            storage={storage}
        />)],
        [isOdd, () => (<View />)]
    ]);

    return (
        <Header
            dispatch={setMoved}
            header={{ title: "Slava" }}
            menu={{ title: "FindParts" }}
            search
            svg={current}
        >
            <ScrollView>
                <View
                    style={{
                        margin: 10
                    }}
                >
                    <SearchInputLine
                        dispatch={(e) => dispatch(e)}
                        mode={false}
                        state={{ text: R.path(['text'])(storage) }}
                    />
                    {isOdd(R.path(['text'])(storage)) ?
                        <CardComponents>
                            <LabelText
                                color={ColorCard('classic').font}
                                fonts="ShellLight"
                                items="SRDescription"
                                style={{
                                    textAlign: 'center',
                                    padding: 10
                                }}
                                testID="BlockLaker"
                            >
                                {i18n.t('ArticleSearch')}
                            </LabelText>
                        </CardComponents>
                        : <View />}
                </View>
                {isOdd(R.path(['text'])(storage)) ?
                    <View /> :
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellBold"
                        items="SSDescription"
                        style={{
                            textAlign: 'left',
                            padding: 10,
                            paddingBottom: 20,
                            textTransform: 'uppercase'
                        }}
                        testID="BlockLaker"
                    >
                        {i18n.t("PermissionMaterial")}
                    </LabelText>}
                {Connecter(storage)}
            </ScrollView>
            <View />
        </Header>
    )
};
