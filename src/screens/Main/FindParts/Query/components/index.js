/* eslint-disable dot-notation */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-key */
/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-wrap-multilines */
import { useQuery } from '@apollo/react-hooks';
import { QUERY_FIND_PARTS } from 'gql/findparts';
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { SplashScreen } from 'screens/Splash';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import styled from 'styled-components';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd, priceModed } from 'utils/helper';


export const ArticleFind = ({ article, brandId, dispatch }) => {
    const { data, error, loading } = useQuery(QUERY_FIND_PARTS, {
        variables: { where: { article, brandId } }
    });


    if (error) return (
        <SplashScreen
            msg={R.toString(error)}
            state="Error"
            testID="Error"
        />
    );
    if (loading) return (
        <SplashScreen
            state="Loading"
            testID="Loading"
        />
    );

    const title = ['ЗАПРОШЕННЫЙ АРТИКУЛ', 'АНАЛОГОВЫЙ АРТИКУЛ'];

    if (isOdd(R.path(['findParts'])(data))) return (
        <View>
            <View style={{ flex: 1, margin: 10 }} >
                <LabelText
                    color={ColorCard('error').font}
                    fonts="ShellBold"
                    items="SSDescription"
                    style={{ textAlign: 'left' }}
                >
                    {R.head(title)}
                </LabelText>
                <CardComponents style={{ padding: 5, marginTop: 10, borderWidth: 1 }}>
                    <CardComponents style={{ padding: 5, justifyContent: 'center', align: 'center' }}>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellBold"
                            items="SSDescription"
                            style={{ textAlign: 'left' }}
                        >
                            Указанный вами артикул {article} не имеет аналогов
                        </LabelText>
                    </CardComponents>
                </CardComponents>
            </View>
            <View
                style={{
                    justifyContent: 'center',
                    bottom: 0
                }}
            >
                <TouchesState
                    color="Yellow"
                    flex={1}
                    onPress={() => dispatch({ type: "Article", payload: null })}
                    title="BACK"
                    unTransform
                />
            </View>
        </View>
    );
    const SortedTableData = (x) => {
        return R.prepend(
            ['склад', 'кол-во', 'доставка на склад', 'Стоимость'],
            R.map((x) => R.paths(
                [['warehouse', 'name'], ['quantity'], ['average_period'], ['price']]
            )(x))(R.path(['offers'])(x)))
    };
    const SortedDataParts = (x) => {
        return R.zipObj(
            ['article', 'brand', 'name'],
            R.paths(
                [['article'], ['brand', 'name'], ['name']]
            )(x))
    };
    const ItemFindParts = R.pipe(
        R.path(['findParts']),
        R.map(SortedTableData),
    )(data);
    const naming = R.pipe(
        R.path(['findParts']),
        R.map(SortedDataParts),
    )(data);


    return (
        <ScrollView>
            {R.addIndex(R.map)((x, key) => (
                <CardComponents style={{ padding: 5 }}>
                    <>
                        <LabelText
                            color={ColorCard('error').font}
                            fonts="ShellBold"
                            items="SSDescription"
                            style={{ textAlign: 'left' }}
                        >
                            {R.equals(key, 0) ? R.head(title) : R.tail(title)}
                        </LabelText>
                        <View style={{ padding: 10 }}>
                            <View style={{ justifyContent: 'space-between', padding: 5 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <LabelText
                                        color={ColorCard('classic').font}
                                        fonts="ShellMedium"
                                        items="SRDescription"
                                        style={{ textAlign: 'left' }}
                                    >
                                        {naming[key]['brand']}
                                    </LabelText>
                                    <LabelText
                                        color={ColorCard('classic').font}
                                        fonts="ShellLight"
                                        items="SRDescription"
                                        style={{
                                            textAlign: 'left',
                                            paddingLeft: 15
                                        }}
                                    >
                                        {naming[key]['article']}
                                    </LabelText>
                                </View>
                                <LabelText
                                    color={ColorCard('classic').font}
                                    fonts="ShellMedium"
                                    items="SRDescription"
                                    style={{ textAlign: 'left' }}
                                >
                                    {naming[key]['name']}
                                </LabelText>
                            </View>
                        </View>
                        {R.addIndex(R.map)((elem, item) => (
                            <CardComponents
                                style={{
                                    shadowColor: "#000",
                                    shadowOffset: {
                                        width: 0,
                                        height: 1,
                                    },
                                    shadowOpacity: 0.20,
                                    shadowRadius: 1.41,
                                    elevation: 2,
                                    borderWidth: 0
                                }}
                            >
                                <Grid
                                    key={item}
                                >
                                    {R.addIndex(R.map)((x, key) => (
                                        <View
                                            style={{
                                                width: R.includes(key, [2, 3]) ? 90 : 60,
                                                textAlign: 'center',
                                                justifyContent: 'center',
                                                alignItems: 'center'
                                            }}
                                        >
                                            <LabelText
                                                color={ColorCard('classic').font}
                                                fonts={R.equals(item, 0) ? "ShellMedium" : "ShellMedium"}
                                                items="SDescription"
                                                style={{
                                                    textAlign: 'center',
                                                    textTransform: "uppercase",
                                                    // borderWidth: 1
                                                }}
                                            >
                                                {R.equals(key, 3) ? priceModed(x) : x}
                                            </LabelText>
                                        </View>
                                    ))(elem)}
                                </Grid>
                            </CardComponents>
                        ))(x)}
                    </>
                </CardComponents>
            ))(ItemFindParts)}
            <TouchesState
                color="Yellow"
                flex={1}
                onPress={() => dispatch({ type: "Article", payload: null })}
                title="BACK"
                unTransform
            />
        </ScrollView>
    )
};

const Grid = styled.View`
text-align:center;
justify-content:space-between;
flex-direction:row
`;