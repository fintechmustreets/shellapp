/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import moment from 'moment'
import * as R from 'ramda'
import React from 'react'
import { View } from 'react-native'
import { styles } from 'screens/Main/AddGoods/helper/style'
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style'
import { LabelText } from 'uiComponents/SplashComponents'
import { ColorCard } from 'utils/helper'

export const MileageList = ({ storage }) => (
    <CardComponents>
        {R.addIndex(R.map)((x, key) => (
            <CardComponents
                key={key}
                style={[styles.shadow, { marginBottom: 1 }]}
            >
                <RowComponents>
                    <View>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellBold"
                            items="SDescription"
                            style={{
                                textAlign: 'left',
                                flex: 1,
                                margin: 5,
                                textTransform: 'uppercase'
                            }}
                            testID="BlockLaker"
                        >
                            {moment(x.createdAt).format("DD.MM.YYYY")}
                        </LabelText>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SDescription"
                            style={{
                                textAlign: 'left',
                                flex: 1,
                                margin: 5,
                                textTransform: 'uppercase'
                            }}
                            testID="BlockLaker"
                        >
                            {moment(x.createdAt).format("HH:mm")}
                        </LabelText>
                    </View>
                    <View>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellBold"
                            items="SDescription"
                            style={{
                                textAlign: 'right',
                                flex: 1,
                                margin: 5,
                                textTransform: 'uppercase'
                            }}
                            testID="BlockLaker"
                        >
                            {x.value}
                        </LabelText>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SDescription"
                            style={{
                                textAlign: 'right',
                                flex: 1,
                                margin: 5,
                                textTransform: 'lowercase'
                            }}
                            testID="BlockLaker"
                        >
                            км
                        </LabelText>
                    </View>
                </RowComponents>
            </CardComponents>
        ))(R.path(['query', 'mileageHistory'])(storage))}
    </CardComponents>
);