import * as R from 'ramda'
import { isOdd } from 'utils/helper'

export const list = [
    'vehicleModels',
    'vehicleModifications',
    'plate',
    'vin',
    'frameNumber',
    'fuel',
    'year',
    'mileage',
    'color'
];

const update = (x) => R.pipe(
    R.without(R.takeLast(1, x)),
    R.append(R.join(' ', R.concat(R.takeLast(1, x), ['HP']))),
)(x);

export const HEADER = R.pipe(
    R.paths([
        ['query', 'year'],
        ['query', 'modification', 'model', 'manufacturer', 'name'],
        ['query', 'modification', 'model', 'name'],
        ['query', 'modification', 'model', 'subbody'],
        ['query', 'modification', 'litres'],
        ['query', 'modification', 'dinHp'],
    ]),
    R.reject(isOdd),
    update,
    R.join(', '));

export const model = R.pipe(
    R.paths([
        ['modification', 'model', 'manufacturer', 'name'],
        ['modification', 'model', 'name'],
    ]), R.join(' '), R.trim,
);

const modificationMask = (x) => {
    if (isOdd(x)) return ['', ''];

    return [
        R.join(' ', R.hasPath(['modification', 'litres']) ? [R.path(['modification', 'litres'])(x), 'л.'] : ['', '']),
        R.path(['modification', 'enginecode'])(x),
        R.join(' ', R.hasPath(['modification', 'dinHp']) ? [R.path(['modification', 'dinHp'])(x), 'л.c'] : ['', '']),
        R.join('-', R.paths([['modification', 'startYear'], ['modification', 'endYear']])(x))
    ]
};

export const modification = R.pipe(
    modificationMask,
    R.join(', '),
    R.replace(/,/g, ""),
    R.trim,
);

export const value = R.paths([
    ['plate'],
    ['vin'],
    ['frameNumber'],
    ['modification', 'fuel'],
    ['year'],
    ['mileage'],
    ['color']
]);

export const reducer = (state = {}, action) => {
    switch (action.type) {
        case "Query":
            return R.assocPath(['query'], action.payload)(state);
        case "Repair":
            return R.assocPath(['repairs', 'query'], action.payload)(state);
        default:
            return state
    }
};