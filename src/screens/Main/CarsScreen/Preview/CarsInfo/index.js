/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { ScrollView } from 'react-native';
import { EmployeeComponents } from 'screens/Main/AddGoods/components/EmployeeComponents';
import { MileageList } from 'screens/Main/CarsScreen/Preview/MileageList';
import { TableComponent } from 'screens/Main/CarsScreen/Preview/TableComponent';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import i18n from 'localization';
import { SplashScreen } from 'screens/Splash';
import { useQuery } from '@apollo/react-hooks';
import { VEHICLE_QUERY } from 'screens/Main/CarsScreen/GQL';

export const CarsInfo = ({ route }) => {
    const { data, error, loading } = useQuery(VEHICLE_QUERY, {
        fetchPolicy: "network-only",
        variables: {
            where: {
                id: { eq: R.path(['params', 'itemId', 'owner'])(route) },
            }
        }
    });

    if (loading) {
        return (
            <SplashScreen
                state="Loading"
                testID="LOADING"
            />
        )
    }
    if (error) {
        return (
            <SplashScreen
                state="Loading"
                testID="LOADING"
            />
        )
    }

    const daretrt = R.pipe(R.path(R.keys(data)), R.path(['items']), R.head)(data)

    return (
        <ScrollView style={{ flex: 1 }}>
            <TableComponent storage={{ query: daretrt }} />
            <EmployeeComponents
                actor={R.path(['owner'])(daretrt)}
                HEADER="owner"
            />
            {R.addIndex(R.map)((x, keys) => (
                <EmployeeComponents
                    key={keys}
                    actor={x}
                    HEADER="Clients"
                />
            ))(R.path(['clients'])(daretrt))}
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="CLabel"
                style={{
                    textAlign: 'left',
                    flex: 1,
                    margin: 5,
                    textTransform: 'uppercase'
                }}
                testID="BlockLaker"
            >
                {i18n.t('MmileageHistory')}
            </LabelText>
            <MileageList
                storage={{ query: daretrt }}
            />
        </ScrollView>
    )
};