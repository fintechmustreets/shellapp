/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { styles } from 'screens/Drawler/components/helper/styleSheets';
import { HEADER } from 'screens/Main/CarsScreen/Preview/config';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const TitleHeader = ({ storage, state, setState }) => (
    <View>
        <CardComponents style={styles.shadow}>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellBold"
                items="SSDescription"
                style={{
                    textAlign: 'left',
                    margin: 5,
                    textTransform: 'uppercase'
                }}
                testID="BlockLaker"
            >
                {HEADER(storage)}
            </LabelText>
        </CardComponents>
        <RowComponents
            style={{
                alignItems: 'center',
                margin: 10,
                marginBottom: 0
            }}
        >
            {R.map(x => (
                <TouchableOpacity
                    key={x}
                    onPress={() => setState(x)}
                    style={{
                        paddingLeft: 10,
                        paddingRight: 10
                    }}
                >
                    <LabelText
                        color={ColorCard(R.equals(x, state) ? 'error' : 'classic').font}
                        fonts="ShellMedium"
                        items="SRDescription"
                        style={[styLes.items, R.equals(x, state) ? {
                            borderBottomWidth: 3
                        } : {}]}
                        testID="BlockLaker"
                    >
                        {i18n.t(x)}
                    </LabelText>
                </TouchableOpacity>
            ))(['ListCar', 'TaskWorks'])}
            <View style={{ flex: 1 }} />
        </RowComponents>
    </View>
);

const styLes = StyleSheet.create({
    items: {
        textAlign: 'center',
        textTransform: 'none',
        paddingBottom: 5,
        borderColor: 'red',
    }
});