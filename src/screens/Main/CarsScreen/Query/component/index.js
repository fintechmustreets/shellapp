/* eslint-disable react/jsx-indent-props */
import { useQuery } from '@apollo/react-hooks';
import * as R from 'ramda';
import React from 'react';
import { Linking, View } from 'react-native';
import { VEHICLE_QUERY } from 'screens/Main/CarsScreen/GQL';
import { SplashScreen } from 'screens/Splash';
import { TableModel } from 'screens/TaskEditor/components/table';
import { CreateScreen } from 'utils/routing/index.json';

export const FormClientVehicle = ({ variables, navigation }) => {
    const ROUTER = CreateScreen.CarsScreen;
    const ROUTERVEHICLE = CreateScreen.DashBoardVehilcle;

    const { error, loading, data } = useQuery(VEHICLE_QUERY, {
        variables,
        fetchPolicy: "network-only"
    });

    if (error) return (
        <SplashScreen
            msg={R.toString(error)}
            state="Error"
            testID="Error"
        />
    );
    if (loading) return (
        <SplashScreen
            state="Loading"
            testID="Loading"
        />
    );

    return (
        <View style={{ flex: 1 }} >
            <TableModel
                data={R.path(['vehicles', 'items'])(data)}
                dispatch={(e) => {
                    if (R.includes(R.path(['type'])(e), ["Edit"])) {
                        return navigation.navigate(ROUTER, { mode: 'edit', itemId: e.payload })
                    }
                    if (R.path(["type"])(e) === "CALL") {
                        return Linking.openURL(`tel:${e.payload}`)
                    }
                    if (R.path(["type"])(e) === "Phone") {
                        return navigation.navigate(ROUTERVEHICLE)
                    }
                    if (R.path(['type'])(e) === "Create") {
                        return navigation.navigate(ROUTER, { mode: 'create', itemId: null })
                    }
                }}
                header="ListClientsCars"
                mode="Date"
                style={{ flex: 1 }}
            />
        </View>
    )
};