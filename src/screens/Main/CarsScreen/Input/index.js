/* eslint-disable multiline-ternary */
/* eslint-disable max-statements */
/* eslint-disable no-shadow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-redeclare */
/* eslint-disable react/no-multi-comp */
/* eslint-disable scanjs-rules/call_connect */
/* eslint-disable react/jsx-indent-props */
import { useQuery } from '@apollo/react-hooks';
import * as R from 'ramda';
import React from 'react';
import { SplashScreen } from 'screens/Splash';
import { isOdd } from 'utils/helper';
import { VEHICLE_QUERY } from '../GQL';
import { CarsInputScreen } from './component';

export const CarsEditScreen = (props) => {
    const { navigation } = props;
    const mode = R.path(['route', 'params', 'mode'])(props);
    const owner = R.path(['route', 'params', 'itemId', 'owner'])(props);
    const plate = R.path(['route', 'params', 'itemId', 'plate'])(props);
    const modificationId = R.path(['route', 'params', 'itemId', 'modificationId'])(props);
    const vin = R.path(['route', 'params', 'itemId', 'vin'])(props);
    const { error, loading, data } = useQuery(VEHICLE_QUERY, {
        variables: {
            "where": {
                vin: { eq: vin },
                modificationId: { eq: parseInt(modificationId) },
                plate: { eq: plate },
                // ownerId: { eq: parseInt(owner) }
            }
        }
    });

    if (error) {
        return (
            <CarsInputScreen
                data={R.assocPath(['ownerId'], { id: ownerId }, {})}
                navigation={navigation}
            />
        );
    }

    if (loading) return (
        <SplashScreen
            state="Loading"
            testID="ISODD"
        />
    );

    const modeState = R.head(R.path(['vehicles', 'items'])(data));

    return (
        <CarsInputScreen
            data={isOdd(owner) ?
                modeState :
                R.assocPath(['ownerId'], { id: owner }, modeState)}
            navigation={navigation}
        />
    )
};
