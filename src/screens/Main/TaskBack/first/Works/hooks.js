/* eslint-disable max-statements */
/* eslint-disable react/jsx-max-depth */
import { useLazyQuery } from '@apollo/react-hooks';
import { WORKCATALOG } from 'gql/billAccount';
import * as R from 'ramda';
import React from 'react';
import { showMessage } from 'react-native-flash-message';
import { shallowEqual, useSelector } from 'react-redux';

export const WorksHooks = () => {
  const { task } = useSelector(state => state.task, shallowEqual);
  const [state, setState] = React.useState({});

  const [cataloggreeting] = useLazyQuery(WORKCATALOG, {
    fetchPolicy: "network-only",
    onCompleted: (data) => setState(R.path(R.keys(data))(data)),
    onError: () => {
      showMessage({
        message: R.toString("ПРОВЕРЬТЕ ПРАВИЛЬНОСТЬ ДАННЫХ"),
        description: "Error",
        type: "info",
      })
      setState([])
    }
  });

  const request = React.useCallback(() => {
    if (R.path(['vehicle', 'modification', 'id'])(task)) {
      return cataloggreeting({
        variables: { where: { modificationId: { eq: R.path(['vehicle', 'modification', 'id'])(task) } } }
      })
    }

    return setState([])
  }, [task]);

  return [state, request]
};
