/* eslint-disable radix */
import * as R from 'ramda'

export const input = [
    {
        value: ["name"],
        title: "Error message",
        label: 'nameByAgent',
        multiline: true,
        main: true,
        path: 'name',
        maxLength: 120,
        keyboardType: "default",
    },
    {
        value: ["unitCount"],
        main: true,
        example: "",
        title: "Error message",
        label: 'unitCount',
        path: 'unitCount',
        keyboardType: "numeric",
    },
    {
        value: ["timeHrs"],
        main: true,
        example: "",
        title: "Error message",
        label: 'timeHrs',
        path: 'timeHrs',
        maxLength: 12,
        keyboardType: "numeric",
    },
    {
        value: ["unitPrice", 'amount'],
        main: true,
        example: "",
        title: "Error message",
        label: 'unitPrice',
        path: 'unitPrice',
        price: true,
        maxLength: 12,
        keyboardType: "numeric",
    },
];

export const discount = [
    {
        example: "",
        title: "Error message",
        label: 'discountPersent',
        mask: "[00] %",
        value: ['discount', 'percent'],
        keyboardType: "numeric",
    },
    {
        example: "",
        title: "Error message",
        label: 'discountCoast',
        value: ['discount', 'amount'],
        price: true,
        maxLength: 12,
        keyboardType: "numeric",
    },
]

export const createInput = ({ dis, task }) => ({
    name: R.path(['name'])(dis),
    unitCount: parseInt(R.path(['unitCount'])(dis)),
    timeHrs: parseFloat(R.path(['timeHrs'])(dis)),
    unitPrice: {
        amount: parseInt(R.path(['unitPrice', 'amount'])(dis))
    },
    discount: {
        percent: parseInt(R.defaultTo("0")(R.path(['discount', 'percent'])(dis))),
        amount: parseInt(R.defaultTo("0")(R.path(['discount', 'amount'])(dis))),
    },
    action: '',
    repairId: R.path(['id'])(task),
});

export const empty = ({ label, element }) => {
    if (R.equals(label, 'CatalogWork')) return true;
    if (R.path(['id'])(element)) return false;

    return true
};

export const dataInput = ({ dis }) => ({
    name: R.path(['name'])(dis),
    unitCount: parseInt(R.path(['unitCount'])(dis)),
    timeHrs: parseFloat(R.path(['timeHrs'])(dis)),
    unitPrice: {
        amount: parseInt(R.path(['unitPrice', 'amount'])(dis))
    },
    discount: {
        percent: parseInt(R.defaultTo("0")(R.path(['discount', 'percent'])(dis))),
        amount: parseInt(R.defaultTo("0")(R.path(['discount', 'amount'])(dis))),
    },
    action: R.defaultTo('UserStory')(R.path(['action'])(dis)),
    id: R.path(['id'])(dis),
});