/* eslint-disable operator-assignment */
/* eslint-disable radix */
/* eslint-disable newline-before-return */
import Rub from 'assets/svg/rub';
import React from 'react';
import { SvgXml } from 'react-native-svg';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, priceModed, totalWorkPrice } from 'utils/helper';

export const TotalCount = ({ storage }) => (
  <CardComponents style={[styles.shadow, { margin: 2, marginBottom: 20 }]}>
    <RowComponents style={{ justifyContent: 'center' }}>
      <LabelText
        color={ColorCard('classic').font}
        fonts="ShellBold"
        items="CLabel"
        style={[styles.container, {
          textAlign: 'center',
          textTransform: 'uppercase',
          margin: 2
        }]}
        testID="BlockLaker"
      >
        {priceModed(totalWorkPrice(storage))}
      </LabelText>
      <SvgXml height="15" weight="15" xml={Rub} />
    </RowComponents>
  </CardComponents>
);
