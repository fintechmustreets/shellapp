/* eslint-disable react/no-multi-comp */
/* eslint-disable multiline-ternary */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import Rub from 'assets/svg/rub';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { TouchableOpacity, View } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CatalogWork } from 'screens/Main/TaskBack/first/Works/Components/ModalWorkScreen';
import { MyWork } from 'screens/Main/TaskBack/first/Works/Components/MyWork';
import { WorksHooks } from 'screens/Main/TaskBack/first/Works/hooks';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { LabelText } from 'uiComponents/SplashComponents';
import { alertDestroyConfirm } from 'utils/api/funcComponent/alertConfirm';
import { ColorCard, isOdd } from 'utils/helper';

export const ModalWorkScreen = ({ element, dispatch, mode }) => {
    const modeIssue = R.defaultTo("CatalogWork")(isOdd(element) ? mode : 'MyWork')
    const [data, setData] = React.useState(element);
    const [state, request] = WorksHooks();
    const [label, setLabel] = React.useState(modeIssue);

    React.useEffect(() => {
        request('catalog')
    }, []);

    const title = "destroy_works";
    const alertReq = (e) => R.cond([
        [R.equals('Cancel'), () => null],
        [R.equals('Destroy'), () => {
            request('delete', R.path(['id'])(element));
            dispatch({ type: "CLOSED" })
        }],
    ])(R.path(['type'])(e));

    const success = (e) => {
        if (R.equals('CLOSED', R.path(['type'])(e))) {
            return dispatch(e)
        }
        if (R.equals("Action", R.path(['type'])(e))) {
            setData(R.mergeAll([element, R.path(['payload'])(e)]));

            return setLabel("MyWork")
        }
        if (R.equals("CATALOG", R.path(['type'])(e))) {
            return request('catalog')
        }
    };

    return (
        <View style={{ flex: 1 }}>
            <CardComponents>
                <HeaderApp
                    button="DESTROY"
                    dispatch={() => alertDestroyConfirm({
                        alertReq,
                        title
                    })}
                    empty
                    title={
                        R.equals(label, 'CatalogWork') ?
                            i18n.t("CustomLite") :
                            R.pipe(
                                R.path(['action']),
                                R.defaultTo(i18n.t('MyWork'))
                            )(element)
                    }
                />
                <RowComponents>
                    {R.addIndex(R.map)((x, key) => (
                        <TouchableOpacity
                            key={R.join('_', ['buttonWork', key])}
                            onPress={() => setLabel(x)}
                            style={[R.equals(x)(label) ? {
                                borderBottomWidth: 2,
                                borderColor: 'red'
                            } : {}]}
                        >
                            <LabelText
                                color={ColorCard(R.equals(x)(label) ? 'error' : 'classic').font}
                                fonts="ShellMedium"
                                items="SRDescription"
                                style={[styles.container, {
                                    textTransform: 'none'
                                }]}
                            >
                                {i18n.t(x)}
                            </LabelText>
                        </TouchableOpacity>
                    ))(R.equals(mode, 'MyWork') ? ['MyWork'] : ['CatalogWork', 'MyWork'])}
                </RowComponents>
            </CardComponents>
            <>
                {R.cond([
                    [R.equals('CatalogWork'), () => {
                        if (isOdd(state)) return (
                            <EmptyComponents
                                dispatch={success}
                            />
                        )

                        return (
                            <CatalogWork
                                dispatch={success}
                                state={state}
                            />
                        )
                    }],
                    [R.equals('MyWork'), () => (
                        <MyWork
                            navigation={success}
                            state={data}
                        />
                    )],
                ])(label)}
            </>
        </View>
    )
};

const EmptyComponents = ({ dispatch }) => {
    const data = [
        'Кажется, что-то пошло не так и каталог работ не загрузился. Это могло произойти по следующим причинам:',
        '- Приложение потеряло соединение с поставщиком данных. Попробуйте запросить каталог еще раз.',
        '- У поставщика данных отсутствует каталог работ на данную модификацию автомобиля'
    ]

    const button = [{
        title: "request",
        onPress: () => dispatch({ type: 'CATALOG' })
    }, {
        title: 'success',
        onPress: () => dispatch({ type: 'CLOSED' })
    }]

    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellBold"
                items="CLabel"
                style={{ textTransform: 'uppercase', textAlign: 'center', padding: 40 }}
            >
                OOPS!!
            </LabelText>
            {R.addIndex(R.map)((x, key) => (
                <View
                    key={key}
                >
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellBold"
                        items="SDescription"
                        style={{ textTransform: 'uppercase', textAlign: 'center', padding: 20 }}
                    >
                        {x}
                    </LabelText>
                </View>
            ))(data)}
            {R.addIndex(R.map)((x, key) => (
                <View
                    key={R.join('', [key, 'button'])}
                >
                    <TouchesState
                        color={R.equals(key, 1) ? 'Yellow' : "LightGrey"}
                        flex={1}
                        onPress={() => x.onPress()}
                        svg={R.equals(key, 1) ? null : Rub}
                        title={x.title}
                    />
                </View>
            ))(button)}
        </View>
    )
}