/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/no-multi-comp */
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Colors } from 'react-native-paper';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { SubWorks } from 'screens/Main/TaskBack/first/Works/Components/ModalWorkScreen/SubWorks';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';

export const ItemSub = ({ el, dispatch }) => {
  const [state, setState] = React.useState(false);

  return (
    <View>
      <TouchableOpacity
        onPress={() => setState(!state)}
        style={[{ marginLeft: 4, borderWidth: 1, borderColor: Colors.grey300 }]}
      >
        <LabelText
          color={ColorCard('classic').font}
          fonts="ShellMedium"
          items="SDescription"
          style={[styles.container, {
            marginTop: 10,
            marginBottom: 10,
          }]}
          testID="BlockLaker"
        >
          {el.name}
        </LabelText>
      </TouchableOpacity>
      {R.pipe(isOdd, R.not)(state) ?
        R.map(elem => (
          <SubWorks
            key={elem.id}
            dispatch={e => dispatch(e)}
            elem={elem}
          />
        ))(R.path(['works'])(el)) :
        <View />}
    </View>
  )
};
