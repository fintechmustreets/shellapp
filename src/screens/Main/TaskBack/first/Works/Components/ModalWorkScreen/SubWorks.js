/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/no-multi-comp */
import Rub from 'assets/svg/rub';
import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { uiColor } from 'styled/colors/uiColor.json';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, priceModed } from 'utils/helper';
import * as R from 'ramda'

export const SubWorks = ({ elem, dispatch }) => (
    <CardComponents
        style={{
            marginLeft: 10,
            margin: 2,
            backgroundColor: uiColor.Very_Pole_Grey
        }}
    >
        <TouchableOpacity onPress={() => dispatch({ type: 'Action', payload: R.omit(['id'])(elem) })}>
            <CardComponents
                auto
                style={{
                    backgroundColor: uiColor.Very_Pole_Grey,
                    paddingBottom: 10
                }}
            >
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SDescription"
                    style={[styles.container, { textTransform: 'none' }]}
                    testID="BlockLaker"
                >
                    {elem.name}
                </LabelText>
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="SDescription"
                    style={styles.container}
                    testID="BlockLaker"
                >
                    {elem.action}
                </LabelText>
            </CardComponents>
            <CardComponents>
                <RowComponents>
                    <View>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SDescription"
                            style={styles.container}
                            testID="BlockLaker"
                        >
                            Время
                        </LabelText>
                        <RowComponents>
                            <LabelText
                                color={ColorCard('classic').font}
                                fonts="ShellMedium"
                                items="SDescription"
                                style={styles.container}
                                testID="BlockLaker"
                            >
                                {elem.timeHrs}
                            </LabelText>
                            <LabelText
                                color={ColorCard('classic').font}
                                fonts="ShellMedium"
                                items="SDescription"
                                style={styles.container}
                                testID="BlockLaker"
                            >
                                ч.
                            </LabelText>
                        </RowComponents>
                    </View>
                    <View>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SDescription"
                            style={styles.container}
                            testID="BlockLaker"
                        >
                            Стоимость
                        </LabelText>
                        <RowComponents>
                            <LabelText
                                color={ColorCard('classic').font}
                                fonts="ShellBold"
                                items="SDescription"
                                style={styles.container}
                                testID="BlockLaker"
                            >
                                {priceModed(elem.unitPrice.amount)}
                            </LabelText>
                            <SvgXml height="10" weight="10" xml={Rub} />
                        </RowComponents>
                    </View>
                </RowComponents>
            </CardComponents>
        </TouchableOpacity>
    </CardComponents >
);
