/* eslint-disable no-unused-vars */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import { Colors, IconButton } from 'react-native-paper';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { alertDestroyConfirm } from 'utils/api/funcComponent/alertConfirm';
import { ColorCard, priceModed } from 'utils/helper';
import { SubHeaderWork } from 'screens/Main/TaskBack/first/Works/Components/AppScreen/SubHeaderWork';
import i18n from 'localization';
import { useWorkHooks } from 'hooks/useWorkHooks';
import Down from 'assets/svg/arrayDown';
import Left from 'assets/svg/arrayLeft';
import rub from 'assets/svg/rub';
import { SvgXml } from 'react-native-svg';

export const AppScreen = ({ items }) => {
    const [list, fetch, catalog, destroy] = useWorkHooks();
    const [state, setState] = React.useState(null);

    const title = "destroy_works";
    const alertReq = (e) => R.cond([
        [R.equals('Cancel'), () => null],
        [R.equals('Destroy'), () => destroy(items)],
    ])(R.path(['type'])(e));

    const summary = R.pipe(R.paths([['unitPrice', 'amount'], ['timeHrs'], ['unitCount']]), R.product)(items);
    const totalSummary = summary - R.defaultTo(0)(R.add(R.path(['discount', 'amount'])(items), summary * (R.pipe(R.path(['discount', 'percent']))(items) / 100)));

    return (
        <View style={{ flex: 1 }}>
            <CardComponents
                style={[styles.shadow, {
                    margin: 1,
                    flex: 1,
                    minHeight: 65
                }]}
            >
                <TouchableOpacity
                    onPress={() => setState(!state)}
                    style={{ flex: 1 }}
                >
                    <RowComponents style={{ flex: 1 }}>
                        <View
                            style={{
                                flex: 1,
                                alignItems: 'center',
                                flexDirection: 'row'
                            }}
                        >
                            <View style={{ padding: 10 }}>
                                <SvgXml
                                    height="10"
                                    width="10"
                                    xml={R.equals(state, true) ? Down : Left}
                                />
                            </View>
                            <View
                                style={{
                                    flex: 1,
                                    justifyContent: 'flex-start'
                                }}
                            >
                                <LabelText
                                    color={ColorCard('classic').font}
                                    fonts="ShellMedium"
                                    items="SDescription"
                                    style={[styles.container, {
                                        textTransform: 'uppercase',
                                    }]}
                                >
                                    {R.path(['name'])(items)}
                                </LabelText>
                                <LabelText
                                    color={ColorCard('classic').font}
                                    fonts="ShellLight"
                                    items="Description"
                                    style={[styles.container, {
                                        textTransform: 'uppercase',
                                    }]}
                                >
                                    {R.defaultTo(i18n.t('MyWork'))(R.path(['action'])(items))}
                                </LabelText>
                            </View>
                        </View>
                        <View
                            style={{
                                flex: 1,
                                flexDirection: 'row',
                                justifyContent: 'flex-end',
                                alignItems: 'center'
                            }}
                        >
                            <LabelText
                                color={ColorCard('classic').font}
                                fonts="ShellMedium"
                                items="SRDescription"
                                style={[styles.container, {
                                    textAlign: 'center',
                                    textTransform: 'lowercase',
                                }]}
                                testID="BlockLaker"
                            >
                                {priceModed(totalSummary)}
                            </LabelText>
                            <SvgXml
                                height="10"
                                width="15"
                                xml={rub}
                            />
                        </View>
                        <IconButton
                            color={Colors.red500}
                            icon="delete"
                            onPress={() => alertDestroyConfirm({
                                alertReq,
                                title
                            })}
                            size={20}
                        />
                    </RowComponents>
                </TouchableOpacity>
            </CardComponents>
            <SubHeaderWork
                el={items}
                state={state}
            />
        </View>
    )
};