/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { shallowEqual, useSelector } from 'react-redux';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle/index';
import { InputComponents } from 'utils/component';
import { statusTaskChecker } from 'utils/helper';
import { initialStateState } from 'screens/Main/TaskBack/first/Describe/config'
import { DesribeHooks } from 'screens/Main/TaskBack/first/Describe/hook';

export const Describe = () => {
    const { task } = useSelector(state => R.path(['task'])(state), shallowEqual);
    const [update, redux, describeUpdate] = DesribeHooks();

    return (
        <View style={{ flex: 1 }}>
            <HeaderApp
                button="added"
                dispatch={() => describeUpdate()}
                empty={update}
                title={i18n.t("Description")}
            />
            <View
                style={{ marginRight: 10, marginLeft: 10 }}
            >
                <InputComponents
                    {...initialStateState}
                    editable={R.includes('description')(R.path(['edit'])(statusTaskChecker(R.path(['status'])(task))))}
                    onChangeText={(change) => redux(change)}
                    storage={task}
                />
            </View>
        </View>
    )
};