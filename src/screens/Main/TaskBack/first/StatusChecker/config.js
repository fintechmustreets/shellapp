/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
import { StyleSheet } from 'react-native';
import styled from 'styled-components';

export const StatusId = [
    { value: "canceled" },
    { value: "suspended" },
    { value: "reconciliation" },
    { value: "closed" },
    { value: "completed" },
    { value: "inWork" },
    { value: "preorder" },
    { value: "AllWork" },
];

// TODO COLORS UPDATE
export const qweqweq = StyleSheet.create({
    qweq: { width: 35, height: 35, backgroundColor: 'lightblue' },
    triger: {
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'ShellMedium'
    },
    switch: {
        width: 30,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'ShellMedium'
    }
});

export const LabelTextMenu = styled.Text`
font-family:"ShellLight";
font-weight:600;
font-size:${props => props.small ? 14 : 10};
color:${props => props.error ? 'red' : 'black'};
`;
