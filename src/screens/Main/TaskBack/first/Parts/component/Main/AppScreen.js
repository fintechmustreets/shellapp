/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable max-statements */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
import Down from 'assets/svg/arrayDown';
import Left from 'assets/svg/arrayLeft';
import rub from 'assets/svg/rub';
import { usePartsHooks } from 'hooks/usePartHooks';
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Colors, IconButton } from 'react-native-paper';
import { SvgXml } from 'react-native-svg';
import { shallowEqual, useSelector } from 'react-redux';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { Subheader } from 'screens/Main/TaskBack/first/Parts/component/Main/Subheader';
import { totalPartsPrice } from 'screens/Main/TaskBack/first/Parts/component/Modal/Component/TotalCount';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { alertDestroyConfirm } from 'utils/api/funcComponent/alertConfirm';
import { ColorCard, priceModed } from 'utils/helper';

export const AppScreen = () => {
    const [state, setState] = React.useState(false);

    const [_, __, ___, destroy] = usePartsHooks();
    const { task } = useSelector(store => store.task, shallowEqual);
    const title = "destroy_repair_part";

    const alertReq = (e) => R.cond([
        [R.equals('Cancel'), () => null],
        [R.equals('Destroy'), () => destroy(R.path(['params'])(e))],
    ])(R.path(['type'])(e));

    return (
        <View style={{ flex: 1 }}>
            {R.addIndex(R.map)((el, key) => (
                <View key={key} style={{ flex: 1 }}>
                    <CardComponents
                        style={[styles.shadow, { margin: 1 }]}
                    >
                        <TouchableOpacity
                            onPress={() => setState(R.equals(state, key) ? false : key)}
                        >
                            <RowComponents
                                style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <View
                                    style={{
                                        flex: 1,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        flexDirection: 'row',
                                    }}
                                >
                                    <View
                                        style={{
                                            padding: 10,
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        }}
                                    >
                                        <SvgXml
                                            height="10"
                                            width="10"
                                            xml={R.equals(state, key) ? Down : Left}
                                        />
                                    </View>
                                    <View style={{
                                        flex: 1,
                                        justifyContent: 'flex-start',
                                    }}
                                    >
                                        <LabelText
                                            color={ColorCard('classic').font}
                                            fonts="ShellMedium"
                                            items="SDescription"
                                            style={[{
                                                textTransform: 'uppercase',
                                                padding: 5
                                            }]}
                                        >
                                            {R.path(['name'])(el)}
                                        </LabelText>
                                        <LabelText
                                            color={ColorCard('classic').font}
                                            fonts="ShellLight"
                                            items="SDescription"
                                            style={[{
                                                textTransform: 'uppercase',
                                                padding: 5
                                            }]}
                                        >
                                            {R.path(['article'])(el)}
                                        </LabelText>
                                    </View>
                                </View>
                                <View
                                    style={{
                                        flex: 1,
                                        flexDirection: 'row',
                                        justifyContent: 'flex-end'
                                    }}
                                >
                                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 5 }}>
                                        <LabelText
                                            color={ColorCard('classic').font}
                                            fonts="ShellMedium"
                                            items="SSDescription"
                                            style={[{
                                                textAlign: 'center',
                                                textTransform: 'uppercase',
                                            }]}
                                        >
                                            {priceModed(totalPartsPrice(el))}
                                        </LabelText>
                                        <SvgXml
                                            height="10"
                                            width="15"
                                            xml={rub}
                                        />
                                    </View>
                                </View>
                                <IconButton
                                    color={Colors.red500}
                                    icon="delete"
                                    onPress={() => alertDestroyConfirm({
                                        alertReq,
                                        title,
                                        params: el
                                    })}
                                    size={20}
                                />
                            </RowComponents>
                        </TouchableOpacity>
                    </CardComponents>
                    <Subheader data={el} el={el} keys={1} state={R.equals(state, key)} />
                </View>
            ))(R.path(['parts'])(task))}
        </View>
    )
};
