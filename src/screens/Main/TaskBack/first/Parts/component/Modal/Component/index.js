/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable max-statements */
/* eslint-disable space-before-blocks */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable no-else-return */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-props-no-spreading */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { SearchItem } from 'screens/Main/TaskBack/first/Parts/component/Modal/Component/SearchItem';
import { SelectAction } from 'screens/Main/TaskBack/first/Parts/component/Modal/Component/SelectAction';
import { TotalCount } from 'screens/Main/TaskBack/first/Parts/component/Modal/Component/TotalCount';
import { partInput, discount } from 'screens/Main/TaskBack/first/Parts/component/Modal/config';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { InputComponents } from 'utils/component';
import { isOdd } from 'utils/helper';
import { ButtonComponents } from 'screens/Main/TaskBack/first/helper/ButtonComponents';
import { usePartsCreateHooks } from 'hooks/usePartHooks'
import { shallowEqual, useSelector } from "react-redux";
import { RowViewDiscount } from 'styled/UIComponents/RowViewDiscount'

const usePriceHooks = ({ redux, storage }) => {
    const [state, setState] = React.useState(null)
    // const isMax = (x) => {
    //     console.log(x)
    //     if (R.pipe(R.length, R.equals(2), R.not)(x)) return 0

    //     if (R.head(x) < R.tail(x)) {
    //         return R.head(x)
    //     }

    //     return R.last(x)
    // }

    // const avaragePartsIn = R.pipe(R.paths([['unitCount'], ['depotItem', 'amount', 'available']]), R.reject(isOdd), isMax)

    React.useEffect(() => {
        if (redux) {
            setState(redux)
        }
        if (storage) {
            setState(storage)
        }
    }, [redux])

    const update = React.useCallback((data) => {
        setState(R.mergeAll([state, data]))
    })

    return [state, update]
}

export const Component = ({
    redux,
    setRedux,
    screen,
    setScreen,
    storage,
    setState
}) => {
    const { task } = useSelector(state => state.task, shallowEqual);
    const [state, setUpdate] = usePriceHooks({ redux, storage })
    const [create, update, reduxFn, _] = usePartsCreateHooks({ navigation: setScreen, state: storage })

    if (isOdd(R.path(['name'])(screen))) {
        return (
            <View
                style={{
                    flex: 1,
                    justifyContent: 'space-between'
                }}
            >
                <SearchItem
                    dispatch={setRedux}
                    style={{ flex: 1 }}
                />
                <ButtonComponents
                    dispatch={() => setState({ isVisible: false })}
                />
            </View>
        )
    }

    const changeText = (e) => {
        const data = R.assocPath(
            R.path(['items', 'value'])(e),
            R.path(['change'])(e)
        )({})

        if (R.includes('discount')(R.path(['items', 'value'])(e))) {
            const discount = {
                discount: R.mergeAll([
                    R.path(['discount'])(state),
                    R.zipObj(R.tail(R.path(['items', 'value'])(e)), [R.path(['change'])(e)])
                ])
            }

            return setUpdate(R.mergeAll([state, discount]))
        }

        if (R.equals(R.head(R.path(['items', 'value'])(e)), ['depotItem'])) {
            return setUpdate(R.mergeAll([state, { depotItem: R.mergeAll([R.path(['depotItem'])(state), data]) }]))
        }

        return setUpdate(R.mergeAll([state, data]))
    };

    const success = (e) => {
        if (R.equals(R.path(['type'])(e), 'BACK')) {
            return setState({ isVisible: false })
        }
        if (R.path(['id'])(task)) {
            if (R.path(['id'])(storage)) {
                update(state)

                return setState({ isVisible: false })
            }
            create(state)

            return setScreen({})
        }
        reduxFn(state)
        if (isOdd(storage)) {
            if (R.includes(R.path(['type'])(e), ['BACK', 'added'])) {
                return setState({ isVisible: false })
            }
        }
    }

    const itemsChange = (change, items) => changeText({
        change,
        items
    })

    const DiscountComponents = () => (
        <>
            {R.addIndex(R.map)((items, key) => (
                <RowViewDiscount
                    key={key}
                    items={R.equals(key, 1)}
                    style={{ flex: 1 }}
                >
                    <InputComponents
                        {...items}
                        onChangeText={(change) => itemsChange(change, items)}
                        storage={state}
                    />
                </RowViewDiscount>
            ))(discount)}
        </>
    )

    const CurrentComponents = () => R.addIndex(R.map)((items, key) => (
        <View
            key={key}
        >
            <InputComponents
                {...items}
                onChangeText={(change) => itemsChange(change, items)}
                storage={state}
            />
        </View>
    ))(R.without([discount])(partInput))

    return (
        <View style={{ flex: 1 }}>
            <CardComponents style={{ flex: 1 }}>
                <SelectAction
                    dispatch={setRedux}
                    storage={redux}
                />
                <View style={{ flex: 1 }}>
                    {CurrentComponents()}
                    <View
                        style={{ flexDirection: 'row', flex: 1 }}
                    >
                        {DiscountComponents()}
                    </View>
                </View>
                <TotalCount storage={state} />
            </CardComponents>
            <ButtonComponents
                dispatch={success}
            />
        </View>
    )
};