/* eslint-disable react/jsx-max-depth */
import * as R from 'ramda';
import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, priceModed } from 'utils/helper';

export const SelectAction = ({ storage, dispatch }) => (
  <CardComponents
    style={[styles.shadow, SelectStyle.margin]}
  >
    <TouchableOpacity
      onPress={() => dispatch({ type: "ChangeArticle" })}
      style={[styles.shadow, { flex: 1 }]}
    >
      <LabelText
        color={ColorCard('blue').font}
        fonts="ShellBold"
        items="CLabel"
        style={[styles.container, SelectStyle.left]}
        testID="BlockLaker"
      >
        {R.path(['name'])(storage)}
      </LabelText>
      <RowComponents>
        <LabelText
          color={ColorCard('classic').font}
          fonts="ShellBold"
          items="SDescription"
          style={[styles.container, SelectStyle.left]}
          testID="BlockLaker"
        >
          {R.path(['article'])(storage)}
        </LabelText>
        <RowComponents>
          <LabelText
            color={ColorCard('classic').font}
            fonts="ShellBold"
            items="SDescription"
            style={[styles.container, SelectStyle.right]}
            testID="BlockLaker"
          >
            {R.defaultTo(0)(priceModed(R.path(['depotItem', 'amount', 'available'])(storage)))}
          </LabelText>
          <LabelText
            color={ColorCard('classic').font}
            fonts="ShellBold"
            items="SDescription"
            style={[styles.container, SelectStyle.right]}
            testID="BlockLaker"
          >
            {R.path(['depotItem', 'measure', 'abbr'])(storage)}
          </LabelText>
        </RowComponents>
      </RowComponents>
    </TouchableOpacity>
  </CardComponents>
);

const SelectStyle = StyleSheet.create({
  margin: {
    margin: 1,
    marginBottom: 10
  },
  left: {
    textAlign: 'left',
    textTransform: 'uppercase',
    // flex: 1
  },
  right: {
    textAlign: 'right',
    textTransform: 'uppercase',
    // flex: 1
  }
});