/* eslint-disable max-statements */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable newline-before-return */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { ScrollView } from 'react-native';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { Component } from 'screens/Main/TaskBack/first/Parts/component/Modal/Component';
import { reducer } from 'screens/Main/TaskBack/first/Parts/component/Modal/config';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';

export const ModalStage = ({ storage, setState }) => {
  const [redux, setRedux] = React.useReducer(reducer, storage);
  const [screen, setScreen] = React.useState(storage);

  const titleApp = R.path(['id'])(redux) ? i18n.t("EditingParts") : i18n.t("AddingParts");

  return (
    <ModalDescription style={{ flex: 1 }}>
      <HeaderApp
        dispatch={() => null}
        empty
        header
        storage={storage}
        title={titleApp}
      />
      <ScrollView contentContainerStyle={{ flexGrow: 1 }} style={{ padding: 5 }}>
        <Component
          redux={redux}
          screen={screen}
          setRedux={(e) => {
            if (R.equals(R.path(['type'])(e), 'ChangeArticle')) {
              setScreen({})
            }
            if (R.equals(R.path(['type'])(e), 'CREATE')) {
              setScreen(R.path(['payload'])(e))
            }
            setRedux(e)
          }}
          setScreen={setScreen}
          setState={setState}
          storage={storage}
        />
      </ScrollView>
    </ModalDescription>
  )
};