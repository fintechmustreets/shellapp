/* eslint-disable arrow-body-style */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
import * as R from 'ramda';
import React from 'react';
import { StyleSheet } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CCurrentSelect } from 'screens/Main/TaskBack/first/CarsEdit/Cars/index';
import { TableModelStreet } from 'screens/Main/TaskBack/first/CarsEdit/CurrentSelect/TableModelStreet';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { uiColor } from 'styled/colors/uiColor.json';
import { TouchesState } from 'styled/UIComponents/UiButtom';

export const CurrentSelect = ({
  state, setState, mode, setMode, create
}) => {
  const { data } = CCurrentSelect();
  const ColorButton = R.pipe(R.length, R.equals(2))(mode) ? [R.head(mode)] : mode;

  return (
    <CardComponents
      style={[styles.shadow, TableStyle.card, { flex: 1 }]}
    >
      <RowComponents style={{ marginBottom: 10 }}>
        {R.addIndex(R.map)((x, key) => (
          <TouchesState
            key={R.join('', ['buttonSelect', key])}
            color={R.equals(x, ColorButton) ? 'Yellow' : 'VeryGrey'}
            flex={1}
            merge
            onPress={() => setMode(x)}
            title={x}
          />
        ))(R.map(x => {
          return [x]
        })(data))}
      </RowComponents>
      <TableModelStreet
        create={create}
        mode={mode}
        setState={setState}
        state={state}
      />
    </CardComponents >
  )
};

const TableStyle = StyleSheet.create({
  line: {
    marginTop: 15,
    marginBottom: 15,
    borderColor: uiColor.Mid_Grey,
    borderWidth: 0.2,
    marginLeft: 0,
    marginRight: 0,
  },
  card: {
    margin: 2,
    backgroundColor: uiColor.Very_Pole_Grey,
    padding: 4
  }
});