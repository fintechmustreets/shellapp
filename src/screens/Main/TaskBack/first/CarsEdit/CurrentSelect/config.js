/* eslint-disable radix */
import * as R from 'ramda';
import { StyleSheet } from 'react-native';
import { uiColor } from 'styled/colors/uiColor.json';
import { isOdd } from 'utils/helper';
import { shallowEqual, useSelector } from 'react-redux';

export const itemState = [{
  id: "vin",
  path: 'vin',
  keyboardType: "default",
  label: 'vin',
  main: true,
  example: 'Minimum 17 character',
  title: "Error message",
  value: ["vin"],
  maxLength: 17,
},
{
  id: "frameNumber",
  path: 'frameNumber',
  keyboardType: "default",
  label: 'frameNumber',
  main: true,
  example: 'Minimum 9 character',
  title: "Error message",
  value: ["frameNumber"],
  maxLength: 9,
}];

export const itemConvState = [{
  id: "vin",
  path: 'vin',
  keyboardType: "default",
  label: 'vin',
  main: true,
  example: 'Minimum 17 character',
  title: "Error message",
  value: ["vin"],
  maxLength: 17,
},
{
  id: "plate",
  path: 'plate',
  keyboardType: "default",
  label: 'plate',
  main: true,
  example: 'Minimum 9 character',
  title: "Error message",
  value: ["plate"],
  maxLength: 9,
}];

export const ScreenHeader = R.cond([
  [isOdd, R.always({ title: 'NewTaskCars', bool: false })],
  [R.T, R.always({ title: 'EditTaskCars', bool: true })],
]);

export const currentSelect = () => {
  const { task } = useSelector(state => state.task, shallowEqual);

  if (R.path(['id'])(task)) return { data: ['vin'] };
  if (R.path(['vehicle'])(task)) return {
    data: [
      R.pipe(
        R.path(['vehicle']),
        R.pick(['vin', 'frameNumber', 'plate']),
        R.reject(isOdd),
        R.keys,
        R.head
      )(task)
    ]
  };

  return { data: ['vin', 'frameNumber', 'plate'] }
};

export const qweqwe = [
  {
    id: "ГОД",
    path: 'middle',
    keyboardType: "numeric",
    label: 'year',
    main: true,
    title: "Error message",
    value: ["year"],
    maxLength: 4,
  },
  {
    id: "ПРОБЕГ",
    path: 'mileage',
    keyboardType: "numeric",
    label: 'mileage',
    main: true,
    title: "Error message",
    value: ["mileage"],
    maxLength: 6,
  }, {
    id: "ГосНомер",
    path: 'plate',
    keyboardType: "default",
    label: 'plate',
    main: true,
    title: "Error message",
    value: ["plate"],
    maxLength: 17,
  },
  {
    id: "ЦВЕТ",
    path: 'color',
    keyboardType: "default",
    label: 'color',
    title: "Error message",
    value: ["color"],
    maxLength: 32,
  },
];

export const dataInput = ({ task }) => ({
  vehicle: {
    vin: R.path(['vehicle', 'vin'])(task),
    year: parseInt(R.path(['vehicle', 'year'])(task)),
    mileage: parseInt(R.path(['vehicle', 'mileage'])(task)),
    plate: R.path(['vehicle', 'plate'])(task),
    color: R.path(['vehicle', 'color'])(task),
    modificationId: R.path(['vehicle', 'modification', 'id'])(task),
  },
  owner: {
    name: {
      first: R.path(['owner', 'name', 'first'])(task),
      middle: R.path(['owner', 'name', 'middle'])(task),
      last: R.path(['owner', 'name', 'last'])(task),
    },
    phone: R.path(['owner', 'phone'])(task)
  },
  client: {
    name: {
      first: R.path(['client', 'name', 'first'])(task),
      middle: R.path(['client', 'name', 'middle'])(task),
      last: R.path(['client', 'name', 'last'])(task),
    },
    phone: R.path(['client', 'phone'])(task)
  },
  description: R.path(['description'])(task)
});

export const TableStyle = StyleSheet.create({
  line: {
    marginTop: 15,
    marginBottom: 15,
    borderColor: uiColor.Mid_Grey,
    borderWidth: 0.2,
    marginLeft: 0,
    marginRight: 0,
  },
  card: {
    margin: 2,
    backgroundColor: uiColor.Very_Pole_Grey,
    padding: 4
  },
  items: {
    textAlign: 'left',
    flex: 1,
    textTransform: 'uppercase',
    marginLeft: 10,
    marginRight: 10,
  },
  ite: { marginTop: 10, marginBottom: 10 }
});