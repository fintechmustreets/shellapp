/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, StyleSheet, View } from 'react-native';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import i18n from 'localization'

export const SideLider = ({ storage, dispatch }) => {
    const success = (mode, e) => R.cond([
        [R.equals('Screen'), () => dispatch({ type: "screen", payload: e })],
        [R.equals('Recompomition'), () => dispatch({ type: "screen", payload: 'Recompomition' })],
    ])(mode);

    const TitleComp = (label) => (
        <LabelText
            color={ColorCard(R.equals(label, R.path(['screen'])(storage)) ? 'error' : 'classic').font}
            fonts="ShellMedium"
            items="SRDescription"
            style={[
                styleItem.items, {
                    borderBottomWidth: R.equals(label, R.path(['screen'])(storage)) ? 2 : 0,
                    textTransform: 'capitalize',
                }
            ]}
        >
            {i18n.t(label)}
        </LabelText>
    );

    return (
        <View>
            <RowComponents style={{ marginLeft: 5, marginRight: 5 }}>
                {R.addIndex(R.map)((x, keys) => (
                    <TouchableOpacity
                        key={R.join('_', [keys, 'HeaderNavigate', x])}
                        onPress={() => success("Screen", x)}
                    >
                        {TitleComp(x)}
                    </TouchableOpacity>
                ))(['Mistake', 'Conclution', 'Recompomition'])}
            </RowComponents>
        </View >
    )
};

const styleItem = StyleSheet.create({
    items: {
        padding: 5,
        textTransform: 'uppercase',
        borderColor: 'red'
    },
    margin: { marginRight: 10, marginLeft: 10 }
});