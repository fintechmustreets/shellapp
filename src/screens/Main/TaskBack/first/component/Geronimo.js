/* eslint-disable no-unused-vars */
/* eslint-disable jsx-quotes */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import moment from 'moment';
import * as R from 'ramda';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import Share from 'assets/svg/print-button-svgrepo-com';
import { SvgXml } from 'react-native-svg';
import {
    Menu, MenuOption, MenuOptions, MenuTrigger
} from 'react-native-popup-menu';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { LabelTextMenu, qweqweq, } from 'screens/Main/TaskBack/first/StatusChecker/config';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import i18n from 'localization'
import Modal from 'react-native-modal';
import { shallowEqual, useSelector } from 'react-redux';
import { WorkDocuments, dataDoc } from 'screens/Main/TaskBack/first/component/config'
import { usePdfManipulationFile } from 'hooks/usePdfManipulationFile'

export const Geronimo = () => {
    const [modal, setModal] = React.useState({ isVisible: false });
    const { task } = useSelector(state => state.task, shallowEqual);
    const [document, shareFile, alertPdfMessage] = usePdfManipulationFile()

    const placeholder = R.join(' ', [
        i18n.t('TaskWork'),
        "#",
        R.path(['id'])(task),
        i18n.t('from'),
        moment(R.path(['createdAt'])(task)).format("DD.MM.YYYY HH:mm")
    ]);

    return (
        <RowComponents style={stylesItems.margin}>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellBold"
                items="SRDescription"
                style={stylesItems.text}
                testID="BlockLaker"
            >
                {placeholder}
            </LabelText>
            <View style={styles.shadow}>
                <Menu>
                    <MenuTrigger
                        style={qweqweq.switch}
                    >
                        <SvgXml
                            height="20"
                            weight="20"
                            xml={Share}
                        />
                    </MenuTrigger>
                    <MenuOptions>
                        {R.addIndex(R.map)((x, key) => (
                            <MenuOption
                                key={R.join('_', [key, 'Items', x.value])}
                                onSelect={() => alertPdfMessage({ title: 'Slava', params: { id: R.path(['id'])(task), url: dataDoc(x.name) } })}
                                style={{ color: R.equals(x.name, 'GX') ? 'red' : 'black', fontFamily: 'ShellMedium' }}
                            >
                                <LabelTextMenu>
                                    <LabelTextMenu
                                        small
                                    >
                                        {i18n.t(x.value)}
                                    </LabelTextMenu>
                                    <LabelTextMenu
                                        error={R.equals(x.name, 'GX') ? 'red' : 'black'}
                                    >
                                        {R.path(['status'])(x) ? i18n.t(x.status) : ''}
                                    </LabelTextMenu>
                                </LabelTextMenu>
                            </MenuOption>
                        ))(WorkDocuments)}
                    </MenuOptions>
                </Menu>
            </View>
        </RowComponents >
    )
};

const stylesItems = StyleSheet.create({
    svg: {
        paddingLeft: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        textAlign: 'left',
        flex: 1,
        textTransform: 'uppercase',
    },
    margin: { marginRight: 10, marginLeft: 10, marginTop: 0, marginBottom: 5 }
});