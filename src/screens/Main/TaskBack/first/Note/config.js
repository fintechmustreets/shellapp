export const initialStateState = {
    id: "ИМЯ",
    path: 'note',
    main: true,
    multiline: true,
    scrollEnabled: true,

    keyboardType: "default",
    value: ['note'],

    maxLength: 250,
    testID: "NameInput",
};
