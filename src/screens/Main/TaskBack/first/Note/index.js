/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { shallowEqual, useSelector } from 'react-redux';
import { initialStateState } from 'screens/Main/TaskBack/first/Note/config';
import { DesribeHooks } from 'screens/Main/TaskBack/first/Note/hook';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { InputComponents } from 'utils/component';

export const Note = () => {
    const { task } = useSelector(state => R.path(['task'])(state), shallowEqual);
    const [update, redux, describeUpdate] = DesribeHooks();
    if (R.includes(R.path(['status'])(task), ['preorder', null, undefined])) return (<View />);

    return (
        <View >
            <HeaderApp
                button="added"
                dispatch={() => describeUpdate()}
                empty={update}
                title={i18n.t("Note")}
            />
            <View
                style={{ marginRight: 10, marginLeft: 10 }}
            >
                <InputComponents
                    {...initialStateState}
                    editable={R.includes(R.path(['status'])(task), ['closed', 'completed'])}
                    onChangeText={(change) => redux(change)}
                    storage={task}
                />
            </View>
        </View>
    )
};