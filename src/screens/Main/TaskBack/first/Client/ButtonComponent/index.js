/* eslint-disable no-constant-condition */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-indent-props */
import { useMutation } from '@apollo/react-hooks';
import { UPDATE_REPAIR } from 'gql/task';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { showMessage } from 'react-native-flash-message';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { ButtonComponents } from 'screens/Main/TaskBack/first/helper/ButtonComponents';

export const ButtonComponent = ({
    setState, mode, currentCheck, disabled
}) => {
    const { task } = useSelector(state => state.task, shallowEqual);
    const dispatchRedux = useDispatch();
    const [employeeUpdate] = useMutation(UPDATE_REPAIR, {
        onCompleted: (data) => {
            dispatchRedux({ type: 'TASK_UPDATE', payload: R.path([R.keys(data)])(data) });
            setState({ isVisible: false });

            return showMessage({
                message: R.toString(i18n.t('succesAddedTask')),
                description: "Success",
                type: "success",
            });
        },
    });

    const data = R.cond([
        [R.equals('client'), R.always({
            client: {
                name: {
                    last: R.path(['name', 'last'])(currentCheck),
                    first: R.path(['name', 'first'])(currentCheck),
                    middle: R.path(['name', 'middle'])(currentCheck),
                },
                phone: R.path(['phone'])(currentCheck),
            }
        })],
        [R.equals('owner'), R.always({
            owner: {
                name: {
                    last: R.path(['name', 'last'])(currentCheck),
                    first: R.path(['name', 'first'])(currentCheck),
                    middle: R.path(['name', 'middle'])(currentCheck),
                },
                phone: R.path(['phone'])(currentCheck),
            }
        })],
        [R.equals('performer'), R.always({
            performerId: R.path(['payload', 'id'])(currentCheck),
        })],
    ])(mode);

    const success = (e) => {
        if (R.equals('added', R.path(['type'])(e))) {
            if (R.path(['id'])(task)) {
                employeeUpdate({ variables: { input: R.mergeAll([R.path(['id'])(task) ? { id: R.path(['id'])(task) } : {}, data]) } })
            } else {
                dispatchRedux({
                    type: "TASK_UPDATE",
                    payload: R.mergeAll([task, data])
                })
            }
        }

        return setState({ isVisible: false })
    };

    return (
        <ButtonComponents
            disabled={disabled}
            dispatch={(e) => success(e)}
        />
    )
};