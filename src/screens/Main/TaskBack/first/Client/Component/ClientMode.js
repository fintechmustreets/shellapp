/* eslint-disable react/jsx-max-depth */
/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { InputComponentState } from 'screens/Main/ClientScreen/Input/component/input';
import { VisualTable } from 'screens/TaskEditor/components/table/component';
import { isOdd } from 'utils/helper';
import { useSelector, shallowEqual } from 'react-redux';

export const ClientMode = ({ mode, dispatch, storage, }) => {
    const { task } = useSelector(redux => redux.task, shallowEqual);

    return R.cond([
        [R.equals('client'), () => (
            <InputComponentState
                dispatch={dispatch}
                storage={storage}
            />
        )],
        [R.equals('owner'), () => (
            <InputComponentState
                dispatch={dispatch}
                storage={storage}
            />
        )],
        [R.equals('performer'), () => (
            <VisualTable
                button={false}
                current={R.path(['performer', 'id'])(task)}
                dispatch={(e) => dispatch({
                    type: 'performer',
                    payload: { id: R.path(['id'])(e) }
                })}
                list="taskEmployee"
                mode="employee"
                unsearch
            />
        )],
        [isOdd, () => (<View />)],
    ])(mode)
};