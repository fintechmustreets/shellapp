/* eslint-disable no-else-return */
/* eslint-disable max-statements */
/* eslint-disable no-constant-condition */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { shallowEqual, useSelector } from 'react-redux';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { ButtonComponent } from 'screens/Main/TaskBack/first/Client/ButtonComponent';
import { ClientMode } from 'screens/Main/TaskBack/first/Client/Component/ClientMode';
import { PolicyScreen } from 'screens/Main/TaskBack/first/Client/PolicyScreen';
import { reducer } from 'screens/Master/CreateEmployeeScreen/reducer';
import { PolicyAdd } from 'utils/support/policyAdd';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';
import { ClientMistake, mistake } from 'screens/Main/TaskBack/first/Client/Component/Mistake'

export const ClientModal = ({
    mode, setState
}) => {
    const { task } = useSelector(state => state.task, shallowEqual);
    const [currentCheck, setChecker] = React.useReducer(reducer, R.path([mode])(task));
    const [policy, setReadPolicy] = React.useState(null);
    const [checkbox, setCheckBox] = React.useState(false);

    const ListClient = R.cond([
        [R.equals('client'), R.always(i18n.t("AddedClient"))],
        [R.equals('owner'), R.always(i18n.t('AddedOwner'))],
        [R.equals('performer'), R.always(i18n.t('AddedPerformer'))],
    ]);

    if (R.equals(policy, false)) {
        return (
            <PolicyScreen
                checkbox={checkbox}
                setCheckBox={(e) => {
                    setReadPolicy(e);
                    setCheckBox(e)
                }}
                setReadPolicy={(e) => setReadPolicy(e)}
            />
        )
    }

    const policyCheck = () => {
        if (R.equals('performer', mode)) return true;
        if (R.includes(mode, ['client', 'owner'])) {
            if (isOdd(R.path([mode, 'id'])(task))) {
                return false
            } else {
                return true
            }
        }

        return null
    };

    const disabled = R.cond([
        [R.equals('owner'), R.always(R.includes(false)(
            R.union(
                R.pipe(mistake, R.not)(currentCheck),
                R.path(['id'])(currentCheck) ? [true] : [checkbox])
        ))],
        [R.equals('client'), R.always(R.includes(false)(
            R.union(
                R.pipe(mistake, R.not)(currentCheck),
                R.path(['id'])(currentCheck) ? [true] : [checkbox])
        ))],
        [R.equals('performer'), R.always(R.has(['payload', 'id'])(currentCheck))]
    ])(mode);

    return (
        <ModalDescription style={{ padding: 0, flex: 1 }}>
            <LabelText
                color={ColorCard('error').font}
                fonts="ShellBold"
                items="CLabel"
                style={[styles.container, {
                    textAlign: 'left',
                    padding: 10,
                    textTransform: 'uppercase',
                }]}
                testID="BlockLaker"
            >
                {ListClient(mode)}
            </LabelText>
            <ScrollView>
                <View
                    style={[R.equals(mode, 'performer') ? {} : { padding: 10 }]}
                >
                    <ClientMode
                        dispatch={setChecker}
                        mode={mode}
                        storage={currentCheck}
                    />
                </View>
                <PolicyAdd
                    checkbox={checkbox}
                    id={policyCheck()}
                    setCheckBox={setCheckBox}
                />
                {R.equals(mode, 'performer') ?
                    <View /> :
                    <View style={{ margin: 10 }} >
                        <ClientMistake state={currentCheck} />
                    </View>}
            </ScrollView>
            <ButtonComponent
                currentCheck={currentCheck}
                disabled={disabled}
                mode={mode}
                setState={setState}
            />
        </ModalDescription>
    )
};