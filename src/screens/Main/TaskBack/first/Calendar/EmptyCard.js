/* eslint-disable react/jsx-key */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import React from 'react';
import { StyleSheet } from 'react-native'
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { styles } from 'screens/Main/AddGoods/helper/style';
import i18n from 'localization'
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const EmptyCard = () => (
    <CardComponents
        style={[styles.shadow, Styles.items]}
    >
        <LabelText
            color={ColorCard('classic').font}
            fonts="ShellLight"
            items="SSDescription"
            style={{ textAlign: 'center', }}
            testID="BlockLaker"
        >
            {i18n.t('falsePay')}
        </LabelText>
    </CardComponents>
);

const Styles = StyleSheet.create({
    items: {
        height: 50,
        flex: 1,
        justifyContent: 'center',
    }
});