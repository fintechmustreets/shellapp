/* eslint-disable react/jsx-key */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { ScrollView } from 'react-native';
import { shallowEqual, useSelector } from 'react-redux';
import moment from 'moment'
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { ColorCard } from 'utils/helper';
import { LabelText } from 'uiComponents/SplashComponents';
import { EmptyCard } from 'screens/Main/TaskBack/first/Calendar/EmptyCard'

export const TableScreenCalendar = () => {
    const { task } = useSelector(state => state.task, shallowEqual);

    const ScreenAdded = ({ title, value }) => (
        <RowComponents>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellLight"
                items="SSDescription"
                style={{
                    textAlign: 'center',
                }}
            >
                {title}
            </LabelText>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SSDescription"
                style={{
                    textAlign: 'center',
                }}
            >
                {value}
            </LabelText>
        </RowComponents>
    );

    return (
        <ScrollView
            horizontal
        >
            {R.addIndex(R.map)((x, keys) => {
                if (R.equals(R.path(['id'])(x), 'empty')) return (<EmptyCard />);

                return (
                    <CardComponents
                        key={R.join('_', ['postUpload', keys])}
                        style={[styles.shadow, { margin: 10, width: 250, height: 150 }]}
                    >
                        {ScreenAdded({ title: i18n.t("NAME"), value: R.path(['garagePost', 'name'])(x) })}
                        {ScreenAdded({ title: i18n.t("DATA"), value: moment(R.path(['startAt'])(x)).format("YYYY.MM.DD") })}
                        {ScreenAdded({ title: i18n.t("startAt"), value: moment(R.path(['startAt'])(x)).format("HH-mm") })}
                        {ScreenAdded({ title: i18n.t("endAt"), value: moment(R.path(['endAt'])(x)).format("HH-mm") })}
                    </CardComponents >
                )
            })(R.isEmpty(R.path(['garagePostReserves'])(task)) ? [{ id: 'empty' }] : R.path(['garagePostReserves'])(task))}
        </ScrollView >
    )
};
