/* eslint-disable react/jsx-wrap-multilines */
import React from 'react'
import { View } from 'react-native'
import { styled } from 'screens/Main/TaskBack/three/styled'
import { LabelText } from 'uiComponents/SplashComponents'
import { ColorCard } from 'utils/helper'

export const HeaderLine = ({ title }) => (
  <View style={styled.row}>
    <LabelText
      color={ColorCard('trans').font}
      fonts="ShellMedium"
      items="Description"
      style={styled.ewq}
      testID="BlockLaker"
    >
      {title}
    </LabelText>
    <View />
  </View>
);
