/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-key */
/* eslint-disable newline-before-return */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable react/no-multi-comp */
import * as R from 'ramda'
import React from 'react'
import { View } from 'react-native'
import { styles } from 'screens/Main/AddGoods/helper/style'
import { LabelText } from 'uiComponents/SplashComponents'
import { ColorCard, isOdd } from 'utils/helper'
import { styled } from 'screens/Main/TaskBack/three/styled'

export const Info = ({ capacities }) => R.addIndex(R.map)((x, key) => (
  <View
    key={key}
    style={[styled.qwe, styles.shadow]}
  >
    <View style={styled.circle}>
      <LabelText
        color={ColorCard('classic').font}
        fonts="ShellBold"
        items="CLabel"
        style={{ textAlign: 'center' }}
        testID="BlockLaker"
      >
        {R.join(' - ', R.reject(isOdd, R.values(R.omit(['__typename', 'range', 'description'])(x))))}
      </LabelText>
    </View>
    <LabelText
      color={ColorCard('classic').font}
      fonts="ShellMedium"
      items="SSDescription"
      style={{
        margin: 10,
        textAlign: 'center'
      }}
      testID="BlockLaker"
    >
      {R.defaultTo(R.path(['__typename'])(x))(R.path(['description'])(x))}
    </LabelText>
  </View >
))(capacities);