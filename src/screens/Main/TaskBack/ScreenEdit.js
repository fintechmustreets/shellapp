/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable max-statements */
import * as R from 'ramda';
import React from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { Colors } from 'react-native-paper';
import { useDispatch } from 'react-redux';
import { reducer } from 'screens/Main/TaskBack/first/component/config';
import { Geronimo } from 'screens/Main/TaskBack/first/component/Geronimo';
import { SideLider } from 'screens/Main/TaskBack/first/component/SideLider';
import { StatusChecker } from 'screens/Main/TaskBack/first/StatusChecker';
import { AppScreen } from 'screens/Main/TaskBack/screen';
import { TouchesState } from 'styled/UIComponents/UiButtom';

export const ScreenEdit = () => {
  const dispatchRedux = useDispatch();
  const [storage, dispatch] = React.useReducer(reducer, { screen: "Mistake" });

  return (
    <View style={{ flex: 1 }}>
      <ScrollView>
        <View style={Style.margin}>
          <StatusChecker />
          <Geronimo />
        </View>
        <SideLider
          dispatch={dispatch}
          storage={storage}
        />
        <AppScreen
          screen={R.path(['screen'])(storage)}
        />
        <TouchesState
          color="Yellow"
          flex={1}
          onPress={() => dispatchRedux({
            type: 'TASK_UPDATE',
            payload: { vehicle: { vin: null, plate: null }, task: 'Create' }
          })}
          title="ADDED"
        />
      </ScrollView >
    </View>
  )
};

const Style = StyleSheet.create({
  margin: {
    backgroundColor: Colors.grey300,
    marginRight: 5,
    marginLeft: 5,
    marginBottom: 10
  }
});