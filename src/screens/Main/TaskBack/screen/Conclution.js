/* eslint-disable no-undef */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-key */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { Describe } from 'screens/Main/TaskBack/first/Describe';
import { Works } from 'screens/Main/TaskBack/first/Works';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const ConclutionScreen = () => {
    const [storage, dispatch] = React.useState(null);

    return (
        <ScrollView>
            {R.map(() => (
                <CardComponents
                    style={[styles.shadow, {
                        margin: 10,
                    }]}
                >
                    <RowComponents>
                        {R.map((x) => (
                            <LabelText
                                color={ColorCard('classic').font}
                                fonts="ShellBold"
                                items="SDescription"
                                style={{
                                    textAlign: 'center',
                                    padding: 10,
                                    textTransform: 'uppercase'
                                }}
                                testID="BlockLaker"
                            >
                                {x}
                            </LabelText>
                        ))([
                            'ОФОРМЛЕНИЕ', 'СОГЛАСОВАНИЕ', 'РЕШЕНИЕ'
                        ])}
                    </RowComponents>
                    <ASdasda />
                    <View style={styles.line} />
                    <Describe
                        dispatch={(e) => dispatch(e)}
                        storage={storage}
                    />
                    <Works />
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="CLabel"
                        style={{
                            textAlign: 'left',
                            padding: 10,
                            flex: 1,
                            textTransform: 'uppercase'
                        }}
                        testID="BlockLaker"
                    >
                        {i18n.t('Total')}
                    </LabelText>
                </CardComponents>
            ))([1])}
        </ScrollView >
    )
};

export const ASdasda = () => {
    return (
        <RowComponents>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="CLabel"
                style={{
                    textAlign: 'left',
                    padding: 10,
                    flex: 1,
                    textTransform: 'uppercase'
                }}
                testID="BlockLaker"
            >
                Сопроводительный текст
            </LabelText>
            <TouchesState
                color="Yellow"
                flex={0}
                onPress={() => null}
                title="ADDED"
            />
        </RowComponents >
    )
};