/* eslint-disable no-extra-parens */
/* eslint-disable newline-after-var */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Client } from 'screens/Main/TaskBack/first/Client';
import { Describe } from 'screens/Main/TaskBack/first/Describe';
import { Note } from 'screens/Main/TaskBack/first/Note';
import { TableVehicle } from 'screens/Main/TaskBack/first/CarsEdit/TableVehicle'
import { Parts } from 'screens/Main/TaskBack/first/Parts';
import { Works } from 'screens/Main/TaskBack/first/Works';
// import { CalendarTask } from 'screens/Main/TaskBack/first/Calendar';
import { Colors } from 'react-native-paper';
import { App } from 'screens/Document'
// {/*<CalendarTask />*/}

export const MainScreen = () => (
  <View
    style={Style.item}
  >
    <TableVehicle />
    <Client />
    <Describe />
    <Works />
    <Parts />
    <Note />
    <App />
  </View>
);

const Style = StyleSheet.create({
  items: { backgroundColor: Colors.grey300, margin: 5 }
});