/* eslint-disable no-undefined */
/* eslint-disable max-statements */
/* eslint-disable newline-before-return */
/* eslint-disable radix */
/* eslint-disable react/jsx-max-depth */
import { useMutation } from '@apollo/react-hooks';
import { CREATE_REPAIR_TASK } from 'gql/task';
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Client } from 'screens/Main/TaskBack/first/Client';
import { TableVehicle } from 'screens/Main/TaskBack/first/CarsEdit/TableVehicle'
import { CarsEdit } from 'screens/Main/TaskBack/first/CarsEdit/Cars';
import { Header } from 'screens/TaskEditor/components/header';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { dataInput } from 'screens/Main/TaskBack/first/CarsEdit/CurrentSelect/config'
import { Describe } from 'screens/Main/TaskBack/first/Describe';
import { Note } from 'screens/Main/TaskBack/first/Note';
import { Preview } from 'utils/routing/index.json';
import { useNavigation } from 'hooks/useNavigation'

export const CreateTask = ({ navigation }) => {
  const [start] = React.useState(null);
  const [current, setMoved] = useNavigation({ navigation, move: 'toggle' });
  const { task } = useSelector(state => state.task, shallowEqual);
  const dispatchRedux = useDispatch();
  const ROUTER = Preview.PreviewTaskGarage;

  React.useEffect(() => {
    dispatchRedux({ type: 'TASK_UPDATE', payload: {} })
  }, [start]);

  const [createRepair] = useMutation(CREATE_REPAIR_TASK, {
    onCompleted: (data) => {
      dispatchRedux({ type: 'TASK_UPDATE', payload: R.path([R.keys(data)])(data) });
      return navigation.navigate(
        ROUTER, {
        "id": R.path(R.concat(R.keys(data), ['id']))(data),
      })
    }
  });

  const success = (e) => {
    if (R.equals(e, 'BACK')) return setMoved('goBack');

    return createRepair({ variables: { input: dataInput({ task }) } })
  };

  const disabled = R.pipe(R.paths([
    ['client', 'name', 'first'], ['client', 'name', 'last'], ['client', 'phone'],
    ['owner', 'name', 'first'], ['owner', 'name', 'last'], ['owner', 'phone'],
    ['description'],
    ['vehicle', 'modification', 'model', 'id'],
    ['vehicle', 'modification', 'dinHp'],
    ['vehicle', 'modification', 'model', 'manufacturer', 'id'],
  ]), R.uniq, R.includes(undefined))(task);

  if (R.path(['vehicle'])(task)) {
    return (
      <Header
        dispatch={setMoved}
        menu={{ title: "NewTaskCars" }}
        search
      >
        <View style={{ flex: 1 }}>
          <ScrollView style={{ flex: 1 }}>
            <TableVehicle />
            <Client />
            <Describe />
            <Note />
            <RowComponents>
              {R.map(func => (
                <TouchesState
                  key={func}
                  color={R.includes(func, ['BACK']) ? "VeryRed" : "Yellow"}
                  disabled={R.equals('SAVE', func) ? disabled : false}
                  flex={1}
                  onPress={() => success(func)}
                  title={func}
                />
              ))(["SAVE", 'BACK'])}
            </RowComponents>
          </ScrollView>

        </View>
      </Header>
    )
  }

  return (
    <CarsEdit
      create
      navigation={navigation}
    />
  )
};