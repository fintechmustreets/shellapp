/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable max-statements */
import addTwo from 'assets/svg/add';
import { useTaskHook } from 'hooks/useTaskHooks';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import Modal from 'react-native-modal';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CCurrentSelect } from 'screens/Main/TaskBack/first/CarsEdit/Cars/index';
import { itemConvState, itemState } from 'screens/Main/TaskBack/first/CarsEdit/CurrentSelect/config';
import { TextVersion } from 'screens/Main/TaskBack/first/CarsEdit/CurrentSelect/TextVersion';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { uiColor } from 'styled/colors/uiColor.json';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { InputComponents } from 'utils/component';
import { ScrollViewCars } from './Empty/ScrollViewCars';
import { VisualInput } from './Empty/VisualInput';

export const LoaderCarsScreen = () => {
  const [request] = useTaskHook()
  const { data } = CCurrentSelect();
  const [modal, setModal] = React.useState({ isVisible: false });
  const [state, setState] = React.useState()

  const list = R.innerJoin(
    (record, id) => record.path === id,
    R.union(itemState, itemConvState),
    [R.path(['mode'])(modal)]
  );

  return (
    <View style={{ flex: 1, justifyContent: 'space-between' }}>
      <CardComponents
        style={[styles.shadow, TableStyle.card]}
      >
        <TextVersion
          version={1}
        />
        {R.addIndex(R.map)((x, keys) => (
          <VisualInput
            key={keys}
            mode={x}
            onPress={setModal}
          />
        ))(data)}
        <View
          style={[styles.line, TableStyle.line]}
        />
        <TextVersion
          version={2}
        />
        <Modal isVisible={R.path(['isVisible'])(modal)}>
          <ModalDescription style={{ flex: 1, margin: 10 }}>
            <HeaderApp
              button="CLOSED"
              dispatch={() => setModal({ isVisible: false })}
              header
              title={i18n.t(R.join('_', ['search', R.path(['mode'])(modal)]))}
            />
            <View style={{ padding: 10 }}>
              {R.addIndex(R.map)((x, key) => (
                <InputComponents
                  key={key}
                  {...x}
                  onChangeText={(change) => setState(R.assocPath([R.path(['path'])(x)], change)(state))}
                  storage={state}
                />))(list)}
            </View>
            <ScrollViewCars
              mode={R.path(['mode'])(modal)}
              onPress={setModal}
              state={state}
            />
          </ModalDescription>
        </Modal>
      </CardComponents >
      <TouchesState
        color="Yellow"
        flex={1}
        onPress={() => request('SelectVehicle', { "__typename": 'Vehicle' })}
        style={{ textTransform: 'none' }}
        svg={addTwo}
        title="ADDEDCARS"
        unTransform
      />
    </View>
  )
};

const TableStyle = StyleSheet.create({
  line: {
    marginTop: 15,
    marginBottom: 15,
    borderColor: uiColor.Mid_Grey,
    borderWidth: 0.2,
    marginLeft: 0,
    marginRight: 0,
  },
  card: {
    margin: 2,
    backgroundColor: uiColor.Very_Pole_Grey,
    padding: 4
  }
});