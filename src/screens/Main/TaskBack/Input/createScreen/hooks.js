/* eslint-disable no-undefined */
/* eslint-disable radix */
/* eslint-disable newline-after-var */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable max-statements */
import * as R from 'ramda';
import React from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { UPDATE_REPAIR, REPAIR_QUERY } from 'gql/task';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import { showMessage } from "react-native-flash-message";

export const LoadingScreenHook = () => {
  const dispatchRedux = useDispatch();
  const { task } = useSelector(state => state.task, shallowEqual);
  const [update, setUpdate] = React.useState(false);

  const [loadgreeting] = useLazyQuery(REPAIR_QUERY, {
    fetchPolicy: "network-only",
    onCompleted: (data) => {
      if (data) {
        dispatchRedux({ type: 'TASK_UPDATE', payload: R.path([R.keys(data)])(data) });
        setUpdate('Close');
        return showMessage({
          message: "Автомобиль успешно изменен",
          description: "Success",
          type: "success",
        });
      }
    }
  });

  const [vehicleUpdate] = useMutation(UPDATE_REPAIR, {
    onError: (data) => showMessage({
      message: R.toString(data),
      description: "Error",
      type: "danger",
    }),
    onCompleted: (data) => {
      if (data) {
        loadgreeting({ variables: { where: { id: { eq: R.path(['id'])(task) } } } })
      }
    },
  });

  const Search = (data) => {
    setUpdate('Edit');
    dispatchRedux({ type: 'TASK_UPDATE', payload: { vehicle: R.omit(['id', 'ownerId', 'owner', 'clients'])(data) } })
  };

  const CreateCars = () => {
    setUpdate('Edit');
    dispatchRedux({ type: 'TASK_UPDATE', payload: R.assocPath(['vehicle'], { "__typename": 'Vehicle' })({}) })
  };

  const editCars = (data) => {
    dispatchRedux({ type: 'TASK_UPDATE', payload: R.assocPath(['vehicle'], data)({ "owner": {} }) })
  };

  const updateCars = (data) => {
    if (R.path(['id'])(task)) {
      return vehicleUpdate({ variables: { input: updateReq(task, data) } })
    }
    dispatchRedux({ type: 'TASK_UPDATE', payload: R.mergeAll([task, { vehicle: data }]) })
  };

  const request = (mode, data) => R.cond([
    [R.equals("Search"), () => Search(data)],
    [R.equals("Create"), () => CreateCars()],
    [R.equals("Refresh"), () => dispatchRedux({ type: 'TASK_UPDATE', payload: {} })],
    [R.equals('Edit_Cars'), () => editCars(data)],
    [R.equals('Update_Cars'), () => updateCars(data)],
    [R.T, () => null],
    [R.F, () => null],
  ])(mode);

  return [update, (mode, data) => request(mode, data)]
};

const updateReq = (task, data) => ({
  id: R.path(['id'])(task),
  vehicle: {
    id: R.path(['vehicle', 'id'])(task),
    vin: R.path(['vin'])(data),
    frameNumber: R.path(['frameNumber'])(data),
    year: parseInt(R.path(['year'])(data)),
    mileage: parseInt(R.path(['mileage'])(data)),
    plate: R.path(['plate'])(data),
    color: R.path(['color'])(data),
    modificationId: parseInt(R.path(['modification', 'id'])(data)),
  }
});

export const mistake = (x) => {
  const isOdd = (x) => R.pipe(R.includes(x))(['', null, undefined, ' ']);

  return [
    isOdd(R.path(['modification', 'model', 'id'])(x)),
    isOdd(R.path(['modification', 'model', 'manufacturer', 'id'])(x)),
    isOdd(R.path(['modification', 'id'])(x)),
    isOdd(R.path(['year'])(x)),
    isOdd(R.path(['plate'])(x)),
    isOdd(R.path(['vin'])(x)),
    isOdd(R.path(['mileage'])(x)),
  ]
};

export const disabled = R.pipe(
  mistake,
  R.uniq,
  R.includes(true)
);