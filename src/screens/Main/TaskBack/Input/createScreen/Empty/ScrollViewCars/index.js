/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-curly-brace-presence */
/* eslint-disable react/no-multi-comp */
import { useLazyQuery } from '@apollo/react-hooks';
import addTwo from 'assets/svg/add';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { VEHICLE_QUERY } from 'screens/Main/CarsScreen/GQL';
import { LoadingScreenHook } from 'screens/Main/TaskBack/Input/createScreen/hooks';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';
import { useTaskHook } from 'hooks/useTaskHooks'
import { styleCar } from './config';
import { ScrollContainerCars } from './ScrollContainerCars';

export const ScrollViewCars = ({ mode, state, onPress }) => {
  const [request] = useTaskHook()
  const [storage, setStorage] = React.useState([]);
  const [__, dispatch] = LoadingScreenHook();
  const [loadgreeting] = useLazyQuery(VEHICLE_QUERY, {
    fetchPolicy: "network-only",
    onCompleted: (data) => setStorage(R.path(R.concat(R.keys(data), ['items']))(data))
  });

  React.useEffect(() => {
    if (R.length(R.path([mode])(state)) > 1) {
      loadgreeting({ variables: R.assocPath(['where', mode, 'contain'], R.path([mode])(state))({}) })
    }
  }, [state]);

  const TableComponents = () => {
    if (isOdd(storage)) {
      return (
        <View style={{ flex: 1 }}>
          <View
            style={[styles.shadow, styleCar.empty]}
          >
            <LabelText
              color={ColorCard('classic').font}
              fonts="ShellMedium"
              items="SSDescription"
              style={{ padding: 15, textAlign: 'left' }}
            >
              {i18n.t('falsePay')}
            </LabelText>
          </View>
        </View>)
    }

    return (
      <ScrollContainerCars
        dispatch={dispatch}
        onPress={onPress}
        storage={storage}
      />
    )
  }

  const success = React.useCallback(() => {
    request('SelectVehicle', { "__typename": 'Vehicle' })

    return onPress({ isVisible: false })
  }, [])

  return (
    <View style={{ flex: 1 }}>
      {TableComponents()}
      <TouchesState
        color="Yellow"
        flex={1}
        onPress={success}
        style={[styles.shadow, styleCar.item]}
        svg={addTwo}
        title="ADDEDCARS"
        unTransform
      />
    </View>
  )
};
