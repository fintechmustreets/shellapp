/* eslint-disable react/jsx-key */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable newline-after-var */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/no-multi-comp */
import { useLazyQuery } from '@apollo/react-hooks';
import Down from 'assets/svg/arrayDown';
import Left from 'assets/svg/arrayLeft';
import { RESERVED_DEPOTS } from 'gql/garage';
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { CardScrollComponents } from 'screens/Main/AddGoods/helper/cardComponents';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';

const ElementHooks = ({ routeID }) => {
    const [state, setState] = React.useState([]);
    const [click, setClick] = React.useState(false);
    const [loadgreeting] = useLazyQuery(RESERVED_DEPOTS, {
        onCompleted: (data) => {
            setState(R.assocPath(['query'], data)(state))
        },
    });

    const success = React.useCallback((data) => {
        setClick(!click);
        loadgreeting({
            variables: {
                where: {
                    stockGroupId: { eq: data },
                    amount: { available: { gt: 0 } },
                    depotItems: {
                        amount: { available: { gt: 0 } },
                        depotId: isOdd(routeID) ? {} : { eq: routeID }
                    }
                },
                order: { id: "desc" },
                paginate: {
                    page: 1,
                    limit: 20
                }
            }
        })
    }, []);

    return [state, success]
};

export const ElementGarage = ({
    element, routeID, margin, dispatch
}) => {

    const [click, setClick] = React.useState(false);
    const [mode, setMode] = React.useState(false);
    const [state, success] = ElementHooks({ routeID });

    if (!element) return (<View />);

    if (R.pipe(R.path(['children']), R.isEmpty, R.not)(element)) {
        return R.addIndex(R.map)((elem, keys) => (
            <View
                key={R.join('_', [keys, 'parent'])}
            >
                <TouchableOpacity
                    onPress={() => {
                        setClick(!click);
                        setMode(!mode);
                        success(R.path(['parentId'])(elem))
                    }}
                    style={{
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                        flexDirection: 'row',
                        flex: 1,
                        marginBottom: 10,
                        marginTop: 10
                    }}
                >
                    <View
                        style={{ paddingRight: 10 }}
                    >
                        <SvgXml
                            height="10"
                            width="10"
                            xml={mode ? Down : Left}
                        />
                    </View>
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SSDescription"
                    >
                        {R.path(['parent', 'name'])(elem)}
                    </LabelText>
                </TouchableOpacity>
                <SelectedItems
                    click={click}
                    dispatch={(e) => dispatch(e)}
                    state={state}
                />
                {mode ? <SelecterShotter
                    element={elem} routeID={routeID} margin={margin} dispatch={dispatch}
                /> : <View />}
            </View>
        ))(R.path(['children'])(element))
    }

    return (
        <SelecterShotter
            dispatch={dispatch}
            element={element}
            margin={margin}
            routeID={routeID}
        />
    )
};

const SelecterShotter = ({
    element, routeID, margin, dispatch
}) => {
    const [state, success] = ElementHooks({ routeID });
    const [click, setClick] = React.useState(false);

    if (!element) return (<View />);

    return (
        <View style={[styles.shadow, { marginLeft: margin ? 40 : 0 }]}>
            <CardComponents>
                <TouchableOpacity onPress={() => {
                    setClick(!click);
                    success(R.path(['id'])(element))
                }}
                    style={{
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                        flexDirection: 'row',
                        flex: 1
                    }}
                >
                    <View
                        style={{ paddingRight: 10 }}>
                        <SvgXml
                            height="10"
                            width="10"
                            xml={click ? Down : Left}
                        />
                    </View>
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SDescription"
                    >
                        {R.path(['name'])(element)}
                    </LabelText>
                </TouchableOpacity>
            </CardComponents>
            <SelectedItems
                click={click}
                dispatch={(e) => dispatch(e)}
                state={state}
            />
        </View>
    )
};

const SelectedItems = ({ state, dispatch, click }) => {
    if (!click) return (<View />);
    if (!R.path(['query', 'stocks', 'items'])(state)) return (<View />);

    return (
        <View>
            {R.addIndex(R.map)((x, key) => (
                <View
                    key={R.join('_', [key, 'exact'])}
                    style={{ marginLeft: 40 }}
                >
                    <CardScrollComponents
                        date={x}
                        dispatch={(e) => dispatch(e)}
                        info
                        state={x}
                        updata
                    />
                </View>
            ))(R.path(['query', 'stocks', 'items'])(state))}
        </View>)
};