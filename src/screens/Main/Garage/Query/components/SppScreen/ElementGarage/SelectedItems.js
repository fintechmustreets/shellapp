/* eslint-disable react/jsx-key */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable newline-after-var */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/no-multi-comp */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { CardScrollComponents } from 'screens/Main/AddGoods/helper/cardComponents';

export const SelectedItems = ({ state, dispatch, click }) => {
    if (!click) return (<View />);
    if (!R.path(['query', 'stocks', 'items'])(state)) return (<View />);

    return R.addIndex(R.map)((x) => (
        <View
            key={R.toString(R.path(['query', 'stocks', 'items'])(state))}
            style={{ marginLeft: 40 }}
        >
            <CardScrollComponents
                date={x}
                dispatch={(e) => dispatch(e)}
                info
                state={x}
                updata
            />
        </View>
    ))(R.path(['query', 'stocks', 'items'])(state))
};