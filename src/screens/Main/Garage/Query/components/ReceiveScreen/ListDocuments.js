/* eslint-disable multiline-ternary */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-key */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable newline-after-var */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/no-multi-comp */
import i18n from 'localization';
import moment from 'moment';
import * as R from 'ramda';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useDispatch } from 'react-redux';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { CreateScreen } from 'utils/routing/index.json';

export const ListDocuments = ({ item, navigation }) => {
    const ROUTER = CreateScreen.ADDGOODS;
    const dispatchRedux = useDispatch();

    const dataSet = [{ x: i18n.t('createdAt'), y: moment(R.path(['createdAt'])(item)).format("DD-MM-YYYY HH:mm") }, { x: i18n.t('org'), y: R.join('\n', [R.path(['counterparty', 'name'])(item), i18n.t(R.path(['counterparty', 'type'])(item))]) }, { x: i18n.t("actor"), y: moment(R.path(['createdAt'])(item)).format("DD-MM-YYYY HH:mm") }, { x: i18n.t('document'), y: R.path(['document', 'type', 'name'])(item) ? R.join('\n', [R.join(' ', ['№', R.path(['document', 'id'])(item), i18n.t('from'), moment(R.path(['document', 'createdAt'])(item)).format('DD.MM.YYYY')]), R.path(['document', 'type', 'name'])(item)]) : '-' }, { x: i18n.t('position'), y: R.length(R.path(['items'])(item)) },
    ]

    const qwere = ({ x, y }) => (
        <RowComponents style={{ flex: 1 }}>
            <Text
                style={{ textAlign: 'right', fontFamily: 'ShellMedium', fontSize: 13 }}
            >
                {x}
            </Text>
            <Text
                style={{ textAlign: 'right', fontFamily: 'ShellLight', fontSize: 13 }}
            >
                {y}
            </Text>
        </RowComponents>
    )

    return (
        <TouchableOpacity
            onPress={() => {
                dispatchRedux({ type: "ADD_GOODS", payload: item })
                navigation.navigate(ROUTER)
            }}
            style={{ margin: 1 }}
        >
            <CardComponents
                style={[style.card, styles.shadow]}
            >
                {R.addIndex(R.map)((x, key) => (
                    <View key={key}>
                        {qwere(x)}
                    </View>
                ))(dataSet)}
            </CardComponents>
        </TouchableOpacity>
    )
};

const style = StyleSheet.create({
    card: {
        flex: 1,
        margin: 1
    }
})