/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable max-len */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { InputComponents } from 'utils/component';
import { promoInput } from 'screens/Drawler/components/helper/config'
import i18n from 'localization'
import { shallowEqual, useSelector } from 'react-redux';
import { useDepotsHoks } from 'hooks/useDepotsHoks'

export const ModalGarage = ({ dispatch, }) => {
    const { depots } = useSelector(state => state.garage, shallowEqual);
    const [load, update, destroy] = useDepotsHoks({ navigation: { navigate: () => dispatch('CLOSED') }, routeID: depots.id })
    const [storage, setState] = React.useState(depots)

    const success = (e) => R.cond([
        [R.equals("CLOSED"), () => dispatch("CLOSED")],
        [R.equals("SAVE"), () => {
            update({ variables: { input: R.omit(['__typename', 'screen'])(storage) } })
            dispatch('CLOSED')
        }],
    ])(e);

    return (
        <ModalDescription style={{ flex: 1, margin: 10 }}>
            <HeaderApp
                empty
                header
                title={i18n.t("EDIT_DEPOTS")}
            />
            <ScrollView style={{ padding: 10 }}>
                <View />
                {R.map(x => (
                    <InputComponents
                        key={x.path}
                        {...x}
                        onChangeText={(change) => setState(R.assocPath([x.path], change, storage))}
                        storage={storage}
                    />
                ))(promoInput)}
            </ScrollView>
            <RowComponents>
                {R.addIndex(R.map)((item, key) => (
                    <TouchesState
                        key={key}
                        color={R.equals("CLOSED", item) ? "VeryRed" : "Yellow"}
                        flex={1}
                        onPress={() => success(item)}
                        title={item}
                    />
                ))(["SAVE", "CLOSED"])}
            </RowComponents>
        </ModalDescription>
    )
};