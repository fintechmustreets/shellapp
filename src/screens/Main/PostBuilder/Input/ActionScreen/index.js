/* eslint-disable max-statements */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { InputComponents } from 'utils/component';
import { AliaseScreen } from 'screens/Main/PostBuilder/Input/AliaseScreen';
import { input } from 'screens/Main/PostBuilder/Input/config';

export const ActionScreen = ({ setState, state }) => (
    <View style={{ padding: 10 }}>
        {R.map(x => (
            <InputComponents
                key={x.path}
                {...x}
                onChangeText={(change) => setState(R.assocPath(x.value, change, state))}
                storage={state}
            />
        ))([R.head(input)])}
        <AliaseScreen
            setState={(e) => setState(e)}
            state={state}
        />
        {R.map(x => (
            <InputComponents
                key={x.path}
                {...x}
                onChangeText={(change) => setState(R.assocPath(x.value, change, state))}
                storage={state}
            />
        ))([R.last(input)])}
    </View>
);