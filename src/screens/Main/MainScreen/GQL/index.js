import gql from 'graphql-tag'

export const ALL_REPAIRS = gql`
query(
  $where: WhereRepairInput
  $order: OrderRepairInput
  $paginate: PageInput
) {
  repairs(where: $where, order: $order, paginate: $paginate) {
    info{
      page
      nextPage
      previousPage
      totalCount
    }
    items {
      id
      status
      description
      mileage
      client {
        id
        name {
          first
          last
          middle
        }
        phone
        createdAt
        updatedAt
      }
      owner {
        id
        name {
          first
          last
          middle
        }
        phone
        createdAt
        updatedAt
      }
      vehicle {
        id
        vin
        frameNumber
        year
        color
        plate
        plateHistory {
          value
          createdAt
        }
        mileage
        mileageHistory {
          value
          createdAt
        }
        modification {
          id
          provider
          providerId
          name
          enginecode
          dinHp
          fuel
          kw
          litres
          rpm
          startYear
          endYear
          subbody
          modelId
          model {
            id
            name
            subbody
            manufacturerId
            manufacturer {
              id
              name
            }
          }
        }
        owner {
          id
          name {
            first
            last
            middle
          }
          phone
          createdAt
          updatedAt
        }
        clients {
          id
          name {
            first
            last
            middle
          }
          phone
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      author {
        id
        name {
          first
          last
          middle
        }
        phone
        garageId
        garage {
          id
          name
          address {
            postcode
            state
            city
            street
            building
          }
          costPerHour {
            amount
            currency
          }
          phone
          email
          legalName
          legalAddress {
            postcode
            state
            city
            street
            building
          }
          bankName
          checkingAccount
          correspondentAccount
          rcbic
          defaultPartSurcharge
          inn
          kpp
          psrn
          createdAt
          updatedAt
        }
        role
        blocked
        createdAt
        updatedAt
      }
      performer {
        id
        name {
          first
          last
          middle
        }
        phone
        garageId
        garage {
          id
          name
          address {
            postcode
            state
            city
            street
            building
          }
          costPerHour {
            amount
            currency
          }
          phone
          email
          legalName
          legalAddress {
            postcode
            state
            city
            street
            building
          }
          bankName
          checkingAccount
          correspondentAccount
          rcbic
          defaultPartSurcharge
          inn
          kpp
          psrn
          createdAt
          updatedAt
        }
        role
        blocked
        createdAt
        updatedAt
      }
      garagePostReserves {
        id
        startAt
        endAt
        garagePostId
        garagePost {
          id
          name
          garagePostType {
            id
            category
          }
        }
        repairId
        repair {
          id
          description
          status
          performer {
            id
            name {
              first
              last
              middle
            }
            garageId
            garage {
              id
              name
            }
            role
          }
          totalPrice {
            amount
            currency
          }
          vehicle {
            id
            vin
            frameNumber
            year
            color
            plate
            plateHistory {
              value
              createdAt
            }
            mileage
            mileageHistory {
              value
              createdAt
            }
            modification {
              id
              provider
              providerId
              name
              enginecode
              dinHp
              fuel
              kw
              litres
              rpm
              startYear
              endYear
              subbody
              modelId
              model {
                id
                name
                subbody
                manufacturerId
                manufacturer {
                  id
                  name
                }
              }
            }
            owner {
              id
              name {
                first
                last
                middle
              }
              phone
              createdAt
              updatedAt
            }
            clients {
              id
              name {
                first
                last
                middle
              }
              phone
              createdAt
              updatedAt
            }
            createdAt
            updatedAt
          }
          createdAt
        }
        createdAt
        updatedAt
      }
      works {
        id
        name
        action
        modificationWorkId
        timeHrs
        unitPrice {
          amount
          currency
        }
        unitCount
        discount {
          amount
          percent
        }
        originalPrice {
          amount
          currency
        }
        totalPrice {
          amount
          currency
        }
      }
      parts {
        id
        repairId
        depotItemId
        name
        description
        article
        oem
        brand
        unitPrice {
          amount
          currency
        }
        unitCount
        discount {
          amount
          percent
        }
        originalPrice {
          amount
          currency
        }
        totalPrice {
          amount
          currency
        }
        depotItem {
          id
          stockId
          stock {
            id
            name
            description
            article
            oem
            brand
            measure {
              code
              precision
              name
              abbr
            }
            garageId
            stockGroupId
            stockGroup {
              id
              name
              parent {
                id
                name
              }
            }
            depotItems {
              id
              amount {
                available
              }
              priceOut {
                amount
                currency
              }
            }
            amount {
              available
              reserved
              released
            }
            createdAt
            updatedAt
          }
          depotId
          depot {
            id
            name
            description
            createdAt
            updatedAt
          }
          priceIn {
            amount
            currency
          }
          priceOut {
            amount
            currency
          }
          measure {
            code
            precision
            name
            abbr
          }
          amount {
            available
            reserved
            released
          }
          vat
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      totalPrice {
        amount
        currency
      }
      testimonial
      attachments {
        id
        objectId
        objectType
        objectProperty
        filename
        mimetype
        encoding
        file {
          url
          path
        }
      }
      closedDocuments {
        id
        objectId
        objectType
        objectProperty
        filename
        mimetype
        encoding
        file {
          url
          path
        }
      }
      note
      completedAt
      closedAt
      createdAt
      updatedAt
    }
  }
}
`;