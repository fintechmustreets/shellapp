/* eslint-disable react/jsx-indent-props */
import { useQuery } from '@apollo/react-hooks';
import { ALL_REPAIRS } from 'screens/Main/MainScreen/GQL'
import * as R from 'ramda';
import React from 'react';
import { Linking, View } from 'react-native';
import { SplashScreen } from 'screens/Splash';
import { TableModel } from 'screens/TaskEditor/components/table';
import { CreateScreen } from 'utils/routing/index.json';

export const FormClient = ({ variables, navigation, storage }) => {
    const ROUTER = CreateScreen.ClientScreen;
    const ROUTERVEHICLE = CreateScreen.DashBoardVehilcle;
    const { error, loading, data } = useQuery(ALL_REPAIRS, {
        variables,
        fetchPolicy: "network-only"
    });

    if (error) return (
        <SplashScreen
            msg={R.toString(error)}
            state="Error"
            testID="Error"
        />
    );
    if (loading) return (
        <SplashScreen
            state="Loading"
            testID="Loading"
        />
    );

    const success = ({ type, e }) => R.cond([
        [R.equals('Create'), () => navigation.navigate(ROUTERVEHICLE, { mode: "create" })],
        [R.equals('Edit'), () => navigation.navigate(ROUTER)],
        [R.equals('CALL'), () => Linking.openURL(`tel:${e.payload}`)],
        [R.equals('Phone'), () => navigation.navigate(ROUTERVEHICLE, { mode: "edit", itemId: R.path(['payload'])(e) })],
        [R.T, () => null],
        [R.F, () => null],
    ])(type);

    return (
        <View style={{ flex: 1 }} >
            <TableModel
                data={R.path(['repairs', 'items'])(data)}
                dispatch={(e) => success({ type: R.path(['type'])(e), e })}
                header={storage}
                mode="Date"
                style={{ flex: 1 }}
            />
        </View>
    )
};