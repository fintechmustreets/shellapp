/* eslint-disable no-confusing-arrow */
/* eslint-disable no-sparse-arrays */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable scanjs-rules/call_setTimeout */
/* eslint-disable react/jsx-wrap-multilines */
import { useNavigation } from 'hooks/useNavigation';
import i18n from 'localization';
import moment from 'moment';
import * as R from 'ramda';
import React from 'react';
import { STOCK_TREE_DEPOTS } from 'gql/garage';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import {
    ScrollView, View, Alert, Text
} from 'react-native';
import { shallowEqual, useSelector, useDispatch } from 'react-redux';
import { ContragentSelect } from 'screens/Main/AddGoods/components/ContragentSelect';
import { EmployeeComponents } from 'screens/Main/AddGoods/components/EmployeeComponents';
import { ComponentsGoods } from 'screens/Main/AddGoods/helper/components/ComponentsGoods';
import { Header } from 'screens/TaskEditor/components/header';
import { TotalSumGoods } from 'screens/Main/AddGoods/components/TotalSumGoods';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { CREATE_GOODS } from 'gql/addGoods'
import { isOdd } from 'utils/helper'
import { InWebView } from 'screens/Document/VideoPlayer/InWebView'
import { App } from 'screens/Document'

const useAddGoodsRequest = () => {
    const { goods } = useSelector(state => state.goods, shallowEqual);
    const dispatchRedux = useDispatch()
    const ref = React.useRef()

    const modification = (x) => {
        const depotsObj = R.pipe(
            R.path(['vat']),
            R.zipObj(['vat']),
            R.assocPath(['depotId'], R.pipe(R.path(['depotId']), R.defaultTo(R.path(['depots', 'id'])(x)))(x)),
            R.assocPath(['priceIn', 'amount'], parseFloat(R.path(['priceIn', 'amount'])(x))),
            R.assocPath(['vat'], parseInt(R.path(['vat'])(x))),
            R.assocPath(['amount'], parseFloat(R.path(['amount'])(x)))
        )(x)

        const stockObj = R.pipe(
            R.path(['depotItem', 'stock']),
            R.paths([['id'], ['stockGroupId'], ['article'], ['oem'], ['name'], ['description'], ['brand']]),
            R.append(
                R.pipe(
                    R.path(['depotItem', 'stock', 'measure']),
                    R.omit(['__typename'])
                )(x)),
            R.zipObj(['id', 'stockGroupId', 'article', 'oem', 'name', 'description', 'brand', 'measure']),
        )(x)

        return R.assocPath(['stock'], stockObj, depotsObj)
    }

    const [stockTree] = useLazyQuery(STOCK_TREE_DEPOTS, {
        fetchPolicy: "network-only",
        onCompleted: (data) => {
            if (data) {
                dispatchRedux({
                    type: "STOCK_TREE_DEPOTS_MERGE",
                    payload: data
                })
            }
        }
    });

    React.useEffect(() => {
        if (!R.equals(ref.current, true)) {
            stockTree()
            console.log("Stock")
            ref.current = true
        }
    }, [])

    const [createGoods] = useMutation(CREATE_GOODS, {
        onCompleted: (data) => {
            Alert.alert(
                i18n.t("HeaderAlert"),
                "Товарные позиции успешно добавлены на склад. Хотите прикрепить фото документов?",
                [
                    {
                        text: i18n.t('Cancel'),
                        onPress: () => dispatchRedux({
                            type: "ADD_GOODS", payload: {}
                        }),
                        style: "cancel"
                    },
                    {
                        text: i18n.t("Yes"),
                        onPress: () => dispatchRedux({
                            type: "ADD_GOODS",
                            payload: R.path(R.keys(data))(data)
                        })
                    },
                ],
                { cancelable: false }
            )
        },
        onError: (data) => console.log(data)
    })

    // TODO MISTAKE MESSAGE

    const listEmpty = () => {
        const notContragent = isOdd(R.path(['counterparty', 'id'])(goods))

        const empty = R.pipe(
            R.path(['items']),
            R.map(modification),
        )(goods)
    }

    const create = React.useCallback(() => {
        const variables = {
            input: R.assocPath(['items'], R.pipe(
                R.path(['items']),
                R.map(modification),
            )(goods), {
                counterpartyId: R.path(['counterparty', 'id'])(goods),
            })
        }

        return createGoods({ variables })
    }, [goods])

    const destroy = React.useCallback(() => {

    }, [goods])

    return [create, destroy]
}

export const AddGoods = ({ navigation }) => {
    const [current, setMoved] = useNavigation({ navigation, move: 'toggle' });
    const { goods } = useSelector(state => state.goods, shallowEqual);
    const [create, destroy] = useAddGoodsRequest()
    const dispatchRedux = useDispatch()

    const title = R.join(' ', [
        R.path(['id'])(goods) ? i18n.t('EDITGARAGESTOCK') : i18n.t('ADDEDGARAGESTOCK'),
        "#",
        R.path(['id'])(goods) ? moment(R.path(['createdAt'])(goods)).format("DD.MM.YYYY") : moment().format("DD.MM.YYYY")
    ])

    const mode = ["inWebView", 'inOfficeView', 'videoView', 'imageView']
    const inVision = R.innerJoin(
        (record, id) => record === id,
        R.defaultTo([])(R.keys(goods)),
        mode
    )

    if (R.pipe(isOdd, R.not)(inVision)) {
        return (
            <Header
                dispatch={() => navigation.goBack()}
                menu={{ title }}
                search
            >
                <InWebView
                    document={R.path([mode])(goods)}
                    hack
                    mode={R.head(inVision)}
                />
            </Header>
        )
    }

    return (
        <Header
            dispatch={setMoved}
            menu={{ title }}
            svg={current}
        >
            <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between' }}>
                {R.path(['id'])(goods) ?
                    <EmployeeComponents
                        actor={R.path(['actor'])(goods)}
                        HEADER="ACTOR"
                    /> : <View />}
                <ContragentSelect />
                <ComponentsGoods />
                <View style={{ marginTop: 10, marginBootom: 20 }}>
                    <App hack />
                </View>
                <View
                    style={{ flex: 1, marginRight: 10, marginLeft: 10, justifyContent: 'flex-end' }}
                >
                    <Text>{isOdd(R.path(['items'])(goods)) ? "ПУСТО ГУСТО ВЫРОСЛА КАПУСТА" : "ВСЕ ПУЧКОМ"}</Text>
                </View>
                <View style={{ margin: 10 }}>
                    <TotalSumGoods />
                </View>
                <View style={{ flexDirection: 'row' }}>
                    {R.addIndex(R.map)(x => (
                        <TouchesState
                            key={x}
                            color={R.equals('CLOSED', x) ? "VeryRed" : "Yellow"}
                            disabled={false}
                            flex={1}
                            onPress={() => {
                                if (x === 'ADDEDNEW') return dispatchRedux({ type: "ADD_GOODS", payload: {} })
                                if (R.equals("SAVE", x)) return create()
                            }}
                            title={R.equals('SAVE', x) ? "SAVE" : x}
                        />
                    ))(R.path(['id'])(goods) ? ['ADDEDNEW'] : ['SAVE', 'CLOSED'])}
                </View>
            </ScrollView>
        </Header>
    )
};