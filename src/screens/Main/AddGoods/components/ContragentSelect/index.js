/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/no-multi-comp */
import Edit from 'assets/svg/edit';
import Find from 'assets/svg/search';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import { SvgXml } from 'react-native-svg';
import { shallowEqual, useSelector } from 'react-redux';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { ModalConterParity } from 'screens/Main/AddGoods/components/ContragentSelect/ModalConterParity';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { isOdd } from 'utils/helper';
import { stylex, useCounterGoods } from './hooks';

export const ContragentSelect = () => {
    const { goods } = useSelector(state => state.goods, shallowEqual);
    const data = R.path(['counterparty'])(goods);

    const [CounterList] = useCounterGoods()
    const [modal, setModal] = React.useState({ isVisible: false })

    const success = (e) => R.cond([
        [R.equals('ModalTrue'), () => setModal({ isVisible: true })],
    ])(e)

    const Components = () => {
        if (isOdd(data)) {
            return (
                <View
                    style={stylex.view}
                >
                    <TouchableOpacity
                        onPress={() => success("ModalTrue")}
                        style={stylex.touch}
                    >
                        <Text
                            style={[
                                styles.container,
                                stylex.text,
                                { flex: 1 }
                            ]}
                        >
                            {i18n.t('SearchCounterparity')}
                        </Text>
                        <View>
                            <SvgXml height="20" width="20" xml={Find} />
                        </View>
                    </TouchableOpacity>
                </View>
            )
        }

        return (
            <CardComponents style={[styles.shadow, { margin: 1 }]}>
                <View
                    style={stylex.card2}
                >
                    <View style={stylex.column}>
                        {CounterList()}
                    </View>
                    <TouchableOpacity
                        disabled={R.path(['id'])(goods)}
                        onPress={() => success("ModalTrue")}
                        style={styles.avatar}
                    >
                        <SvgXml height="20" width="20" xml={Edit} />
                    </TouchableOpacity>
                </View>
            </CardComponents>
        )
    }

    return (
        <View>
            <HeaderApp
                empty
                header
                title={i18n.t("ADDContr")}
            />
            <View style={{ marginRight: 10, marginLeft: 10 }}>
                {Components()}
                <Modal isVisible={R.path(['isVisible'])(modal)}>
                    <ModalConterParity
                        dispatch={(e) => {
                            setModal(R.assocPath(['isVisible'], e, {}))
                        }}
                    />
                </Modal>
            </View>
        </View>
    )
};
