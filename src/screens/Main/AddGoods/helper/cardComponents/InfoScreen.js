/* eslint-disable react/no-multi-comp */
/* eslint-disable multiline-ternary */
/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import Rub from 'assets/svg/rub';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, priceModed } from 'utils/helper';

export const InfoScreen = ({ updata, state }) => {
    if (R.equals(true, updata)) {
        return (
            <CardComponents style={[styles.shadow, { marginTop: 5, marginBottom: 5 }]}>
                <RowComponents>
                    <View style={styles.rowCenter}>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SDescription"
                            style={[styles.container, { textAlign: 'left' }]}
                        >
                            {(R.path(['amount', 'available'])(state))}
                        </LabelText>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SDescription"
                            style={[styles.container, {
                                textAlign: 'left',
                                textTransform: 'lowercase'
                            }]}
                        >
                            {(R.path(['measure', 'abbr'])(state))}
                        </LabelText>
                    </View>
                    <View
                        style={{
                            justifyContent: 'flex-end'
                        }}
                    >
                        {R.map(x => (
                            <View
                                key={x}
                                style={[styles.rowCenter, {
                                    justifyContent: 'flex-end'
                                }]}
                            >
                                <LabelText
                                    color={ColorCard('classic').font}
                                    fonts="ShellMedium"
                                    items="SDescription"
                                    style={[styles.container, { textAlign: 'right' }]}
                                >
                                    {priceModed(R.path(['priceOut', 'amount'])(x))}
                                </LabelText>
                                <SvgXml
                                    height="10"
                                    style={[styles.items, {
                                        paddingLeft: 10,
                                        justifyContent: 'center'
                                    }]}
                                    weight="10"
                                    xml={Rub}
                                />
                            </View>
                        ))(R.path(['depotItems'])(state))}
                    </View>
                </RowComponents>
            </CardComponents>
        )
    }

    return (
        <View />
    )
};
