/* eslint-disable no-unused-expressions */
/* eslint-disable react/no-multi-comp */
/* eslint-disable multiline-ternary */
/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { uiColor } from 'styled/colors/uiColor.json';
import { Describe } from './Describe';
import { InfoScreen } from './InfoScreen';
import { Header } from './Header';

export const CardScrollComponents = ({
    state, date, data, info, updata, dispatch
}) => {
    const [magic, setState] = React.useState(null);

    return (
        <CardComponents
            style={[styles.shadow, {
                flexDirection: 'column',
                backgroundColor: R.equals(R.path(['stock'])(data), state) ? uiColor.Yellow : uiColor.Very_Pole_Grey,
                margin: 2
            }]}
        >
            <TouchableOpacity
                onPress={() => {
                    if (R.equals(true, info)) {
                        setState(
                            R.assocPath(['click'], !R.path(['click'])(magic), magic))
                    }
                    dispatch({ type: "COUNT", payload: state })
                }}
            >
                <Header
                    state={state}
                />
                <InfoScreen
                    state={state}
                    updata={updata}
                />
                <TouchableOpacity onPress={() => dispatch({ type: 'CREATE', payload: state })}>
                    <Describe
                        data={date}
                        magic={magic}
                        style={{ marginTop: 5, marginBottom: 5 }}
                    />
                </TouchableOpacity>
            </TouchableOpacity>
        </CardComponents>
    )
};