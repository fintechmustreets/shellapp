/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-props-no-spreading */
import { useLazyQuery } from '@apollo/react-hooks';
import { RESERVED_DEPOTS } from 'gql/garage';
import * as R from 'ramda';
import React from 'react';
import { View, ScrollView } from 'react-native';
import { reducerSelect } from 'screens/Main/AddGoods/helper/components/reducer';
import { InputComponents } from 'utils/component';
import { CardScrollComponents } from 'screens/Main/AddGoods/helper/cardComponents';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import i18n from 'localization'

export const Search = ({ dispatch, setScreen }) => {
    const [state, setReducer] = React.useReducer(reducerSelect, { query: [] });
    const ref = React.useRef()
    const [loadgreeting] = useLazyQuery(RESERVED_DEPOTS, {
        fetchPolicy: "network-only",
        onCompleted: (data) => {
            setReducer({ type: "Query", payload: R.path(['stocks', 'items'])(data) })
        }
    });

    React.useEffect(() => {
        if (!R.equals(ref.current, true)) {
            loadgreeting({ variables: { where: {} } })
            ref.current = true
        }
    }, [state])

    const changeText = (change) => {
        setReducer({
            type: R.path(['path'])(initialInputState),
            payload: R.assocPath(
                ['stock'], R.mergeAll([
                    R.path(['data', 'stock'])(state),
                    R.zipObj(["name"], [change])
                ]), {})
        });
        loadgreeting({ variables: { where: { q: change } } })
    };

    return (
        <View style={{ flex: 1, justifyContent: 'space-between' }}>
            <HeaderApp
                button="CLOSED"
                dispatch={() => dispatch({ type: "Modal", payload: false })}
                empty={false}
                header
                title={i18n.t("SearchByAgentGarage")}
            />
            <ScrollView contentContainerStyle={{ flexGrow: 1, marginLeft: 10, marginRight: 10 }}>
                <InputComponents
                    {...initialInputState}
                    onChangeText={(change) => changeText(change)}
                    storage={state}
                />
                {R.addIndex(R.map)((element, key) => (
                    <CardScrollComponents
                        key={key}
                        data={{ stock: state }}
                        dispatch={() => {
                            dispatch({ type: 'UpdateStock', payload: element })

                            return dispatch({ type: "Modal", payload: false })
                        }}
                        state={R.pipe(R.omit(['__typename']))(element)}
                    />
                ))(state.query)}
            </ScrollView>
            <TouchesState
                color="Yellow"
                flex={0}
                onPress={() => setScreen(true)}
                title="ADDEDACTION"
            />
        </View>
    )
};

const initialInputState = {
    id: "text",
    path: 'Text',
    main: true,

    keyboardType: "default",
    label: 'NameAction',
    title: "Error message",
    value: ['text', 'stock', 'name'],
    string: true,

    maxLength: 120,
    testID: "NameInput",
};
