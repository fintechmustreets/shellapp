/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { InputComponents } from 'utils/component';

export const InputInitial = ({ state, setState }) => (
    <InputComponents
        {...initialInputState}
        onChangeText={(change) => setState({
            type: R.path(['path'])(initialInputState),
            payload: change
        })}
        storage={state}
    />
);

export const InputName = ({ state, setState }) => R.addIndex(R.map)((element, keys) => (
    <InputComponents
        key={keys}
        {...element}
        onChangeText={(change) => setState({
            type: R.path(['path'])(element),
            payload: change
        })}
        storage={state}
    />
))(R.without([R.last(initialStateState)], initialStateState));

export const InputDescription = ({ state, setState }) => R.addIndex(R.map)((element, keys) => (
    <InputComponents
        key={keys}
        {...element}
        onChangeText={(change) => setState({
            type: R.path(['path'])(element),
            payload: change
        })}
        storage={state}
    />
))([R.last(initialStateState)]);

export const initialStateState = [{
    id: "ИМЯ",
    path: 'article',
    main: true,

    keyboardType: "default",
    label: 'article',
    title: "Error message",
    value: ['article'],

    maxLength: 120,
    testID: "NameInput",
}, {
    id: "ИМЯ",
    path: 'oem',
    main: true,

    keyboardType: "default",
    label: 'oem',
    title: "Error message",
    value: ['oem'],

    maxLength: 120,
    testID: "NameInput",
}, {
    id: "ИМЯ",
    path: 'brand',
    main: true,

    keyboardType: "default",
    label: 'brand',
    title: "Error message",
    value: ['brand'],

    maxLength: 120,
    testID: "NameInput",
}, {
    id: "ИМЯ",
    path: 'description',
    keyboardType: "default",
    main: true,
    label: 'shortByAgent',
    title: "Error message",
    value: ['description'],

    maxLength: 120,
    testID: "NameInput",
}];

const initialInputState = {
    id: "text",
    path: 'name',
    main: true,

    keyboardType: "default",
    label: 'NameAction',
    title: "Error message",
    value: ['name'],
    string: true,

    maxLength: 120,
    testID: "NameInput",
};
