/* eslint-disable max-statements */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import { useGarageFace } from 'hooks/useGarageFace';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import {
    Text,
    TouchableOpacity, View
} from 'react-native';
import { shallowEqual, useSelector } from 'react-redux';
import { ModalUpdate } from 'screens/Main/AddGoods/helper/components/GoodHomeScreen/ModalUpdate';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { isOdd } from 'utils/helper';
import { CurrentComponent } from './CurrentComponent';
import { stylex } from './stylex';

export const GoodHomeScreen = () => {
    const [click, setClick] = React.useState(false)
    const { goods } = useSelector(state => state.goods, shallowEqual);
    const [modal, setSetModal] = React.useState({ isVisible: false })
    const [headerGroup, totalCard, summary, update] = useGarageFace()

    if (isOdd(R.path(['items'])(goods))) {
        return (
            <CardComponents
                style={[styles.shadow, stylex.card]}
            >
                <Text
                    style={[styles.container, stylex.text]}
                >
                    {i18n.t('falsePay')}
                </Text>
            </CardComponents>
        )
    }

    return (
        <View>
            {R.addIndex(R.map)((x, key) => (
                <View
                    key={key}
                    style={{ flex: 1 }}
                >
                    <CardComponents
                        style={[styles.shadow, {
                            margin: 1,
                        }]}
                    >
                        <TouchableOpacity
                            onPress={() => setClick(R.equals(click, x) ? false : x)}
                        >
                            <View
                                style={[stylex.view, { justifyContent: 'space-between' }]}
                            >
                                <View
                                    style={[stylex.start, { flex: 1, justifyContent: 'flex-start' }]}
                                >
                                    {headerGroup(x)}
                                </View>
                                {summary(x)}
                            </View>
                            <View style={stylex.footer}>
                                {totalCard(x)}
                            </View>
                        </TouchableOpacity>
                    </CardComponents>
                    {R.equals(x, click) ?
                        <CurrentComponent
                            click={click}
                            setSetModal={(e) => {
                                if (R.path(['id'])(goods)) {
                                    return null
                                }

                                return setSetModal(e)
                            }}
                            state={x}
                        /> :
                        <View />}
                </View>
            ))(goods.items)}
            <ModalUpdate
                modal={modal}
                setSetModal={setSetModal}
            />
        </View>
    )
}
