/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import Modal from 'react-native-modal';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { Update } from 'screens/Main/AddGoods/helper/components/goodsScreen/Update';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';

export const ModalUpdate = ({
    modal, setSetModal
}) => {
    const [state, setState] = React.useState(R.path(['params'])(modal))
    const dispatchRedux = useDispatch()
    const { goods } = useSelector(state => state.goods, shallowEqual);

    const successModal = (e) => R.cond([
        [R.equals('Goods'), () => {
            dispatchRedux({
                type: 'ADD_GOODS',
                payload: R.mergeAll([goods, {
                    items: R.pipe(
                        R.path(['items']),
                        R.defaultTo({}),
                        R.without([R.path(['params'])(modal)]),
                        R.append(R.path(['payload'])(e))
                    )(goods)
                }])
            })
        }],
        [R.equals('UpdateStock'), () => {
            setState(R.mergeAll([state, R.path(['payload'])(e)]))
        }],
        [R.equals('Modal'), () => setSetModal({ isVisible: false })]
    ])(R.path(['type'])(e))

    return (
        <Modal isVisible={R.path(['isVisible'])(modal)}>
            <ModalDescription style={{ flex: 1, margin: 10 }}>
                <HeaderApp
                    button="CLOSED"
                    dispatch={() => null}
                    empty
                    header
                    title={i18n.t('ADDEDGARAGEPOSITION')}
                />
                <Update
                    data={R.path(['params'])(modal)}
                    dispatch={successModal}
                />
            </ModalDescription>
        </Modal>
    )
}