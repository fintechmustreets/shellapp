import * as R from 'ramda'
import { AsyncStorage } from 'react-native';

export const profilePersistConfig = {
    key: 'profile',
    storage: AsyncStorage,
    blacklist: ['someEphemeralKey'],
};

export const initialState = {
    lastEnter: Date.now(),
    verifyAccount: false,
    TokenCreate: false,
    phone: null,
    jswToken: null,
    name: null,
    language: 'ru'
};

export const profileReducer = (state = initialState, action) => R.cond([
    [R.equals('PROFILE_MERGE'), R.always(R.mergeRight(state || {}, action.payload))],
    [R.equals('PROFILE_LOGOUT'), R.always(R.mergeRight(state, {
        TokenCreate: false,
        jswToken: null,
        verifyAccount: false,
    }))],
    [R.equals('PROFILE_SETNAMEDATA'), R.mergeAll(state, {
        name: action.payload
    })],
    [R.equals('PROFILE_SETWORKER'), R.mergeAll(state, {
        worker: action.payload
    })],
    [R.equals('PROFILE_PHONE'), R.mergeAll(state, {
        phone: action.payload
    })],
    [R.equals('PROFILE_PHONE'), R.mergeAll(state, {
        phone: action.payload
    })],
    [R.equals('PROFILE_LOGIN'), R.always(R.mergeRight(state, {
        jswToken: action.payload,
        TokenCreate: true,
        verifyAccount: true,
    }))],
    [R.equals('PROFILE_CLEAR'), R.always(initialState)],
    [R.equals('LANG'), R.always(R.assocPath(['language'], action.payload, state))],
    [R.T, R.always(state)],
])(action.type);