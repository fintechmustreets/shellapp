/* eslint-disable react/jsx-indent-props */
import { useProfile } from 'hooks/useProfile';
import React from 'react';
import { ErrorScreen } from 'screens/Drawler/components/ErrorScreen';
import { FlatModuleList } from 'screens/Drawler/components/FlatModuleList';

export const AppScreen = (props) => {
    const { navigation, route } = props;
    const [error, setError] = React.useState(null);
    const [loader, update] = useProfile()
    // const [depots, current, createDepots] = useSkladLoading({ navigation })

    // React.useEffect(() => {
    //     current()
    // }, [depots])

    React.useEffect(() => {
        if (!loader) {
            update()
        }
    }, [loader])

    if (error) {
        return (
            <ErrorScreen navigation={navigation} />
        )
    }

    return (
        <FlatModuleList
            navigation={navigation}
            route={route}
        />
    )
};

export default AppScreen