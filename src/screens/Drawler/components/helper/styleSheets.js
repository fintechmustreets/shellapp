import { StyleSheet } from 'react-native'
import { uiColor } from 'styled/colors/uiColor.json';


export const styles = StyleSheet.create(
    {
        container: {
            textAlign: 'left',
            padding: 10,
            textTransform: 'uppercase'
        },
        items: {
            textAlign: 'left',
            paddingLeft: 20,
            padding: 10,
        },
        create: {
            textAlign: 'left',
            paddingLeft: 20,
            padding: 10,
            textTransform: 'capitalize'
        },
        line: {
            marginLeft: 15,
            marginRight: 15,
            height: 1,
            backgroundColor: 'grey'
        },
        rowCenter: {
            flexDirection: 'row',
            alignItems: 'center',
            flex: 1
        },
        center: {
            padding: 10,
            justifyContent: 'center',
            alignItems: 'center'
        },
        avatar: {
            borderRadius: 50,
            width: 70,
            height: 70,
            alignItems: 'center',
            alignSelf: 'center',
            justifyContent: 'center',
            backgroundColor: uiColor.Polo_Grey,
        }
    }
);