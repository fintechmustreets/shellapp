/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-max-depth */
import famous from 'assets/svg/famous';
import { SvgXml } from 'react-native-svg';
import React from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { LogOut } from 'screens/Drawler/components/history/LogOut';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const ErrorScreen = ({ navigation }) => (
    <ScrollView
        contentContainerStyle={styleError.scroll}
        style={{ flex: 1 }}
    >
        <SvgXml
            height="40"
            width="180"
            xml={famous}
        />
        <View style={{ flex: 1 }} />
        <View
            style={[styleError.center]}
        >
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellBold"
                items="SSDescription"
                style={[styles.items, { textAlign: 'center', paddingLeft: 0, padding: 3 }]}
                testID="BlockLaker"
            >
                Проверьте интернет соединение
            </LabelText>
        </View>
        <View style={{ flex: 1 }} />
        <LogOut
            navigation={navigation}
        />
    </ScrollView >
);

const styleError = StyleSheet.create({
    logOut: {
        flexDirection: "row",
        alignItems: 'flex-end',
        flex: 1
    },
    text: {
        alignItems: 'center',
        paddingLeft: 0,
        padding: 3
    },
    center: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        flex: 1
    },
    scroll: {
        flex: 1,
        justifyContent: 'space-between',
        padding: 10
    }
});