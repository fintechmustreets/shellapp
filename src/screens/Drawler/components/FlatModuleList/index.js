/* eslint-disable react/no-multi-comp */
/* eslint-disable react/self-closing-comp */
/* eslint-disable react/jsx-indent-props */
import famous from 'assets/svg/famous';
import HELP from 'assets/svg/help';
import SETTINGS from 'assets/svg/settings';
import SKLAD from 'assets/svg/sklad';
import CLIENT from 'assets/svg/user';
import PROFILE from 'assets/svg/userPro';
import REMONT from 'assets/svg/wrench';
import * as R from 'ramda';
import React, { useState } from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { Colors } from 'react-native-paper';
import { SvgXml } from 'react-native-svg';
import { ClientsApp } from 'screens/Drawler/components/clients';
import { IconState } from 'screens/Drawler/components/FlatModuleList/IconState';
import { HelpApp } from 'screens/Drawler/components/help';
import { ProfileApp } from 'screens/Drawler/components/history';
import { RemontApp } from 'screens/Drawler/components/remont';
import { SettingsApp } from 'screens/Drawler/components/settings';
import { SkladApp } from 'screens/Drawler/components/sklad';
import { SplashScreen } from 'screens/Splash';

const router = [{
    svg: REMONT,
    title: "REMONT",
}, {
    svg: CLIENT,
    title: "CLIENTS",
}, {
    svg: SKLAD,
    title: "SKLAD",
}, {
    svg: SETTINGS,
    title: "SETTINGS",
}, {
    svg: PROFILE,
    title: "PROFILE",
}, {
    svg: HELP,
    title: "HELP",
}];

export const FlatModuleList = ({ navigation }) => {
    const [state, setState] = useState({
        title: 'REMONT',
        comp: <RemontApp navigation={navigation} />
    });

    // if (!employee) return (
    //     <SplashScreen
    //         state="Loading"
    //     />
    // );

    const ScreenUpdate = () => (
        <View
            style={stylex.screen}
        >
            <View
                style={stylex.view}
            >
                {R.addIndex(R.map)((elem, key) => (
                    <IconState
                        key={key}
                        elem={elem}
                        setState={setState}
                        state={state}
                    />
                ))(router)}
            </View>
            {R.cond([
                [R.equals('REMONT'), () => <RemontApp navigation={navigation} />],
                [R.equals('CLIENTS'), () => <ClientsApp navigation={navigation} />],
                [R.equals('SKLAD'), () => <SkladApp navigation={navigation} reload />],
                [R.equals('SETTINGS'), () => <SettingsApp navigation={navigation} />],
                [R.equals('PROFILE'), () => <ProfileApp navigation={navigation} />],
                [R.equals('HELP'), () => <HelpApp navigation={navigation} />],
                [R.T, () => <SplashScreen state="Loading" />],
                [R.F, () => <SplashScreen state="Error" />],
            ])(R.path(['title'])(state))}
        </View>
    )

    return (
        <View
            style={{ flex: 1 }}
        >
            <View
                style={stylex.card}
            >
                <SvgXml
                    height="40"
                    width="180"
                    xml={famous}
                />
            </View>
            <ScrollView
                contentContainerStyle={stylex.scrool}
            >
                {ScreenUpdate()}
            </ScrollView>
        </View>
    )
};

const stylex = StyleSheet.create({
    card: {
        flex: 1,
        maxHeight: 60,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    scrool: {
        justifyContent: 'space-between',
        flexGrow: 1
    },
    view: {
        flexDirection: "column",
        flex: 0,
        backgroundColor: Colors.grey300
    },
    screen: {
        flexDirection: "row",
        flex: 1,
    }
})
