/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import {
    View, TouchableOpacity, Text, StyleSheet
} from 'react-native';
import { SvgXml } from 'react-native-svg';
import { uiColor } from 'styled/colors/uiColor.json';
import { Colors } from 'react-native-paper';

export const IconState = ({ elem, state, setState }) => (
    <View>
        <TouchableOpacity
            onPress={() => setState({
                comp: elem.comp,
                title: elem.title
            })}
            style={[stylex.touch, {
                backgroundColor: R.equals(elem.title, state.title) ? uiColor.Yellow : Colors.grey300
            }]}
        >
            <View
                style={stylex.view}
            >
                <SvgXml height="30" width="30" xml={elem.svg} />
                <Text
                    style={stylex.text}
                >
                    {i18n.t(elem.title)}
                </Text>
            </View>
        </TouchableOpacity>
    </View>
);

const stylex = StyleSheet.create({
    text: {
        paddingTop: 10,
        textTransform: 'uppercase',
        fontFamily: 'ShellMedium',
        fontSize: 10
    },
    view: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    touch: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 90,
        height: 90,
    }
})