/* eslint-disable react/no-multi-comp */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import moment from 'moment';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { ProgressBar } from 'react-native-paper';
import { styles } from 'screens/Drawler/components/helper/styleSheets';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { useEmployeeProfile } from 'hooks/useProfile';

export const TimeLeft = () => {
    const [state, update] = useEmployeeProfile()
    const time = moment(moment().unix()) * 1000;
    const endTime = time + (R.path(['balance'])(state) * 1000);
    const x = moment(endTime).diff(moment(), 'seconds') * 1000;
    const tempTime = moment.duration(x);
    const ref = React.useRef()
    ref.current = state

    React.useEffect(() => {
        if (!R.path(['__typename'])(state)) {
            update()
        }
    }, [state])

    return (
        <>
            <View
                style={{
                    marginTop: 15, marginBottom: 15, padding: 10, flex: 1
                }}
            >
                <ProgressBar
                    color="#66bb6a"
                    progress={state.balance / state.totalBalance}
                    style={{ backgroundColor: 'grey' }}
                />
            </View>
            <View
                style={{ padding: 10, flexDirection: 'column' }}
            >
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SDescription"
                    style={[styles.items, { paddingLeft: 0, padding: 0 }]}
                >
                    {i18n.t('EndedPay')}
                </LabelText>
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="SDescription"
                    style={[styles.items, { paddingLeft: 0, padding: 0 }]}
                >
                    {moment(endTime).format("DD.MM.YYYY HH:mm")}
                </LabelText>
            </View>
            <View
                style={{ padding: 10, flexDirection: 'column' }}
            >
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SDescription"
                    style={[styles.items, { paddingLeft: 0, padding: 0 }]}
                >
                    {i18n.t('DiffPay')}
                </LabelText>
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="SDescription"
                    style={[styles.items, { textTransform: 'lowercase', padding: 0, paddingLeft: 0 }]}
                >
                    {R.join(' ', [R.head(R.split('.', R.toString(tempTime.asDays()))), 'д. ', tempTime.hours(), 'ч. ', tempTime.minutes(), 'мин. '])}
                </LabelText>
            </View>
        </>
    )
};
