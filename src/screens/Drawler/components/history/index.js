/* eslint-disable react/no-multi-comp */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import ADD from 'assets/svg/card';
import i18n from 'localization';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { styles } from 'screens/Drawler/components/helper/styleSheets';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { MainScreen } from 'utils/routing/index.json';
import { LogOut } from './LogOut';
import { PayAgent } from './PayAgent';
import { TimeLeft } from './TimeLeft';

export const ProfileApp = ({ navigation }) => (
    <View style={{ flexDirection: 'column', flex: 1 }}>
        <View>
            <LabelText
                color={ColorCard('error').font}
                fonts="ShellBold"
                items="SSDescription"
                style={styles.container}
            >
                {i18n.t('ConnectedSystem')}
            </LabelText>
            <TimeLeft />
            <View
                style={styles.line}
            />
            <View
                style={styles.container}
            >
                <TouchableOpacity
                    onPress={() => navigation.navigate(MainScreen.Pay)}
                    style={styles.rowCenter}
                >
                    <SvgXml
                        height="15"
                        width="15"
                        xml={ADD}
                    />
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SSDescription"
                        style={[styles.items, { paddingLeft: 10 }]}
                    >
                        {i18n.t('Pay')}
                    </LabelText>
                </TouchableOpacity>
            </View>
        </View>
        <PayAgent />
        <View
            style={styles.line}
        />
        <LogOut
            navigation={navigation}
        />
    </View >
)