/* eslint-disable react/no-multi-comp */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import logout from 'assets/svg/logout';
import i18n from 'localization';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { useDispatch } from 'react-redux';
import { styles } from 'screens/Drawler/components/helper/styleSheets';
import { LabelText } from 'uiComponents/SplashComponents';
import { removeItem } from 'utils/async';
import { ColorCard } from 'utils/helper';
import { HeaderScreen } from 'utils/routing/index.json';

export const LogOut = ({ navigation }) => {
    const dispatch = useDispatch();

    return (
        <View
            style={styles.center}
        >
            <View />
            <TouchableOpacity
                onPress={async () => {
                    await removeItem();
                    dispatch({ type: "PROFILE_LOGOUT" });

                    return navigation.navigate(HeaderScreen.Phone)
                }}
                style={styles.rowCenter}
            >
                <SvgXml
                    height="15"
                    width="15"
                    xml={logout}
                />
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="SSDescription"
                    style={[styles.items, { paddingLeft: 10 }]}
                    testID="BlockLaker"
                >
                    {i18n.t('LOGOUT')}
                </LabelText>
            </TouchableOpacity>
        </View>
    )
};