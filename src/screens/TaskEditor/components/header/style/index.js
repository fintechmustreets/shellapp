/* eslint-disable no-confusing-arrow */
import styled from 'styled-components'

export const BottomSelect = styled.TouchableOpacity`
bottom: 0;
width:100%;
`;

export const BottomLabel = styled.Text`
 font-weight: ${props => props.current ? 600 : 400};
text-align:center;
padding:5px;
min-width:60px;
font-family:"ShellMedium";
border-bottom-color:${props => props.current ? 'red' : 'transparent'};
border-bottom-width:${props => props.current ? '2px' : '0px'};
`;

export const TouchBtn = styled.TouchableOpacity`
width:50px;
height:100%;
border-width:0.3px;
border-color:${props => props.theme.color.uiColor.Mid_Grey};
background-color:${props => props.current ? props.theme.color.uiColor.Yellow : "transparent"};
`;

export const TouchesStyle = styled.TouchableOpacity`
align-items: center;
justify-content: center;
width: ${(props) => props.current ? 50 : '100%'};
height: ${(props) => props.current ? 50 : '100%'};
`;

export const Block = styled.View`
height: ${props => props.small ? "15px" : "55px"};
width: 100%;
flex-direction: row;
justify-content: space-around;
align-items: ${(props) => props.current ? "center" : "center"};
background-color: ${props => props.theme.color.uiColor.Very_Pole_Grey};
`;