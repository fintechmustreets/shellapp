/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import React from 'react';
import { View } from 'react-native';
import i18n from 'localization';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';
import { VisualTable } from 'screens/TaskEditor/components/table/component'

export const TableModel = ({
    data, dispatch, mode, unfilter
}) => {
    if (isOdd(data)) {
        return (
            <View
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    flex: 1
                }}
            >
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SSDescription"
                    style={{ textAlign: 'center', textTransform: "uppercase" }}
                >
                    {i18n.t("not_found")}
                </LabelText>
            </View>
        )
    }

    return (
        <VisualTable
            data={data}
            dispatch={dispatch}
            mode={mode}
            unfilter={unfilter}
        />
    )
};