/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import Rub from 'assets/svg/rub';
import me from 'assets/svg/userPro';
import moment from 'moment';
import * as R from 'ramda';
import React from 'react';
import {
    Linking, ScrollView, StyleSheet, TouchableOpacity, View
} from 'react-native';
import { SvgXml } from 'react-native-svg';
import { styles } from 'screens/Main/Pay/helper/index';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';
import i18n from 'localization'

export const checkStatus = (data) => {
    const maybe = R.innerJoin(
        (record) => R.path(['status'])(record),
        R.defaultTo([{ status: null }])(R.path(['billPayments'])(data)),
        ['succeeded']
    );

    return R.pipe(R.head, R.path(['status']))(maybe)
};

export const BackFlip = ({ data, status }) => {
    const dasdasd = R.cond([
        [R.equals('pending'), R.always({ label: i18n.t('pendingPay') })],
        [R.equals('succeeded'), R.always({ label: i18n.t('succeededPay') })],
        [R.equals('waiting'), R.always({ label: i18n.t('waitingPay') })],
        [R.equals('canceled'), R.always({ label: i18n.t('canceledPay') })],
        [R.T, R.always({ label: i18n.t('falsePay') })],
        [R.F, R.always({ label: i18n.t('falsePay') })],
    ])(status);

    return (
        <View style={{ flex: 1, justifyContent: 'space-between' }}>
            <RowComponents
                style={[
                    isOdd(status) ? {
                        borderWidth: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: 100,
                        marginTop: 25
                    } : { flex: 1 }
                ]}
            >
                {isOdd(status) ? <View /> : <SvgXml height="30" weight="30" xml={me} />}
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="SRDescription"
                    style={[styles.items, styleCard.item, { flex: 1 },
                    isOdd(status) ? { justifyContent: 'center', textAlign: 'center' } : {}]}
                >
                    {R.path(['label'])(dasdasd)}
                </LabelText>
            </RowComponents>
            {R.includes(checkStatus(data))([
                'pending',
                'succeeded',
                'waiting',
                'canceled'
            ]) ? <View style={[styles.line, { marginBottom: 10 }]} /> : <View />}
            <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center', paddingRight: 5 }}>
                {R.map(x => (
                    <TouchableOpacity
                        key={R.path(['id'])(x)}
                        onPress={() => Linking.openURL(R.path(['confUrl'])(x))}
                        style={{ paddingTop: 5, paddingBottom: 5 }}
                    >
                        <RowComponents>
                            <LabelText
                                color={ColorCard('classic').font}
                                fonts="ShellMedium"
                                items="SDescription"
                                style={[styles.items, styleCard.item,]}
                            >
                                {moment(R.path(['updatedAt'])(x)).format("YYYY-MM-DD HH-mm")}
                            </LabelText>
                            <RowComponents>
                                <LabelText
                                    color={ColorCard('classic').font}
                                    fonts="ShellMedium"
                                    items="SDescription"
                                    style={[styles.items, styleCard.item]}
                                >
                                    {R.path(['amount', 'amount'])(x)}
                                </LabelText>
                                <SvgXml height="10" weight="10" xml={Rub} />
                            </RowComponents>
                        </RowComponents>
                    </TouchableOpacity>
                ))(R.defaultTo([])(R.path(['billPayments'])(data)))}
            </ScrollView>
            {R.includes(checkStatus(data))([
                'pending',
                'succeeded',
                'waiting',
                'canceled'
            ]) ? <View style={[styles.line, { marginTop: 10 }]} /> : <View />}
        </View>
    )
};

const styleCard = StyleSheet.create({
    item: {
        padding: 10,
        paddingBottom: 0,
        textTransform: 'uppercase',
        paddingTop: 0,
    },
    text: {
        borderLeftWidth: 4,
        flex: 1,
        marginTop: 10,
        marginBottom: 10,
    }
});
