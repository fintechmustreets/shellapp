/* eslint-disable no-extra-parens */
/* eslint-disable id-length */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable no-unsanitized/method */
/* eslint-disable react/jsx-indent-props */
import famous from 'assets/svg/userPro';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { uiColor } from 'styled/colors/uiColor.json';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const EmplloyeeTaskItem = ({ data, dispatch, state }) => (
    <CardComponents
        style={[styles.shadow, styles.content, {
            minHeight: 50,
            flex: 1,
            backgroundColor: R.equals(R.path(['id'])(data), state) ? uiColor.Yellow : uiColor.Very_Pole_Grey
        }]}
    >
        <TouchableOpacity
            onPress={() => dispatch(data)}
            style={[styles.content, { flex: 1, }]}
        >
            <RowComponents
                style={[styles.items]}
            >
                <View
                    style={styles.container}
                >
                    <SvgXml
                        height="20"
                        width="20"
                        xml={famous}
                    />
                </View>
                <View style={{ flex: 1 }} >
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SRDescription"
                        style={{ textAlign: 'right' }}
                    >
                        {R.join(' ', R.paths([['name', 'last'], ['name', 'first']])(data))}
                    </LabelText>
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellBold"
                        items="SDescription"
                        style={{ textAlign: 'right' }}
                    >
                        {R.paths([['phone']])(data)}
                    </LabelText>
                </View>
            </RowComponents>
        </TouchableOpacity>
    </CardComponents >
)

EmplloyeeTaskItem.propTypes = {
    data: PropTypes.object,
    dispatch: PropTypes.func,
    state: PropTypes.string,
};

EmplloyeeTaskItem.defaultProps = {
    data: {
        blocked: false,
        id: 151,
        name: { first: "Slalva", last: 'Slava', middle: 'Slava' },
        phone: '+7999999999'
    },
    dispatch: () => null,
    state: 151
};

export const styles = StyleSheet.create({
    container: {
        width: 30,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    items: {
        paddingTop: 2,
        paddingBottom: 2,
        flex: 1
    },
    line: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        marginBottom: 5,
        borderStyle: 'dashed',
        borderRadius: 15,
        borderWidth: 1,
    },
    content: {
        flex: 1,
        maxHeight: 75,
        justifyContent: 'center'
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        flex: 1,
        margin: 5,
        marginTop: 1,
        marginBottom: 1
    },
});