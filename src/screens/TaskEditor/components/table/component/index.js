/* eslint-disable object-curly-newline */
/* eslint-disable max-statements */
/* eslint-disable no-confusing-arrow */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable brace-style */
/* eslint-disable max-len */
/* eslint-disable newline-after-var */
/* eslint-disable scanjs-rules/call_setTimeout */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-max-depth */
import { useListRequest } from 'hooks/useList/useListRequest';
import { useVariables } from 'hooks/useList/useVariables';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { FlatList, View } from 'react-native';
import { HeaderInputPay } from 'screens/Main/Pay/Query';
import { SplashScreen } from 'screens/Splash';
import { SearchInputLine } from 'screens/TaskEditor/components/header/component/SearchInputLine';
import { Tag } from 'screens/TaskEditor/components/table/component/CurrentScreen/Tag';
import { Item } from 'screens/TaskEditor/components/table/component/Item';
import { Navigator } from 'screens/TaskEditor/components/table/component/Navigator';
import { isOdd } from 'utils/helper';

export const VisualTable = ({
    mode,
    dispatch,
    route,
    button,
    navigation,
    unsearch,
    unfilter,
    list,
    id,
    current
}) => {
    const [forceVar, a, b] = useVariables({ mode, id, route })
    const [text, setText] = React.useState(null)

    const SearchScreen = () => {
        if (R.includes(mode)(['pay'])) {
            return (
                <View>
                    <HeaderInputPay
                        navigation={navigation}
                        update={() => {
                            const variables = forceVar({ mode, id, route })
                            setText(variables)
                        }}
                    />
                </View>
            )
        }
        if (R.equals('taskEmployee', list)) return (<View />)
        if (R.all(R.equals(true))([R.equals(true, unsearch), isOdd(button), R.equals('stock')(mode)])) return (<View />);

        return (
            <View style={{ margin: 5, }}>
                <SearchInputLine
                    dispatch={(e) => {
                        if (R.equals(R.path(['type'])(e), 'Create')) return dispatch(e);
                        if (R.equals(R.path(['type'])(e), 'Filter')) dispatch(e);
                        setText(e)
                    }}
                    mode={button}
                    state={text}
                    unfilter={unfilter}
                    unsearch={unsearch}
                />
                <Tag />
            </View>
        )
    }

    const variables = () => forceVar({
        mode, id, route, text
    })

    return (
        <View style={{ flex: 1 }} >
            {SearchScreen()}
            <FlatScrollTaskView
                current={current}
                dispatch={dispatch}
                id={id}
                list={list}
                mode={mode}
                route={route}
                variables={variables()}
            />
        </View>
    )
};

const FlatScrollTaskView = ({
    mode,
    dispatch,
    route,
    list,
    id,
    variables,
    current
}) => {
    const [forceVar, a, b] = useVariables({ mode, id, route })
    const [state, request] = useListRequest()
    const ref = React.useRef()
    const [currentItems, setCurrent] = React.useState(current);

    React.useEffect(() => {
        if (!R.equals(ref.current, { mode, route, id, variables })) {
            request({ variables, mode, route })
            ref.current = { mode, route, id, variables }
        }
    }, [mode, route, id, variables]);

    const renderItem = React.useCallback(({ item }) => (
        <Item
            current={currentItems}
            data={item}
            dispatch={(e) => {
                setCurrent(R.path(['id'])(e));
                dispatch(e)
            }}
            list={list}
        />
    ), [R.path(['query', 'items'])(state)])

    const Navigation = React.useCallback(() => {
        const paginate = (e) => {
            const variables = forceVar({
                mode, id, route, page: R.path(['payload'])(e)
            })

            return request({ variables, mode, route })
        }

        return (
            <View style={{ flex: 1 }}>
                <Navigator
                    setDispatch={paginate}
                    storage={state}
                />
            </View>
        )
    }, [R.path(['query', 'items'])(state)])

    if (R.pipe(R.path(['query']), R.isNil)(state)) {
        return (
            <View style={{ flex: 1 }}>
                <SplashScreen
                    msg="Загрузка"
                    state="Error"
                    style={{ margin: 4, flex: 1 }}
                />
            </View >
        )
    }
    if (R.pipe(R.path(['query', 'items']), isOdd)(state)) {
        return (
            <View style={{ flex: 1 }}>
                <SplashScreen
                    msg={i18n.t('falsePay')}
                    state="Error"
                    style={{ margin: 4, flex: 1 }}
                />
            </View >
        )
    }
    if (R.pipe(R.path(['query', 'items']), isOdd)(state)) {
        return (
            <View style={{ flex: 1 }}>
                <SplashScreen
                    msg={R.pipe(R.path(['query']), R.isNil)(state) ? i18n.t('falsePay') : "Загрузка"}
                    state="Error"
                    style={{ margin: 4, flex: 1 }}
                />
            </View>
        )
    }

    return (
        <FlatList
            data={R.path(['query', 'items'])(state)}
            extraData={R.path(['query', 'items'])(state)}
            initialNumToRender={10}
            keyExtractor={item => item.id}
            ListFooterComponent={Navigation}
            renderItem={renderItem}
        />
    )
}
