/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import Car from 'assets/svg/car';
import Rub from 'assets/svg/rub';
import Wrench from 'assets/svg/wrench';
import i18n from 'i18n-js';
import moment from 'moment';
import * as R from 'ramda';
import React from 'react';
import { View, Text } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { styles } from 'screens/Main/AddGoods/helper/style';
import {
    CardComponents,
    DiscriptionComponents,
    FooterComponent,
    IDComponents,
    RowComponents
} from 'screens/TaskEditor/components/table/style';
import styled from 'styled-components';
import { uiColor } from 'styled/colors/uiColor.json';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd, priceModed } from 'utils/helper';

const color = (data) => R.cond([
    [R.equals("AllWork"), R.always('trans')],
    [R.equals("preorder"), R.always('classic')],
    [R.equals("inWork"), R.always('trans')],
    [R.equals("completed"), R.always('trans')],
    [R.equals("closed"), R.always('trans')],
    [R.equals("reconciliation"), R.always('classic')],
    [R.equals("suspended"), R.always('trans')],
    [R.equals("canceled"), R.always('trans')],
    [isOdd, R.always('trans')],
])(data);

export const RepairsItem = ({ data }) => (
    <View style={{ flex: 1 }}>
        <CardComponents
            auto
            style={styles.shadow}
        >
            <FlexCompon>
                <FlexCompon>
                    <IDComponents
                        status={R.path(['status'])(data)}
                        style={{ padding: 10 }}
                    >
                        <LabelText
                            color={ColorCard(color(R.path(['status'])(data))).font}
                            fonts="ShellMedium"
                            items="Description"
                            style={{
                                textTransform: 'uppercase'
                            }}
                        >
                            ID
                        </LabelText>
                        <LabelText
                            color={ColorCard(color(R.path(['status'])(data))).font}
                            fonts="ShellBold"
                            items="SDescription"
                        >
                            {R.path(['id'])(data)}
                        </LabelText>
                    </IDComponents>
                    <View style={{ padding: 10 }}>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellLight"
                            items="Description"
                            style={{
                                textAlign: 'left',
                                textTransform: 'uppercase'
                            }}
                        >
                            {i18n.t('status')}
                        </LabelText>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SDescription"
                            style={{
                                textTransform: 'uppercase'
                            }}
                        >
                            {i18n.t(R.path(['status'])(data))}
                        </LabelText>
                    </View>
                </FlexCompon>
                <View style={{ padding: 10 }}>
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellLight"
                        items="Description"
                        style={{
                            textAlign: 'left',
                            textTransform: 'uppercase'
                        }}
                    >
                        {i18n.t('createdAt')}
                    </LabelText>
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SDescription"
                    >
                        {moment(R.path(['createdAt'])(data)).format('DD.MM.YYYY HH:mm')}
                    </LabelText>
                </View>
            </FlexCompon>
            <FooterComponent>
                <RowComponents>
                    <View
                        style={{
                            borderRadius: 50,
                            width: 45,
                            height: 45,
                            alignItems: 'center',
                            alignSelf: 'center',
                            justifyContent: 'center',
                            backgroundColor: uiColor.Polo_Grey,
                        }}
                    >
                        <SvgXml height="30" width="30" xml={Car} />
                    </View>
                    <View
                        style={{
                            flex: 1,
                            padding: 10
                        }}
                    >
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellBold"
                            items="SDescription"
                            style={{ background: "red" }}
                        >
                            {R.path(['vehicle', 'modification', 'model', 'manufacturer', 'name'])(data)} {R.path(['vehicle', 'modification', 'model', 'name'])(data)} {R.path(['vehicle', 'modification', 'model', 'subbody'])(data)}
                        </LabelText>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellLight"
                            items="SDescription"
                            style={{ textAlign: 'left' }}
                        >
                            {R.path(['vehicle', 'vin'])(data)}
                        </LabelText>
                    </View>
                    <PlateNumber
                        color={ColorCard('classic').font}
                        fonts="ShellBold"
                        items="SDescription"
                    >
                        {R.path(['vehicle', 'plate'])(data)}
                    </PlateNumber>
                </RowComponents>
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellLight"
                    items="SDescription"
                >
                    {R.path(['description'])(data)}
                </LabelText>
            </FooterComponent>
            <DiscriptionComponents
                style={{
                    padding: 10
                }}
            >
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                    <SvgXml height="10" width="10" xml={Wrench} />
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SDescription"
                        style={{
                            paddingLeft: 5,
                            alignItems: 'center',
                            color: "#4183C4"
                        }}
                    >
                        {R.path(['performer', 'name', 'last'])(data)} {R.path(['performer', 'name', 'first'])(data)}
                    </LabelText>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                    <Text
                        style={{ fontFamily: "ShellMedium", fontSize: 13, }}
                    >
                        {priceModed(R.path(['totalPrice', 'amount'])(data))}
                    </Text>
                    {R.cond([
                        [R.equals('RUB'), R.always(<SvgXml height="10" weight="5" xml={Rub} />)],
                        [R.T, R.always(<View />)],
                    ])(R.path(['totalPrice', 'currency'])(data))}
                </View>
            </DiscriptionComponents>
        </CardComponents>
    </View>
);

export const PlateNumber = styled(LabelText)`
border-width:2px;
padding:5px;
min-width:100px;
text-transform:uppercase;
border-radius:5px;
text-align:center;
justify-content:center;
align-self:center;
`;

const FlexCompon = styled.View`
flex-direction: row;
justify-content: space-between;
align-items: center;
`;