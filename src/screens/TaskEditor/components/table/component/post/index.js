/* eslint-disable no-extra-parens */
/* eslint-disable id-length */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable no-unsanitized/method */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { StyleSheet } from 'react-native';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { uiColor } from 'styled/colors/uiColor.json';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const PostWorkItem = ({ data }) => {
    const keys = ['#', 'naiming', 'category', 'type', 'numberType'];
    const value = R.paths([['id'], ['name'], ['garagePostType', 'category'], ['garagePostType', 'alias'], ['position']])(data);
    const moderator = R.addIndex(R.map)((x, key) => (R.zipObj([keys[key]], [value[key]])
    ))(keys);

    return (
        <CardComponents
            style={[{ backgroundColor: uiColor.Very_Pole_Grey, flex: 1 }, styles.shadow]}
        >
            {R.addIndex(R.map)((elem, item) => (
                <RowComponents
                    key={item}
                    style={{
                        paddingTop: 2,
                        paddingBottom: 2,
                        flex: 1
                    }}
                >
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SDescription"
                        style={styles.container}
                    >
                        {i18n.t(R.keys(elem))}
                    </LabelText>
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellLight"
                        items="SSDescription"
                        styles={[styles.items, { flex: 1 }]}
                    >
                        {R.cond([
                            [R.equals(['category']), R.always(i18n.t(R.values(elem)))],
                            [R.equals(['type']), R.always(i18n.t(R.values(elem)))],
                            [R.T, R.always(R.values(elem))]
                        ])(R.keys(elem))}
                    </LabelText>
                </RowComponents>
            ))(moderator)}
        </CardComponents >
    )
};

export const styles = StyleSheet.create({
    container: {
        textAlign: 'left',
        textTransform: 'uppercase'
    },
    items: {
        textAlign: 'right',
        textTransform: 'capitalize',
        flex: 1,
    },
    line: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        marginBottom: 5,
        borderStyle: 'dashed',
        borderRadius: 15,
        borderWidth: 1,
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        flex: 1,
        margin: 5,
        marginTop: 1,
        marginBottom: 1
    },
});