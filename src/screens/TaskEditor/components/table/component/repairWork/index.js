/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';
import { SvgXml } from 'react-native-svg';
import Logo from 'assets/svg/next';
import Phone from 'assets/svg/phone';
import i18n from 'i18n-js'
import {
    CardComponents,
    DiscriptionComponents,
    RowComponents
} from 'screens/TaskEditor/components/table/style'


export const RepairWorkItem = ({ data }) => (
    <CardComponents
        style={{
            flex: 1,
            padding: 10
        }}
    >
        <RowComponents>
            <View>
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SSDescription"
                    style={{
                        alignItems: 'center',
                        textAlign: 'left'
                    }}
                    testID="frameNumber"
                >
                    {R.path(['name'])(data)}
                </LabelText>
                <View>
                    <LabelText
                        color={ColorCard('classic').font}
                        items="SDescription"
                        style={{
                            alignItems: 'center'
                        }}
                        testID="frameNumber"
                    >
                        {i18n.t('action')}
                    </LabelText>
                    <LabelText
                        color={ColorCard('classic').font}
                        items="SSDescription"
                        style={{
                            textAlign: 'left',
                            alignItems: 'center'
                        }}
                        testID="frameNumber"
                    >
                        {R.path(['action'])(data)}
                    </LabelText>
                </View>
            </View>
            <View>
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SSDescription"
                    style={{
                        alignItems: 'center'
                    }}
                    testID="frameNumber"
                >
                    {i18n.t('timeHrs')}
                </LabelText>
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SSDescription"
                    style={{
                        alignItems: 'center'
                    }}
                    testID="frameNumber"
                >
                    {R.path(['timeHrs'])(data)} часов
                </LabelText>
            </View>
        </RowComponents>
        <View>
            <LabelText
                color={ColorCard('classic').font}
                items="SDescription"
                style={{
                    textAlign: 'left',
                    alignItems: 'center'
                }}
                testID="frameNumber"
            >
                {i18n.t('discount')} {isOdd(R.path(['discount', 'amount'])(data)) ? R.path(['discount', 'currency'])(data) : R.path(['discount', 'amount'])(data)}
            </LabelText>
        </View>
        <DiscriptionComponents>
            <View>
                <LabelText
                    color={ColorCard('classic').font}
                    items="SDescription"
                    style={{
                        textAlign: 'left',
                        alignItems: 'center'
                    }}
                    testID="frameNumber"
                >
                    {i18n.t('unitPrice')}
                </LabelText>
                <LabelText
                    color={ColorCard('classic').font}
                    items="Description"
                    style={{
                        alignItems: 'center'
                    }}
                    testID="frameNumber"
                >
                    {R.path(['unitPrice', 'amount'])(data)} {R.path(['unitPrice', 'currency'])(data)}
                </LabelText>
            </View>
            <View>
                <LabelText
                    color={ColorCard('classic').font}
                    items="SDescription"
                    style={{
                        textAlign: 'left',
                        alignItems: 'center'
                    }}
                    testID="frameNumber"
                >
                    {i18n.t('unitCount')}
                </LabelText>
                <LabelText
                    color={ColorCard('classic').font}
                    items="Description"
                    style={{
                        alignItems: 'center'
                    }}
                    testID="frameNumber"
                >
                    {R.path(['unitCount'])(data)}
                </LabelText>
            </View>
            <View>
                <LabelText
                    color={ColorCard('classic').font}
                    items="SDescription"
                    style={{
                        textAlign: 'left',
                        alignItems: 'center'
                    }}
                    testID="frameNumber"
                >
                    {i18n.t('totalPrice')}
                </LabelText>
                <LabelText
                    color={ColorCard('classic').font}
                    items="Description"
                    style={{
                        alignItems: 'center'
                    }}
                    testID="frameNumber"
                >
                    {R.path(['unitPrice', 'amount'])(data) * R.path(['unitCount'])(data)} {R.path(['unitPrice', 'currency'])(data)}
                </LabelText>
            </View>
        </DiscriptionComponents>
    </CardComponents>
);


export const RepairWorkRender = ({ data, dispatch }) => (
    <BlocBackground>
        <TouchHighBackButtom
            color
            onPress={() => dispatch({
                type: "CALL",
                payload: R.path(['phone'])(data)
            })}
            testID="CALL"
        >
            <SvgXml height="30" width="30" xml={Phone}/>
        </TouchHighBackButtom>
        <TouchHighBackButtom
            onPress={() => dispatch({
                type: "Edit",
                payload: R.path(['id'])(data),
            })}
            testID="NAVIGATE"
        >
            <SvgXml height="30" width="30" xml={Logo}/>
        </TouchHighBackButtom>
    </BlocBackground>
);