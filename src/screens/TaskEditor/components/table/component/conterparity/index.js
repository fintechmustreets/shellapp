/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import i18n from 'i18n-js';
import * as R from 'ramda';
import React from 'react';
import { StyleSheet } from 'react-native';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd, phoneMedd } from 'utils/helper';

export const ConterparityItem = ({ data }) => {
    const key = ['num', 'name', 'phone', 'email', 'inn'];
    const value = R.paths([['id'], ['name'], ['phone'], ['email'], ['inn']])(data);
    const moderator = R.addIndex(R.map)(((x, keys) => {
        return R.zipObj([key[keys]], [value[keys]])
    }))(key);

    const addressAccount = R.join(' ', R.values(R.omit(['__typename'])(R.path(['physicalAddress'])(data))));

    return (
        <CardComponents
            style={[styles.shadow, { flex: 1 }]}
        >
            {R.addIndex(R.map)((x, keys) => (
                <RowComponents
                    key={keys}
                    style={{ flex: 1 }}
                >
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SDescription"
                        style={styles.container}
                    >
                        {i18n.t(R.keys(x))}
                    </LabelText>
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellLight"
                        items="SSDescription"
                        style={styles.items}
                    >
                        {R.cond([
                            [R.equals(['phone']), R.always(phoneMedd(R.head(R.values(x))))],
                            [R.T, R.always(R.values(x))]
                        ])(R.keys(x))}
                    </LabelText>
                </RowComponents>
            ))(moderator)}
            <RowComponents
                style={{
                    justifyContent: "space-between"
                }}
            >
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="SDescription"
                    style={styles.container}
                >
                    {i18n.t('physicalAddress')}
                </LabelText>
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellLight"
                    items="SSDescription"
                    style={[styles.items, { flex: 1, marginLeft: 10 }]}
                >
                    {isOdd(R.trim(addressAccount)) ? "---" : addressAccount}
                </LabelText>
            </RowComponents>
        </CardComponents>
    )
};

export const styles = StyleSheet.create({
    container: {
        textAlign: 'left',
        textTransform: 'uppercase'
    },
    items: {
        textAlign: 'right',
        flex: 1
    },
    line: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        marginBottom: 5,
        borderStyle: 'dashed',
        borderRadius: 15,
        borderWidth: 1,
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        flex: 1,
        margin: 5,
        marginTop: 1,
        marginBottom: 1
    },
});