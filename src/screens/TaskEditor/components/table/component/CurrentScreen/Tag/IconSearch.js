/* eslint-disable max-len */
/* eslint-disable react/jsx-key */
/* eslint-disable react/no-multi-comp */
/* eslint-disable newline-after-var */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import React from 'react';
import { Text, View } from 'react-native';
import { Colors, IconButton } from 'react-native-paper';
import { style } from './style'

export const IconSearch = ({ title, dispatch, value }) => (
    <View
        style={style.container}
    >
        <View
            style={style.view}
        >
            <Text style={style.title}>{i18n.t(title)} :</Text>
            <Text style={style.text}>{value}</Text>
        </View>
        <View
            style={style.item}
        >
            <IconButton
                color={Colors.black500}
                icon="close"
                onPress={dispatch}
                size={20}
            />
        </View>
    </View>
);

