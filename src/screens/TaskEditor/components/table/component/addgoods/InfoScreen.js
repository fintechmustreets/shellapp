/* eslint-disable object-curly-newline */
/* eslint-disable react/no-multi-comp */
/* eslint-disable multiline-ternary */
/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { RowComponents } from 'screens/TaskEditor/components/table/style';

export const InfoScreen = ({ updata, state }) => {
    if (R.equals(true, updata)) {
        return (
            <RowComponents style={{ justifyContent: 'flex-start', margin: 10 }}>
                <Text
                    style={[stylex.text]}
                >
                    {R.pipe(
                        R.paths([['amount', 'reserved']]),
                        R.append(' '),
                        R.append(i18n.t(R.path(['measure', 'code'])(state))),
                        R.join(' '))(state)}
                </Text>
            </RowComponents>
        )
    }

    return (
        <View />
    )
};

const stylex = StyleSheet.create({
    text: {
        textAlign: 'left',
        fontFamily: 'ShellMedium',
        fontSize: 11,
        textTransform: 'lowercase',
        paddingLeft: 5
    },
    amount: {
        textAlign: 'left',
        fontFamily: 'ShellBold',
        fontSize: 10
    }
})