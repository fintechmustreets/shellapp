/* eslint-disable react/no-multi-comp */
/* eslint-disable multiline-ternary */
/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import Rub from 'assets/svg/rub';
import * as R from 'ramda';
import React from 'react';
import { Text, View } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const IconState = ({ item }) => {
    const mesureUpdate = R.anyPass([R.equals(3), R.equals(1), R.equals(2)]);
    const svgXml = R.anyPass([R.equals(6), R.equals(5)]);

    return R.cond([
        [R.equals('vat'), () => (
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SDescription"
                style={[styles.items, {
                    paddingLeft: 10,
                    justifyContent: 'center'
                }]}
                testID="BlockLaker"
            >
                %
            </LabelText>
        )],
        [mesureUpdate, () => (
            <View />
        )],
        [svgXml, () => (
            <SvgXml
                height="10"
                style={[styles.items, { paddingLeft: 10, justifyContent: 'center' }]}
                weight="10"
                xml={Rub}
            />
        )],
        [R.T, () => (
            <Text
                style={[styles.items]}
            />)],
        [R.F, () => (
            <Text
                style={[styles.items,
                { paddingLeft: 10, justifyContent: 'center' }]}
            />)],
    ])(item)
};