/* eslint-disable no-confusing-arrow */
import styled from 'styled-components'
import { uiColor } from 'styled/colors/uiColor.json';
import * as R from 'ramda'
import { isOdd } from 'utils/helper'

export const FooterComponent = styled.View`
background-color: ${uiColor.Very_Pole_Grey};
padding: 10px;
`;
export const CardComponents = styled.View`
flex-direction: column;
padding:${props => props.auto ? "0px" : "10px"};
justify-content: space-between;
background-color:${uiColor.White};
`;
const status = ({ props }) => R.cond([
    [R.equals("AllWork"), R.always(uiColor.White)],
    [R.equals("preorder"), R.always(uiColor.Polo_Grey)],
    [R.equals("inWork"), R.always(uiColor.Light_Blue)],
    [R.equals("completed"), R.always(uiColor.Dark_Green)],
    [R.equals("closed"), R.always(uiColor.Dark_Green)],
    [R.equals("reconciliation"), R.always(uiColor.Yellow)],
    [R.equals("suspended"), R.always(uiColor.Yellow)],
    [R.equals("canceled"), R.always(uiColor.Red)],
    [isOdd, R.always(uiColor.White)],
])(props.status);

export const IDComponents = styled.View`
width:52px;
height:52px;
align-items: center;
justify-content: center;
background-color: ${props => status({ props })};
`;

export const RowComponents = styled.View`
flex-direction: row;
justify-content: space-between;
align-items: center;
`;
export const DiscriptionComponents = styled.View`
flex-direction: row;
align-items: center;
justify-content: space-between;
padding: 10px;
`;