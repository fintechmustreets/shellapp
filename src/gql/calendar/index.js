import gql from 'graphql-tag'

export const GARAGEPOST = gql`
query($where:WhereGaragePostReserveInput){
    garagePostReserves(where:$where) {
      items {
          id
          startAt
          endAt
          garagePostId
          garagePost {
            id
            name
            garagePostType {
              id
              category
            }
          }
          repairId
          repair {
            id
            description
            status
            totalPrice {
              amount
              currency
            }
            vehicle {
              id
              vin
              frameNumber
              plate
              modification {
                id
                name
                modelId
                model {
                  id
                  name
                  subbody
                  manufacturerId
                  manufacturer {
                    id
                    name
                  }
                }
              }
            }
          }
        }
      }
    }
    `;

export const GARAGECALENDARPOST = gql`
query($where: WhereGaragePostReserveInput) {
  garagePostReserves(where: $where) {
    items {
      id
      startAt
      endAt
      repairId
    }
  }
}
`;

export const CREATE_GARAGEPOST = gql`
mutation($input: CreateGaragePostReserveInput!) {
  createGaragePostReserve(input: $input) {
    id
    startAt
    endAt
    garagePostId
    garagePost {
      id
      name
      garagePostType {
        id
        category
      }
    }
  }
}
`;

export const UPDATE_GARAGEPOST = gql`
mutation($input: UpdateGaragePostReserveInput!) {
  updateGaragePostReserve(input: $input) {
    id
    startAt
    endAt
    garagePostId
    garagePost {
      id
      name
      garagePostType {
        id
        category
      }
    }
  }
}
`;