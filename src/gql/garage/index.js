import gql from 'graphql-tag'

export const DESTROY_DEPOT = gql`
mutation($id:Int!) {
    destroyDepot(id:$id){
      id
      name
      description
    }
}
`;

export const DEPOT_QUERY = gql`
query($where: WhereDepotInput!) {
    depot(where: $where) {
      id
      name
      description
    }
  }
`;

export const RESERVED_DEPOTS = gql`
query($where: WhereStockInput, $order: OrderStockInput, $paginate: PageInput) {
  stocks(where: $where, order: $order, paginate: $paginate) {
    items {
      id
      name
      description
      article
      oem
      brand
      measure {
        code
        precision
        name
        abbr
      }
      garageId
      stockGroupId
      stockGroup {
        id
        name
        parent {
          id
          name
        }
      }
      depotItems {
        id
        depotId
        stockId
        amount {
          available
          reserved
          released
        }
        priceIn {
          amount
          currency
        }
        priceOut {
          amount
          currency
        }
        vat
      }
      amount {
        available
        reserved
        released
      }
      createdAt
      updatedAt
    }
  }
}
`;

export const STOCK_TREE_DEPOTS = gql`
    query($where:WhereStockGroupInput){
        stockGroupsTree(where:$where) {
          id
          name
          children {
            id
            name
            children {
              id
              name
              parentId
              createdAt
              updatedAt
            }
            parentId
            parent {
              id
              name
              parentId
              createdAt
              updatedAt
            }
            createdAt
            updatedAt
          }
          parentId
          parent {
            id
            name
            children {
              id
              name
              parentId
              createdAt
              updatedAt
            }
            parentId
            parent {
              id
              name
              parentId
              createdAt
              updatedAt
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
    }
    `;

export const RECEIVE_DEPOTS = gql`
query(
    $where: WhereReceiveDepotOperationInput
    $order: OrderReceiveDepotOperationInput
    $paginate: PageInput
    ){
    receiveDepotOperations(
        where: $where
        order: $order
        paginate: $paginate
        ) {
        items {
        id
        items {
            stockId
            depotId
            depotItem {
            id
            stockId
            stock {
                id
                name
                description
                article
                oem
                brand
                measure {
                code
                precision
                name
                abbr
                }
                garageId
                stockGroupId
                stockGroup {
                id
                name
                parent {
                    id
                    name
                }
                }
                depotItems {
                id
                amount {
                    available
                }
                priceOut {
                    amount
                    currency
                }
                }
                amount {
                available
                reserved
                released
                }
                createdAt
                updatedAt
            }
            depotId
            depot {
                id
                name
                description
                createdAt
                updatedAt
            }
            priceIn {
                amount
                currency
            }
            priceOut {
                amount
                currency
            }
            measure {
                code
                precision
                name
                abbr
            }
            amount {
                available
                reserved
                released
            }
            vat
            createdAt
            updatedAt
            }
            priceIn {
            amount
            currency
            }
            vat
            amount
        }
        counterparty {
            type
            id
            name
            phone
            email
            inn
            physicalAddress {
            postcode
            state
            city
            street
            building
            }
            bankDetails {
            id
            name
            currentAccount
            correspondentAccount
            rcbic
            counterpartyId
            mainBankDetail
            createdAt
            updatedAt
            }
            createdAt
            updatedAt
            description
            psrn
            kpp
            legalAddress {
            postcode
            state
            city
            street
            building
            }
            registrationAddress {
            postcode
            state
            city
            street
            building
            }
            personalData
            psrnsp
            certificateNumber
            certificateDate
        }
        actor {
            id
            name {
            first
            last
            middle
            }
            phone
            garageId
            garage {
            id
            name
            address {
                postcode
                state
                city
                street
                building
            }
            costPerHour {
                amount
                currency
            }
            phone
            email
            legalName
            legalAddress {
                postcode
                state
                city
                street
                building
            }
            bankName
            checkingAccount
            correspondentAccount
            rcbic
            defaultPartSurcharge
            inn
            kpp
            psrn
            createdAt
            updatedAt
            }
            role
            blocked
            createdAt
            updatedAt
        }
        document {
            id
            type {
            code
            name
            objectTypes
            }
            number
            date
            objectId
            objectType
            attachments {
            id
            objectId
            objectType
            objectProperty
            filename
            mimetype
            encoding
            file {
                url
                path
            }
            }
            createdAt
            updatedAt
        }
        createdAt
        updatedAt
        }
        info {
        totalCount
        page
        limit
        }
    }
    }
`;

export const MUTATION_DEPOTSINPUT = gql`
mutation($input:CreateReceiveDepotOperationInput) {
    createReceiveDepotOperation(
      input: $input
    ) {
      id
      items {
        stockId
        depotId
        depotItem {
          id
          stockId
          stock {
            id
            name
            description
            article
            oem
            brand
            measure {
              code
              precision
              name
              abbr
            }
            garageId
            stockGroupId
            stockGroup {
              id
              name
              parent {
                id
                name
              }
            }
            depotItems {
              id
              amount {
                available
              }
              priceOut {
                amount
                currency
              }
            }
            amount {
              available
              reserved
              released
            }
            createdAt
            updatedAt
          }
          depotId
          depot {
            id
            name
            description
            createdAt
            updatedAt
          }
          priceIn {
            amount
            currency
          }
          priceOut {
            amount
            currency
          }
          measure {
            code
            precision
            name
            abbr
          }
          amount {
            available
            reserved
            released
          }
          vat
          createdAt
          updatedAt
        }
        priceIn {
          amount
          currency
        }
        vat
        amount
      }
      counterparty {
        type
        id
        name
        phone
        email
        inn
        physicalAddress {
          postcode
          state
          city
          street
          building
        }
        bankDetails {
          id
          name
          currentAccount
          correspondentAccount
          rcbic
          counterpartyId
          mainBankDetail
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
        description
        psrn
        kpp
        legalAddress {
          postcode
          state
          city
          street
          building
        }
        registrationAddress {
          postcode
          state
          city
          street
          building
        }
        personalData
        psrnsp
        certificateNumber
        certificateDate
      }
      actor {
        id
        name {
          first
          last
          middle
        }
        phone
        garageId
        garage {
          id
          name
          address {
            postcode
            state
            city
            street
            building
          }
          costPerHour {
            amount
            currency
          }
          phone
          email
          legalName
          legalAddress {
            postcode
            state
            city
            street
            building
          }
          bankName
          checkingAccount
          correspondentAccount
          rcbic
          defaultPartSurcharge
          inn
          kpp
          psrn
          createdAt
          updatedAt
        }
        role
        blocked
        createdAt
        updatedAt
      }
      document {
        id
        type {
          code
          name
          objectTypes
        }
        number
        date
        objectId
        objectType
        attachments {
          id
          objectId
          objectType
          objectProperty
          filename
          mimetype
          encoding
          file {
            url
            path
          }
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
  `;

export const UPDATE_DEPOTS = gql`
  mutation($input: UpdateDepotInput!) {
    updateDepot(input: $input) {
      id
      name
      description
    }
  }
  `;