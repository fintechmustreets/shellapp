import gql from 'graphql-tag'

export const QUERYPOST = gql`
query(
    $where: WhereGaragePostInput
    $order: OrderGaragePostInput
    $paginate: PageInput
  ) {
    garagePosts(where: $where, order: $order, paginate: $paginate) {
      items {
        id
        name
        garageId
        position
        garagePostTypeId
        garagePostType {
          id
          alias
          category
          createdAt
          updatedAt
        }
      }
      info {
        totalCount
        page
        limit
      }
    }
  }
  `;

export const GARAGEPOST = gql`
{
    garagePostTypes {
      id
      alias
      category
      createdAt
      updatedAt
      __typename
    }
  }
  `;

export const CREATE_POST = gql`
mutation($input:CreateGaragePostInput!) {
  createGaragePost(input: $input) {
    id
    name
    garageId
    position
    garagePostTypeId
    garagePostType {
      id
      alias
      category
      createdAt
      updatedAt
    }
  }
}
`;

export const UPGRADE_POST = gql`
mutation ($input:UpdateGaragePostInput!) {
    updateGaragePost(
      input: $input
    ) {
      id
      name
      garageId
      position
      garagePostTypeId
      garagePostType {
        id
        alias
        category
        createdAt
        updatedAt
      }
    }
  }
`;

export const DESTROY_GARAGE = gql`
mutation($input: Int!) {
  destroyGaragePost(id: $input) {
    id
    name
    garageId
    position
    garagePostTypeId
    garagePostType {
      id
      alias
      category
      createdAt
      updatedAt
    }
  }
}
`;