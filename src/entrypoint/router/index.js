/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-wrap-multilines */
import { DrawerContentScrollView } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import * as R from 'ramda';
import React from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import Drawler from 'screens/Drawler';
import { uiColor } from 'styled/colors/uiColor.json';
import { HeaderScreen } from 'utils/routing';
import Main from './configRouter/mainConfig';
import Register from './configRouter/registerConfig';
import { Drawer } from './stack';

const DrawlerScreen = () => {
  const user = useSelector(state => state.profile, shallowEqual);

  const jswTokenEnable = R.isNil(R.path(['jswToken'])(user));
  const initialRouter = R.cond([
    [R.equals(true), R.always(HeaderScreen.Home)],
    [R.equals(false), R.always(HeaderScreen.Main)],
  ])(jswTokenEnable);

  if (jswTokenEnable) {
    return (
      <Drawer.Navigator >
        <Drawer.Screen
          component={Register}
          name={HeaderScreen.Home}
          options={{
            gestureEnabled: false,
            initialRouteName: HeaderScreen.Home
          }}
        />
      </Drawer.Navigator>)
  }

  return (
    <Drawer.Navigator
      activeBackgroundColor={uiColor.Very_Pole_Grey}
      drawerContent={props => (
        <DrawerContentScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <Drawler {...props} />
        </DrawerContentScrollView>
      )}
      drawerStyle={{
        backgroundColor: uiColor.Very_Pole_Grey,
      }}
      drawerType="slide"
      initialRouteName={initialRouter}
    >
      <Drawer.Screen
        component={Main}
        name={HeaderScreen.Main}
        options={{
          initialRouteName: HeaderScreen.Main
        }}
      />
    </Drawer.Navigator>)
};

const CombineRouterConnector = () => (
  <NavigationContainer>
    <DrawlerScreen />
  </NavigationContainer>
);

export default CombineRouterConnector;
