/* eslint-disable multiline-ternary */
/* eslint-disable max-len */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable object-curly-newline */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-sort-default-props */
/* eslint-disable react/sort-prop-types */
/* eslint-disable react/jsx-indent-props */
import searchSvg from 'assets/svg/search'
import PropTypes from 'prop-types'
import * as R from 'ramda'
import React, { useState, useRef } from 'react'
import { View, Text } from 'react-native'
import { isOdd } from 'utils/helper'
import validator from 'validator'
import { Components } from './Component'
import { Block, BlockChildren, InputSearch, SearchContainer, SvgImage } from './style'


export const FuncComponentSearch = ({ setUpdate, search, data }) => {
    const [active, setActive] = useState(null);
    // const VARIABLES = validator.isNumeric(R.path(['search'])(search)) ? R.last(data) : R.head(data)

    return (
        <BlockChildren>
            {R.map(path => (
                <Components
                    key={R.path(['path'])(path)}
                    active={active}
                    data={data}
                    path={path}
                    setActive={(e) => setActive(e)}
                    setUpdate={(e) => setUpdate(e)}
                />
            ))(data)}
        </BlockChildren>
    )
};


export const SearchBar = ({
    search, onChangeText, onEndEditing, onSubmitEditing, placeholder, children, data
}) => (
        <SearchContainer>
            {isOdd(data) ? <View /> : <Text>{data}</Text>}
            <Block>
                <SvgImage xml={searchSvg} />
                <InputSearch
                    ref={useRef()}
                    autoFocus
                    enablesReturnKeyAutomatically
                    onChangeText={text => onChangeText(text)}
                    onEndEditing={text => onEndEditing(text)}
                    onSubmitEditing={(e) => onSubmitEditing(e)}
                    placeholder={placeholder}
                    style={{ flex: 1 }}
                    value={search}
                />
            </Block>
            {isOdd(search) ? <View /> : children}
        </SearchContainer>
    );

SearchBar.propTypes = {
    search: PropTypes.string,
    onChangeText: PropTypes.func,
    onEndEditing: PropTypes.func,
    onSubmitEditing: PropTypes.func,
    placeholder: PropTypes.string
};

SearchBar.defaultProps = {
    search: null,
    onChangeText: () => { return null },
    onEndEditing: () => { return null },
    onSubmitEditing: () => { return null },
    placeholder: "Введите текст для поиска"
};
