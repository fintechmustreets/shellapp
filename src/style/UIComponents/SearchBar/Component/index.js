/* eslint-disable no-unneeded-ternary */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable object-curly-newline */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/no-multi-comp */
import React from 'react'
import * as R from 'ramda'
import {
    ButtonSearchClick, TextSelecter
} from '../style'

export const SearchButtom = ({ onPress, title, lenght, active }) => (
    <ButtonSearchClick
        active={active}
        lenght={lenght}
        onPress={(e) => onPress(e)}
    >
        <TextSelecter
            active={active}
            lenght={lenght}
            style={{ fontSize: 14 }}
        >
            {title}
        </TextSelecter>
    </ButtonSearchClick>
);

export const Components = ({ path, active, data, setActive, setUpdate }) => (
    <SearchButtom
        key={R.path(['path'])(path)}
        active={R.equals(R.path(['path'])(path))(active) ? true : false}
        lenght={R.length(R.head(data))}
        onPress={() => {
            setActive(R.path(['path'])(path));
            setUpdate(R.path(['path'])(path))
        }}
        title={R.path(['title'])(path)}
    />
);
