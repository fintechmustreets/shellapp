/* eslint-disable no-confusing-arrow */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import PropTypes from 'prop-types';
import React from 'react';
import { View } from 'react-native';
import styled from 'styled-components';
import { SvgXml } from 'react-native-svg';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { ButtonContainer, ButtonText } from 'styled/UIComponents/UiButtom/style';
import * as R from 'ramda'

export const TouchesState = ({
    onPress, title, color, flex, merge, disabled, button, unTransform, svg, font
}) => (
        <ButtonContainer
            color={color}
            disabled={disabled}
            merge={merge}
            onPress={(click) => onPress(click)}
            style={{
                flex,
                minHeight: button ? 30 : 45,
                maxHeight: button ? 30 : 45,
            }}
        >
            <RowComponents style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                {svg ?
                    <View
                        style={{ paddingRight: 10 }}
                    >
                        <SvgXml
                            height="15"
                            width="15"
                            xml={svg}
                        />
                    </View> : <View />}
                <ButtonText
                    color={color}
                    style={{
                        fontFamily: R.defaultTo('ShellMedium')(font),
                        textTransform: 'none'
                    }}
                    unTransform={unTransform}
                >
                    {i18n.t(title)}
                </ButtonText>
            </RowComponents>
        </ButtonContainer>
    );

export const ButtonBlock = ({ dispatch, mode, title }) => {
    return (
        <ButtonStyleBLock
            style={{
                flexDirection: 'row'
            }}
        >
            <TouchesState
                color="Yellow"
                flex={0}
                onPress={(e) => mode ? dispatch({ type: "Create" }) : dispatch({ type: "Update" })}
                title={title}
            />
        </ButtonStyleBLock>
    )
};

const UiButtom = ({
    color, loading, onPress, Title
}) => (
        <ButtonContainer
            color={color}
            onPress={(click) => (loading ? null : onPress(click))}
            style={{
                flex: 1,
                fontFamily: "ShelBold",
                minHeight: 60,
            }}
        >
            <ButtonText
                color={color}
            >
                {Title}
            </ButtonText>
        </ButtonContainer>
    );

UiButtom.propTypes = {
    color: PropTypes.string,
    loading: PropTypes.bool,
    onPress: PropTypes.func,
    Title: PropTypes.string,
};

UiButtom.defaultProps = {
    color: "Red",
    loading: true,
    onPress: null,
    Title: null,
};

export const ButtonStyleBLock = styled.View`
justify-content: flex-end;
bottom:0;
`;

export default UiButtom
