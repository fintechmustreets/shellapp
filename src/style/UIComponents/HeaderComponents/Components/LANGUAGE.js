/* eslint-disable prefer-destructuring */
/* eslint-disable newline-after-var */
/* eslint-disable scanjs-rules/call_setTimeout */
/* eslint-disable scanjs-rules/call_connect */
/* eslint-disable scanjs-rules/call_hide */
/* eslint-disable react/no-set-state */
/* eslint-disable object-shorthand */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable space-before-function-paren */
/* eslint-disable react/sort-comp */
/* eslint-disable react/jsx-handler-names */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-first-prop-new-line */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
import React from 'react'
import * as R from 'ramda'
import { TouchableOpacity, View } from 'react-native'
import { SvgXml } from 'react-native-svg'
import ru from 'assets/svg/languages/ru'
import en from 'assets/svg/languages/en'
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import { uiColor } from 'styled/colors/uiColor.json';
import { connect } from 'react-redux'


const location = R.cond([
    [R.equals('ru'), R.always(ru)],
    [R.equals('en'), R.always(en)],
]);

class App extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            lang: this.props.profile.language,
        };
    }

    _menu = null;


    setMenuRef = ref => {
        this._menu = ref;
    };

    hideChangeMenu = () => {
        const lang = R.equals(this.state.lang, 'ru') ? "en" : "ru";
        this.props.changeLanguage(lang);
        this.setState({ lang });
        this._menu.hide();
        setTimeout(() => {
            this.props.changeLanguage(lang)
        }, 1);
    };

    hideMenu = () => {
        const lang = this.state.lang;
        this.props.changeLanguage(lang);
        this._menu.hide();
        setTimeout(() => {
            this.props.changeLanguage(lang)
        }, 1);
    };

    componentDidMount() {
        const lang = this.state.lang;
        setTimeout(() => {
            this.props.changeLanguage(lang)
        }, 1);
    }

    showMenu = () => {
        this._menu.show();
    };

    render() {
        return (
            <View style={{
                height: 40,
                width: 40,
                alignItems: 'center',
                justifyContent: 'center'
            }}
            >
                <Menu
                    ref={this.setMenuRef}
                    button={
                        <TouchableOpacity
                            onPress={this.showMenu}
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                flex: 1,
                                width: 40,
                                height: 40
                            }}
                        >
                            <SvgXml height="25" width="25" xml={location(this.state.lang)} />
                        </TouchableOpacity>
                    }
                >
                    <MenuItem
                        onPress={this.hideMenu}
                        style={{ backgroundColor: uiColor.Yellow }}
                    >
                        <SvgXml height="25" width="25" xml={location(this.state.lang)} />
                    </MenuItem>
                    <MenuItem
                        onPress={this.hideChangeMenu}
                    >
                        <SvgXml height="25" width="25" xml={location(R.equals('ru', this.state.lang) ? 'en' : 'ru')} />
                    </MenuItem>
                    <MenuDivider />
                </Menu>
            </View>
        );
    }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => {
    return {
        changeLanguage: (data) => dispatch({ type: 'LANG', payload: data }),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);