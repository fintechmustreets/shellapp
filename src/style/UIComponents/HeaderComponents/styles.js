/* eslint-disable no-confusing-arrow */
import styled from 'styled-components/native';

const HeaderContainer = styled.View`
position: ${props => props.absolute ? 'absolute' : 'relative'}; 
min-height:60px;
flex-direction:row;
padding:10px;
justify-content:space-around;
`;

export { HeaderContainer };
