import { StyleSheet, } from 'react-native'

export const Styles = StyleSheet.create({
    item: {
        textAlign: 'left',
        textTransform: 'uppercase',
        flex: 1,
        margin: 10,
        fontFamily: 'ShellMedium',
        fontSize: 13
    },
    items: {
        backgroundColor: 'gold',
        padding: 3,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10
    }
});
