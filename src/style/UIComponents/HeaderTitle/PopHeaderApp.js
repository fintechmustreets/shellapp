/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
import React from 'react';
import i18n from 'localization';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { HeaderButton } from 'styled/UIComponents/HeaderTitle/HeaderButton';
import * as R from 'ramda'
import { Text, View } from 'react-native'
import { SvgXml } from 'react-native-svg';
import { ButtonText } from 'styled/UIComponents/UiButtom/style';
import {
    Menu, MenuOption, MenuOptions, MenuTrigger
} from 'react-native-popup-menu';
import { Styles } from './Styles'

export const PopHeaderApp = ({
    popUp, table, button, svg, dispatch, empty
}) => {
    if (popUp) {
        return (
            <Menu
                style={{ marginRight: 10 }}
            >
                <MenuTrigger>
                    {svg ? <RowComponents
                        style={Styles.items}
                    >
                        <SvgXml
                            height="20"
                            width="20"
                            xml={svg}
                        />
                        <View style={{ paddingLeft: 10 }}>
                            <ButtonText
                                color="Yellow"
                                style={{
                                    fontFamily: 'ShellMedium'
                                }}
                            >
                                {i18n.t(button)}
                            </ButtonText>
                        </View>
                    </RowComponents> :
                        <View />}
                </MenuTrigger>
                <MenuOptions
                    style={{ margin: 5 }}
                >
                    {R.addIndex(R.map)((x, key) => (
                        <MenuOption
                            key={key}
                            onSelect={() => dispatch(x)}
                        >
                            <Text>{i18n.t(x)}</Text>
                        </MenuOption>))(table)}
                </MenuOptions>
            </Menu >
        )
    }

    return (
        <HeaderButton
            button={button}
            dispatch={dispatch}
            empty={empty}
            svg={svg}
        />
    )
}