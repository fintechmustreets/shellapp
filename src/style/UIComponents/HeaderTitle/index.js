/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import React from 'react';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { Text } from 'react-native';
import { Styles } from './Styles'
import { PopHeaderApp } from './PopHeaderApp'

export const HeaderApp = ({
    dispatch, title, button, empty, header, svg, popUp, table
}) => (
        <RowComponents>
            <Text
                style={[Styles.item, {
                    color: header ? '#DD1D21' : '#404040',
                    fontFamily: header ? "ShellBold" : 'ShellMedium',
                    fontSize: header ? 14 : 13
                }]}
            >
                {title}
            </Text>
            <PopHeaderApp
                button={button}
                dispatch={dispatch}
                empty={empty}
                popUp={popUp}
                svg={svg}
                table={table}
            />
        </RowComponents>
    );