/* eslint-disable dot-notation */
import { SvgXml } from 'react-native-svg'
import styled from 'styled-components/native'

export const SvgImage = styled(SvgXml)`
height:30px;
width:30px;
margin-right:10px;
`;

export const DrawlerView = styled.TouchableOpacity`
flex-direction:row;
align-items:center;
justify-content:space-around;
text-align:center;
height: 51px;
background: ${props => props.theme.drawler[props.items]["background"]};
`;

export const DrawlerText = styled.Text`
font-size: ${props => props.theme.drawler[props.items]["size"]};
color: ${props => props.theme.drawler[props.items]["font"]};
font-weight: ${props => props.theme.drawler[props.items]["weight"]};
text-align: ${props => props.theme.drawler[props.items]["align"]};
justify-content: ${props => props.theme.drawler[props.items]["js"]};
align-items: ${props => props.theme.drawler[props.items]["js"]};
padding-left: ${props => props.theme.drawler[props.items]["left"]};
`;