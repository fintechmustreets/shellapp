/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/display-name */
import PropTypes from 'prop-types'
import React, { useState } from 'react'
import { ViewPropTypes, YellowBox } from 'react-native'
import TextInputMask from 'react-native-text-input-mask'
import styled from 'styled-components/native'

YellowBox.ignoreWarnings([
  'Warning: Fail',
  'Warning: component',
]);

const CustomTextInput = React.forwardRef(({
  containerStyle = {},
  errorMessage,
  style = {},
  label,
  editable,
  placeholder,
  renderRightIcon,
  value,
  onEndEditing,
  maxLength,
  onFocus,
  onBlur,
  ...rest
}, ref) => {
  const [focused, setFocused] = useState(false);
  const Input = rest.mask ? StyledMaskInput : StyledInput;

  return (
    <InputContainer style={containerStyle}>
      <Body>
        <Input
          ref={ref}
          {...rest}
          editable={editable}
          fontSize={13}
          inputRef={input => {
            ref.current = input
          }}
          maxLength={maxLength}
          onBlur={(click) => {
            setFocused(false);
            if (onBlur) onBlur(click)
          }}
          onFocus={(click) => {
            setFocused(true);
            if (onFocus) onFocus(click)
          }}
          placeholder={focused ? '' : placeholder}
          selectTextOnFocus
          style={style}
          value={value}
        />
        {errorMessage && <Label>{errorMessage}</Label>}
      </Body>
      {
        renderRightIcon && (
          <RightIconContainer>
            {renderRightIcon()}
          </RightIconContainer>
        )
      }
    </InputContainer >
  )
});

CustomTextInput.propTypes = {
  containerStyle: ViewPropTypes.style,
  label: PropTypes.string,
  labelStyle: ViewPropTypes.style,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  placeholder: PropTypes.string,
  renderRightIcon: PropTypes.func,
  showLabel: PropTypes.bool,
  style: ViewPropTypes.style,
  value: PropTypes.string,
};

CustomTextInput.defaultProps = {
  containerStyle: {},
  label: null,
  labelStyle: {},
  onBlur: () => null,
  onFocus: () => null,
  placeholder: null,
  renderRightIcon: () => null,
  showLabel: false,
  style: {},
  value: null,
};

const sharedInputStyles = `
  font-size: 14px;
  width: 100%;
`;

const StyledInput = styled.TextInput`
  ${sharedInputStyles};
`;

const StyledMaskInput = styled(TextInputMask)`
  ${sharedInputStyles};
`;

const InputContainer = styled.View`
  padding: 10px 0 12px;
  min-height: 70px;
  flex-direction: row;
  justify-content: center;
  position: relative;
`;
const Body = styled.View`
  align-self: stretch;
  flex: 1;
  flex-direction: column;
  justify-content: flex-end;
`;
const Label = styled.Text`
  font-weight: 300;
  font-size: 10px;
  line-height: 21px;
  text-align:right;
`;

const RightIconContainer = styled.View`
  align-self: stretch;
  justify-content: center;
`;

export default CustomTextInput
