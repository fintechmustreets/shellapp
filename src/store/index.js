// TODO ADD NEW ACTION REDUCER
// TODO DOWNLOAD REDUCER INITIAL QUERY DATA
// TODO SAGAS QUERY LOADER
// TODO ERROR SCREEN PACKAGES
// TODO COLLECT REDUCER
// TODO UPDATE REDUCER STORAGE WHEN INPUT GRAPHQL

// eslint-disable-next-line object-curly-newline
import { configureStore } from '@reduxjs/toolkit';
import { persistStore } from 'redux-persist';
import thunk from 'redux-thunk';
import { rootReducer } from './reducer';

const store = configureStore({
    reducer: rootReducer,
    middleware: [thunk],
});

const persistor = persistStore(store);

export { persistor, store };