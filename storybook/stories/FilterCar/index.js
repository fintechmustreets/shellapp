/* eslint-disable max-statements */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { DropDownScreen } from 'screens/Main/TaskBack/first/CarsEdit/CurrentSelect/DropDownScreen';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { AppScreen } from './AppScreen'
import { EmployeeSelector } from './EmployeeSelector'

export const mamatu = ({ state }) => {
    let input;

    if (R.path(['modification', 'model', 'id'])(state)) {
        input = R.assocPath(['vehicle', 'modification', 'modelId', 'eq'], R.path(['modification', 'model', 'id'])(state))(input)
    }
    if (R.path(['modification', 'model', 'manufacturerId'])(state)) {
        input = R.assocPath(['vehicle', 'modification', 'model', 'manufacturerId', 'eq'], R.path(['modification', 'model', 'manufacturerId'])(state))(input)
    }
    if (R.path(['owner', 'eq'])(state)) {
        input = R.assocPath(['ownerId', 'eq'], R.path(['owner', 'eq'])(state))(input)
    }
    if (R.path(['client', 'eq'])(state)) {
        input = R.assocPath(['clientId', 'eq'], R.path(['client', 'eq'])(state))(input)
    }
    if (R.path(['author', 'eq'])(state)) {
        input = R.assocPath(['authorId', 'eq'], R.path(['author', 'eq'])(state))(input)
    }
    if (R.path(['performer', 'eq'])(state)) {
        input = R.assocPath(['performerId', 'eq'], R.path(['performer', 'eq'])(state))(input)
    }

    return input
};


export const FilterCar = ({ dispatch }) => {
    const [state, setState] = React.useState({ modification: null });

    const success = (e) => {
        if (R.equals(e, "Reset")) {
            setState({ modification: null });

            return dispatch({ type: "Reset" })
        }
        if (R.equals(e, "Apply")) {
            return dispatch({
                type: "Apply",
                payload: mamatu({ state })
            })
        }
    };

    return (
        <View
            style={{ flex: 1, justifyContent: 'space-between' }}
        >
            <View style={{ flex: 1 }}>
                <HeaderApp
                    button="CLOSED"
                    dispatch={() => {
                        dispatch({ type: "CLOSE" })
                    }}
                    header
                    title={i18n.t("Filter")}
                />
                <ScrollView style={{ padding: 10 }}>
                    <DropDownScreen
                        dispatch={(e) => {
                            if (e) return setState(R.mergeAll([state, e]))
                        }}
                        notModification
                        storage={{ modification: R.path(['modification'])(state) }}
                    />
                    {R.map((x) => (
                        <AppScreen
                            key={x}
                            dispatch={(e) => setState(R.mergeAll([state, e]))}
                            mode={x}
                            storage={R.path([x])(state)}
                        />
                    ))(['owner', 'client'])}
                    {R.map((x) => (
                        <EmployeeSelector
                            key={x}
                            dispatch={(e) => { setState(R.assocPath([x, 'eq'], e)(state)) }}
                            mode={x}
                            storage={state}
                        />
                    ))(['author', 'performer'])}
                </ScrollView>
            </View>
            <RowComponents>
                {R.map(func => (
                    <TouchesState
                        key={func}
                        color={R.equals("Reset", func) ? "VeryGrey" : "Yellow"}
                        flex={1}
                        onPress={() => {
                            success(func)
                        }}
                        title={func}
                    />
                ))(["Apply", "Reset"])}
            </RowComponents>
        </View>
    )
};