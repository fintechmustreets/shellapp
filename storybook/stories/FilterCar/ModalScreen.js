/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable lines-between-class-members */
/* eslint-disable react/require-optimization */
/* eslint-disable react/no-set-state */
/* eslint-disable space-before-function-paren */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/jsx-sort-props */
/* eslint-disable react/jsx-curly-brace-presence */
/* eslint-disable react/jsx-boolean-value */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import { useLazyQuery } from '@apollo/react-hooks';
import { ALL_CLIENT } from 'gql/clients';
import * as R from 'ramda';
import React from 'react';
import {
    ScrollView, Text, TouchableOpacity, View
} from 'react-native';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { InputComponents } from 'utils/component';
import { isOdd } from 'utils/helper';

export const grees = {
    id: 4,
    path: 'text',

    main: true,
    label: 'text',
    // Lenght: 12,
    keyboardType: "default",
    // mask: "[00000] [00000] [00]",
    value: ['text'],
    example: "что ищем",
    error: "Error message",
    errorMessage: 'PhoneError',
};

export const ModalScreen = ({ dispatch, storage }) => {
    const [state, setState] = React.useState(null);
    const [loadGreeting] = useLazyQuery(ALL_CLIENT, {
        onCompleted: (data) => setState(R.assocPath(['query'],
            R.path(R.concat(R.keys(data), ['items']))(data))(state))
    });

    React.useEffect(() => {
        loadGreeting({ variables: { where: { q: R.defaultTo(null)(R.path(['text'])(state)) } } })
    }, [R.path(['text'])(state)]);

    return (
        <View
            style={{ flex: 1 }}
        >
            <View style={{ padding: 10 }}>
                <InputComponents
                    {...grees}
                    onChangeText={(e) => {
                        dispatch(R.assocPath(['text'], e, storage))
                    }}
                    storage={storage}
                />
            </View>
            <ScrollView>
                {R.map(x => {
                    if (isOdd(x)) return <View />;
                    return (
                        <View
                            key={R.path(['id'])(x)}
                            style={{
                                backgroundColor: R.equals(R.path(['id'])(x), storage) ? 'gold' : 'transparent',
                                padding: 10,
                            }}
                        >
                            <TouchableOpacity
                                onPress={() => dispatch(x)}
                            >
                                <RowComponents
                                    style={{ paddingLeft: 10, paddingRight: 10 }}
                                >
                                    <Text
                                        style={{ flex: 1 }}>
                                        {R.pipe(R.path(['name']), R.dissoc('__typename'), R.values, R.join(" "))(x)}
                                    </Text>
                                    <Text>
                                        {R.path(['phone'])(x)}
                                    </Text>
                                </RowComponents>
                            </TouchableOpacity>
                        </View>
                    )
                })(R.defaultTo([{ id: 1 }])(R.path(['query'])(state)))}
            </ScrollView>
        </View>
    )
};