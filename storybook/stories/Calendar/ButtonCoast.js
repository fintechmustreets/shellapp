/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import React from 'react';
import { StyleSheet, Text } from 'react-native';
import * as AS from 'screens/Main/AddGoods/helper/style';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { uiColor } from 'styled/colors/uiColor.json';

export const ButtonCoast = () => (
    <CardComponents
        style={[AS.styles.shadow, TableStyle.card, { margin: 3 }]}
    >
        <RowComponents>
            <Text>Ожидаемая выручка</Text>
            <Text>1500</Text>
        </RowComponents>
    </CardComponents>
);


const TableStyle = StyleSheet.create({
    line: {
        marginTop: 15,
        marginBottom: 15,
        borderColor: uiColor.Mid_Grey,
        borderWidth: 0.2,
        marginLeft: 0,
        marginRight: 0,
    },
    card: {
        margin: 2,
        backgroundColor: uiColor.Very_Pole_Grey,
        padding: 4
    }
});