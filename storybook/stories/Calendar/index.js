/* eslint-disable max-len */
/* eslint-disable arrow-body-style */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import React from 'react';
import { Header } from 'screens/TaskEditor/components/header';
import { AgentaCalendar } from 'storybook/stories/Calendar/AgentaCalendar';
import { ButtonCoast } from 'storybook/stories/Calendar/ButtonCoast';
import { PickerType } from 'storybook/stories/Calendar/PickerType';

export const Calendar = ({ navigation }) => {
    // const [open, setOpen] = React.useState(false)

    return (
        <Header
            // dispatch={(e) => {
            //     // if (R.equals(R.path(['type'])(e), "GOBACK")) return navigation.toggleDrawer()
            // }}
            menu={{ title: "Postupload" }}
        >
            <PickerType />
            <AgentaCalendar navigation={navigation} />
            {/* <FAB.Group
                actions={[
                    { icon: 'plus', onPress: () => console.log('Pressed add') },
                    { icon: 'egg', label: 'Go Today', onPress: () => console.log('Pressed star') },
                    { icon: 'bank', label: 'income', onPress: () => console.log('Pressed email') },
                    { icon: 'task', label: 'type', onPress: () => console.log('Pressed notifications') },
                ]}
                icon={open ? 'calendar-today' : 'plus'}
                onPress={() => {
                    if (open) {
                        // do something if the speed dial is open
                    }
                }}
                onStateChange={(e) => {
                    setOpen(!open)
                }}
                open={open}
            /> */}
            <ButtonCoast />
        </Header>
    )
};