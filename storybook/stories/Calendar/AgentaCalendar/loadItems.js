/* eslint-disable max-statements-per-line */
/* eslint-disable id-length */
/* eslint-disable no-plusplus */
/* eslint-disable newline-after-var */
/* eslint-disable prefer-template */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */


export const loadItems = ({ day, setState, state }) => {
    const timeToString = (time) => new Date(time).toISOString().split('T')[0];

    for (let i = -15; i < 85; i++) {
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime = timeToString(time);
        if (!state.items[strTime]) {
            state.items[strTime] = [];
            const numItems = Math.floor(Math.random() * 3 + 1);
            for (let j = 0; j < numItems; j++) {
                state.items[strTime].push({
                    name: 'Item for ' + strTime + ' #' + j,
                    height: Math.max(50, Math.floor(Math.random() * 150))
                });
            }
        }
    }
    const newItems = {};
    Object.keys(state.items).forEach(key => { newItems[key] = state.items[key]; });
    setState({
        items: newItems
    });
};
