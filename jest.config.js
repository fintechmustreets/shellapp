module.exports = {
    "bail": 1,
    verbose: true,
    setupFiles: [
        "./node_modules/react-native-gesture-handler/jestSetup.js"
    ],
    // "collectCoverage": true,
    // "collectCoverageFrom": [
    //     "**/*.{js,jsx}",
    // ],
    "preset": "react-native",
    // "cacheDirectory": "./cache",
    // "coveragePathIgnorePatterns": [
    //     "./app/utils/vendor"
    // ],
    "defaultPlatform": "android",
    "platforms": ["android", "ios", "native"],
    "coverageThreshold": {
        "global": {
            "statements": 80
        }
    },
    "transformIgnorePatterns": [
        "/node_modules/(?!react-native|react-clone-referenced-element|react-navigation)"
    ]
};